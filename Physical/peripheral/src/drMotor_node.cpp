#include "htKYDBLDri.h"
#include "ros/ros.h"
#include <tf/transform_broadcaster.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/Vector3.h>
#include <std_msgs/Int32.h>
#include <std_msgs/Int64.h>
#include <std_msgs/Float64.h>
#include <stdio.h>
#include <cmath>
#include <algorithm>
#include <iostream>
#include <sensor_msgs/Imu.h>
#include <string>
#include "peripheral/drive_error.h"

class keyanode
{
public:
    keyanode();
    ~keyanode();
    
    ros::NodeHandle n;
    ros::NodeHandle nh_private_;
    htLib::htKYDBLDRI *left_drive;
    htLib::htKYDBLDRI *right_drive;
    std::string left_drive_port;
    std::string right_drive_port;
    int drive_baudrate;
    double rate;

    // Topic
    ros::Subscriber left_sub;               
    ros::Subscriber right_sub;               

    ros::Publisher left_motor_voltage;        
    ros::Publisher right_motor_voltage;       
    ros::Publisher left_bettery_voltage;      
    ros::Publisher right_bettery_voltage;     
    ros::Publisher left_drive_temp;           
    ros::Publisher right_drive_temp;          
    ros::Publisher left_motor_current;        
    ros::Publisher right_motor_current;       
    ros::Publisher left_encoder_stick;        
    ros::Publisher right_encoder_stick;       
    ros::Publisher left_drive_fault;          
    ros::Publisher right_drive_fault;         
    ros::Publisher left_encoder_speed;        
    ros::Publisher right_encoder_speed;

    void getPara();
    void handle_rpm_left( const std_msgs::Float64& left_rpm);
    void handle_rpm_right( const std_msgs::Float64& right_rpm);
    void init();
    void update(const ros::TimerEvent& te); 

    ros::Timer updateTimer;
 
};

keyanode::keyanode():nh_private_("~")
{
    left_drive_port = "/dev/ttyUSB0";
    right_drive_port = "/dev/ttyUSB1";
    drive_baudrate = 115200;
    rate = 10;
    
    // received from "base_controller.cpp"
    left_sub                  = n.subscribe("left_drive/velocity", 10, &keyanode::handle_rpm_left, this);
    right_sub                 = n.subscribe("right_drive/velocity", 10, &keyanode::handle_rpm_right, this); 

    // data from encoder
    left_motor_voltage         = n.advertise<std_msgs::Float64>("left_drive/status/motor_voltage", 50);
    left_bettery_voltage       = n.advertise<std_msgs::Float64>("left_drive/status/battery_voltage", 50);
    left_drive_temp            = n.advertise<std_msgs::Float64>("left_drive/status/drive_temperature", 50);
    left_motor_current         = n.advertise<std_msgs::Float64>("left_drive/status/motor_current", 50);
    left_encoder_stick         = n.advertise<std_msgs::Int64>("left_drive/status/encoder_stick", 1);
    left_drive_fault           = n.advertise<peripheral::drive_error>("left_drive/status/fault", 50);
    left_encoder_speed         = n.advertise<std_msgs::Float64>("left_drive/status/encoder_speed", 5);
    
    right_motor_voltage        = n.advertise<std_msgs::Float64>("right_drive/status/motor_voltage", 50);
    right_bettery_voltage      = n.advertise<std_msgs::Float64>("right_drive/status/battery_voltage", 50);
    right_drive_temp           = n.advertise<std_msgs::Float64>("right_drive/status/drive_temperature", 50);
    right_motor_current        = n.advertise<std_msgs::Float64>("right_drive/status/motor_current", 50);
    right_encoder_stick        = n.advertise<std_msgs::Int64>("right_drive/status/encoder_stick", 1);
    right_drive_fault          = n.advertise<peripheral::drive_error>("right_drive/status/fault", 50);
    right_encoder_speed        = n.advertise<std_msgs::Float64>("right_drive/status/encoder_speed", 5);
}


void keyanode::getPara()
{
    if(nh_private_.getParam("publish_rate", rate))
    {
		ROS_INFO_STREAM("publish_rate from param: " << rate);	       
	}

    if (nh_private_.getParam("left_drive_port", left_drive_port))
    {
        ROS_INFO_STREAM("left_drive_port from param: " << left_drive_port);
    }

    if (nh_private_.getParam("right_drive_port", right_drive_port))
    {
        ROS_INFO_STREAM("right_drive_port from param: " << right_drive_port);
    }

    if (nh_private_.getParam("drive_baudrate", drive_baudrate))
    {
        ROS_INFO_STREAM("drive_baudrate from param: " << drive_baudrate);
    }
}

void keyanode::handle_rpm_left( const std_msgs::Float64& left_rpm) 
{
  if (left_drive!=NULL)
  {
      if (left_drive->isAlive() && left_drive->isEnab())
      {
            int cmd_rpm = left_rpm.data;
            if (cmd_rpm > 1000) 
            {
                cmd_rpm = 1000;
            }
            else if (cmd_rpm < -1000)
            {
                cmd_rpm = -1000;
            }

            left_drive->Set_RPM_Motor(cmd_rpm);
      }
  }
}

void keyanode::handle_rpm_right( const std_msgs::Float64& right_rpm) 
{
  if (right_drive!=NULL)
  {
      if (right_drive->isAlive() && right_drive->isEnab())
      {
            int cmd_rpm = right_rpm.data;
            if (cmd_rpm > 1000) 
            {
                cmd_rpm = 1000;
            }
            else if (cmd_rpm < -1000) 
            {
                cmd_rpm = -1000;
            }

            right_drive->Set_RPM_Motor(-cmd_rpm);
      }
  }
}

keyanode::~keyanode()
{
    delete left_drive;
    delete right_drive;
}

void keyanode::init()
{
    getPara();

    left_drive = new htLib::htKYDBLDRI(left_drive_port, drive_baudrate);
    right_drive = new htLib::htKYDBLDRI(right_drive_port, drive_baudrate);

    left_drive->dri_name = "Left_Dri";
    right_drive->dri_name = "Right_Dri";

    if(left_drive->connect())
    {
        ROS_INFO("Connect to left driver successfully");
    }
    else
    {
        ROS_INFO("Connect to left driver failed");
    }

    if(right_drive->connect())
    {
        ROS_INFO("Connect to right driver successfully");
    }
    else
    {
        ROS_INFO("Connect to right driver failed");
    }

    // The timer ensures periodic data publishing
    updateTimer = ros::Timer(n.createTimer(ros::Duration(1.0/rate), &keyanode::update, this));
}

void keyanode::update(const ros::TimerEvent& te)
{
    htLib::ServorSatus LD_status;
    htLib::ServorSatus RD_status;
    peripheral::drive_error _left_drive_fault;
    peripheral::drive_error _right_drive_fault;

    std_msgs::Float64 _left_bettery_voltage;
    std_msgs::Float64 _right_bettery_voltage;
    std_msgs::Float64 _left_motor_voltage;
    std_msgs::Float64 _right_motor_voltage;
    std_msgs::Float64 _left_motor_current;
    std_msgs::Float64 _right_motor_current;
    std_msgs::Int64   _left_encoder_stick;
    std_msgs::Int64   _right_encoder_stick;
    std_msgs::Float64 _left_encoder_speed;
    std_msgs::Float64 _right_encoder_speed;
    std_msgs::Float64 _left_drive_temp;
    std_msgs::Float64 _right_drive_temp;

    ros::Time current_time = ros::Time::now();
    _left_drive_fault.header.stamp = current_time;
    _right_drive_fault.header.stamp = current_time;

    _left_drive_fault.IsAlive                = left_drive->isAlive();
    if (_left_drive_fault.IsAlive )
    {
        LD_status                  = left_drive->getServorSatus();
        _left_bettery_voltage.data = LD_status.BatteryVoltage;
        _left_motor_voltage.data   = LD_status.MottorVoltage;
        _left_motor_current.data   = LD_status.MottorCurrent;
        _left_encoder_stick.data   = LD_status.Encoder;
        _left_encoder_speed.data   = LD_status.EncoderSpeed;
        _left_drive_temp.data      = LD_status.Temperature;

        _left_drive_fault.ErrFree                 = LD_status.Error.ErrFree;
        _left_drive_fault.OverVoltage             = LD_status.Error.OverVoltage;
        _left_drive_fault.UnderVoltage            = LD_status.Error.UnderVoltage;                          
        _left_drive_fault.OverCurrent             = LD_status.Error.OverCurrent;
        _left_drive_fault.Stall                   = LD_status.Error.Stall;
        _left_drive_fault.OverTemperature         = LD_status.Error.OverTemperature;
        _left_drive_fault.OverTemperatureMore75   = LD_status.Error.OverTemperatureMore75;
        _left_drive_fault.IsEnable                = left_drive->isEnab();
    }
        
    _right_drive_fault.IsAlive                = right_drive->isAlive();
    if (_right_drive_fault.IsAlive)
    {
        RD_status                   = right_drive->getServorSatus();
        _right_bettery_voltage.data = RD_status.BatteryVoltage;
        _right_motor_voltage.data   = RD_status.MottorVoltage;
        _right_motor_current.data   = RD_status.MottorCurrent;
        _right_encoder_stick.data   = -RD_status.Encoder;
        _right_encoder_speed.data   = -RD_status.EncoderSpeed;
        _right_drive_temp.data      = RD_status.Temperature;

        _right_drive_fault.ErrFree                 = RD_status.Error.ErrFree;
        _right_drive_fault.OverVoltage             = RD_status.Error.OverVoltage;
        _right_drive_fault.UnderVoltage            = RD_status.Error.UnderVoltage;                          
        _right_drive_fault.OverCurrent             = RD_status.Error.OverCurrent;
        _right_drive_fault.Stall                   = RD_status.Error.Stall;
        _right_drive_fault.OverTemperature         = RD_status.Error.OverTemperature;
        _right_drive_fault.OverTemperatureMore75   = RD_status.Error.OverTemperatureMore75;
        _right_drive_fault.IsEnable                = right_drive->isEnab();
    }

    left_bettery_voltage.publish(_left_bettery_voltage);
    left_motor_voltage.publish(  _left_motor_voltage);
    left_motor_current.publish(  _left_motor_current);
    left_encoder_stick.publish(  _left_encoder_stick);
    left_encoder_speed.publish(  _left_encoder_speed);
    left_drive_temp.publish(     _left_drive_temp);      
    left_drive_fault.publish(    _left_drive_fault);

    right_bettery_voltage.publish(_right_bettery_voltage);
    right_motor_voltage.publish(  _right_motor_voltage);
    right_motor_current.publish(  _right_motor_current);
    right_encoder_stick.publish(  _right_encoder_stick);
    right_encoder_speed.publish(  _right_encoder_speed);
    right_drive_temp.publish(     _right_drive_temp);      
    right_drive_fault.publish(    _right_drive_fault);
}


int main(int argc, char *argv[])
{
    ros::init(argc, argv, "keya_node");
    keyanode obj;
    obj.init();
    ros::spin();                           
    
    return 0;
}
