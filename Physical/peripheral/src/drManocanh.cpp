#include "control.h"
#include <ros/ros.h>
#include <std_msgs/Bool.h>

#include <string>
#include <stdio.h>
#include <cmath>

class drManocanh
{
    private:
        ros::NodeHandle n;
        ros::NodeHandle nh_private_;
        control *manocanh_drive;
        std::string port;
        int32_t baudrate;
        double rate_spin;

        // Topic
        ros::Subscriber hit_sub;
        ros::Subscriber restore_sub;

        ros::Publisher control_robot;
        ros::Publisher encoder_stick;
        ros::Timer timer;

        /*
        * @brief: control motor Driver in Position Count Mode
        * @param: Position Value ( range: -100000 - 10000)
        * @return: true/false
        */
        void setMotorPos (int32_t pos_value);

        void hit_status_Cb (const std_msgs::Bool::ConstPtr& msg);

        void restore_manocanh_Cb (const std_msgs::Bool::ConstPtr& msg);

        void spin_timer (const ros::TimerEvent& e);

        void getParam ();

    public:
        drManocanh(/* args */);
        ~drManocanh();

        void init ();
};

drManocanh::drManocanh(/* args */) : n (""), nh_private_ ("~")
{
    hit_sub         = n.subscribe ("/sensor/hit_status", 10, &drManocanh::hit_status_Cb, this);
    restore_sub     = n.subscribe ("/sensor/restore_status", 10, &drManocanh::restore_manocanh_Cb, this);
    control_robot   = n.advertise<std_msgs::Bool>("/sensor/Shoted", 50);

    port      = "/dev/ttyUSB3";
    baudrate  = 115200;
    rate_spin = 20;
}

drManocanh::~drManocanh()
{
    delete manocanh_drive;
}

void drManocanh::init ()
{
    getParam();
    manocanh_drive = new control (port, baudrate);
    manocanh_drive->set_Dri_Name ("manocanh");

    if (manocanh_drive->connect ())
    {
        printf ("Connecting to Manocanh_Dri is successfull \n");
    }
    else
    {
        printf ("Connecting to Manocanh_Dri is failed \n");
    }

    ROS_INFO ("rate = %f", rate_spin);

    // ros::Timer timer_ = n.createTimer(ros::Duration(1 / rate_spin), &drManocanh::spin_timer, this);
}

/*
* @brief: control motor Driver in Position Count Mode
* @param: Position Value ( range: -100000 - 10000)
* @return: true/false
*/
void drManocanh::setMotorPos (int32_t pos_value)
{
    manocanh_drive->set_Motor_Pos (pos_value);
}

void drManocanh::hit_status_Cb (const std_msgs::Bool::ConstPtr& msg)
{
    if (msg->data == true)
    {
        printf ("hit Cb\n");

        // publish topib stop robot
        std_msgs::Bool msg;
        msg.data = true;
        control_robot.publish (msg);
        
        setMotorPos(100000);        // 350000 // bi ban
        sleep (4);
        setMotorPos(20000);         // vi tri ban dau
        sleep (0.5);

        // publish topic run robot
        msg.data = false;
        control_robot.publish (msg);
    }
}

void drManocanh::restore_manocanh_Cb (const std_msgs::Bool::ConstPtr& msg)
{
    if (msg->data == true)
    {
        printf ("restore Cb\n");
        setMotorPos(20000);         // vi tri ban dau
    }
}

// void drManocanh::spin_timer (const ros::TimerEvent& e)
// {   
//     // do nothing
// }

void drManocanh::getParam ()
{
    if(nh_private_.getParam("rate_spin", rate_spin))
    {
		ROS_INFO_STREAM("rate_spin from param: " << rate_spin);	       
	}

    if (nh_private_.getParam("port", port))
    {
        ROS_INFO_STREAM("port from param: " << port);
    }

    if (nh_private_.getParam("baudrate", baudrate))
    {
        ROS_INFO_STREAM("baudrate from param: " << baudrate);
    }
}

int main (int argc, char *argv[])
{
    ros::init(argc, argv, "manocanh_node");
    drManocanh obj;
    obj.init();
    ros::spin();                           
    
    return 0;
}
