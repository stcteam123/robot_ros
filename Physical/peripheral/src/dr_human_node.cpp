#include "dr_human_node.h"

drHuman::drHuman ()
{
    hit_sub               = n.subscribe ("/sensor/hit_status", 10, &drHuman::hit_status_Cb, this);

    // data from encoder
    motor_voltage         = n.advertise<std_msgs::Float64>("human_drive/status/motor_voltage", 50);
    bettery_voltage       = n.advertise<std_msgs::Float64>("human_drive/status/battery_voltage", 50);
    drive_temp            = n.advertise<std_msgs::Float64>("human_drive/status/drive_temperature", 50);
    motor_current         = n.advertise<std_msgs::Float64>("human_drive/status/motor_current", 50);
    drive_fault           = n.advertise<peripheral::drive_error>("human_drive/status/fault", 50);
    encoder_stick         = n.advertise<std_msgs::Int64>("human_drive/status/encoder_stick", 1);
    encoder_speed         = n.advertise<std_msgs::Float64>("human_drive/status/encoder_speed", 5);

    human_drive_port      = "/dev/ttyUSB1";
    human_drive_baudrate  = 115200;
    rate_spin             = 20;
}

drHuman::~drHuman () 
{

}

void drHuman::init ()
{
    getParam();
    human_drive = new htLib::htKYDBLDRI (human_drive_port, human_drive_baudrate);
    human_drive->dri_name = "human_Dri";

    if (human_drive->connect ())
    {
        printf ("Connecting to human_Dri is successfull \n");
    }
    else
    {
        printf ("Connecting to human_Dri is failed \n");
    }

    timer_ = n.createTimer(ros::Duration(1/rate_spin), &drHuman::spin_timer, this);
}

/*
* @brief: control motor Driver in Position Count Mode
* @param: Position Value ( range: -100000 - 10000)
* @return: true/false
*/
void drHuman::setMotorPos (int32_t pos_value)
{
    human_drive->Set_Motor_Pos (pos_value);
}

void drHuman::hit_status_Cb (const std_msgs::Bool::ConstPtr& msg)
{
    printf ("hit Cb\n");

    setMotorPos(300000);	// nga ve sau
    sleep(2);               // sleep two seconds
    setMotorPos(0);         // vi tri ban dau
    sleep(2);               // Wait for next
}

void drHuman::spin_timer (const ros::TimerEvent& e)
{
    printf ("spin timer Cb \n");

    htLib::ServorSatus Human_status;
    peripheral::drive_error _human_drive_fault;

    std_msgs::Float64 _bettery_voltage;
    std_msgs::Float64 _motor_voltage;
    std_msgs::Float64 _motor_current;
    std_msgs::Int64   _encoder_stick;
    std_msgs::Float64 _encoder_speed;
    std_msgs::Float64 _drive_temp;

    _human_drive_fault.header.stamp = ros::Time::now();
    _human_drive_fault.IsAlive       = human_drive->isAlive();

    if (_human_drive_fault.IsAlive)
    {
        Human_status          = human_drive->getServorSatus();
        _bettery_voltage.data = Human_status.BatteryVoltage;
        _motor_voltage.data   = Human_status.MottorVoltage;
        _motor_current.data   = Human_status.MottorCurrent;
        _encoder_stick.data   = Human_status.Encoder;
        _encoder_speed.data   = Human_status.EncoderSpeed;
        _drive_temp.data      = Human_status.Temperature;

        _human_drive_fault.ErrFree                 = Human_status.Error.ErrFree;
        _human_drive_fault.OverVoltage             = Human_status.Error.OverVoltage;
        _human_drive_fault.UnderVoltage            = Human_status.Error.UnderVoltage;                          
        _human_drive_fault.OverCurrent             = Human_status.Error.OverCurrent;
        _human_drive_fault.Stall                   = Human_status.Error.Stall;
        _human_drive_fault.OverTemperature         = Human_status.Error.OverTemperature;
        _human_drive_fault.OverTemperatureMore75   = Human_status.Error.OverTemperatureMore75;
        _human_drive_fault.IsEnable                = human_drive->isEnab();
    }

    bettery_voltage.publish( _bettery_voltage);
    motor_voltage.publish( _motor_voltage);
    motor_current.publish( _motor_current);
    encoder_stick.publish( _encoder_stick);
    encoder_speed.publish( _encoder_speed);
    drive_temp.publish( _drive_temp);      
    drive_fault.publish( _human_drive_fault);
}

void drHuman::getParam ()
{
    if(n.getParam("rate_spin", rate_spin))
    {
		ROS_INFO_STREAM("rate_spin from param: " << rate_spin);	       
	}

    if (n.getParam("human_drive_port", human_drive_port))
    {
        ROS_INFO_STREAM("human_drive_port from param: " << human_drive_port);
    }

    if (n.getParam("human_drive_baudrate", human_drive_baudrate))
    {
        ROS_INFO_STREAM("human_drive_baudrate from param: " << human_drive_baudrate);
    }
}

int main(int argc, char *argv[])
{
    ros::init(argc, argv, "dr_human_node");
    drHuman obj;
    obj.init();
    ros::spin();                           
    
    return 0;
}
