#ifndef PERIPHERAL_DR_HUMAN_NODE_H
#define PERIPHERAL_DR_HUMAN_NODE_H

#include "htKYDBLDri.h"

// ros lib
#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
#include <std_msgs/Int32.h>
#include <std_msgs/Int64.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Bool.h>
// msg
#include "peripheral/drive_error.h"

// c++ lib
#include <iostream>
#include <stdio.h>
#include <string>
#include <algorithm>
#include <cmath>

class drHuman 
{
public:
    drHuman ();
    ~drHuman ();

    void init ();

private:
    
    ros::NodeHandle n;

    htLib::htKYDBLDRI *human_drive;
    std::string human_drive_port;
    int human_drive_baudrate;
    double rate_spin;
    // Topic
    ros::Subscriber hit_sub;              

    ros::Publisher motor_voltage;        
    ros::Publisher bettery_voltage;      
    ros::Publisher drive_temp;           
    ros::Publisher motor_current;        
    ros::Publisher drive_fault;  
    ros::Publisher encoder_stick;        
    ros::Publisher encoder_speed; 

    ros::Timer timer_;       

    /*
    * @brief: control motor Driver in Position Count Mode
    * @param: Position Value ( range: -100000 - 10000)
    * @return: true/false
    */
    void setMotorPos (int32_t pos_value);

    void hit_status_Cb (const std_msgs::Bool::ConstPtr& msg);

    void spin_timer (const ros::TimerEvent& e);

    void getParam ();
};


#endif  // PERIPHERAL_DR_HUMAN_NODE_H