#ifndef _HT_KYDBL_PROTOCOL_H_
#define _HT_KYDBL_PROTOCOL_H_

#include <string>
#include <sstream>

#define Check_Connection "!ADD"
#define Set_Operation_Control_Mode "^CPRI"
#define Get_Operation_Control_Mode "?CPRI"
#define Rpy_Operation_Control_Mode "!CPRI"
#define Control_SIGNAL_AIN1        "0"
#define Control_SIGNAL_AIN2        "1"
#define Control_SIGNAL_PWM1        "2"
#define Control_SIGNAL_PWM2        "3"
#define Control_SIGNAL_RC          "4"
#define Control_SIGNAL_RS232       "5"
#define Control_SIGNAL_CAN         "6"
#define Control_SIGNAL_FIN         "7"
#define Enable_Controller "!ENAB"
#define Disable_Controller "!DISENAB"
#define Control_Motor_Speed "!M"
#define Set_Max_Speed_In_Closed_Loop "^MXRPM"
#define Get_Max_Speed_In_Closed_Loop "?MXRPM"
#define Rpy_Max_Speed_In_Closed_Loop "!MXRPM"
#define Set_Acc_Speed "^MAC"
#define Get_Acc_Speed "?MAC"
#define Rpy_Acc_Speed "?MAC"
#define Set_Dec_Speed "^MDEC"
#define Get_Dec_Speed "?MDEC"
#define Rpy_Dec_Speed "?MDEC"
#define Set_Motor_Operation_Mode "^MMOD"
#define Get_Motor_Operation_Mode "?MMOD"
#define Rpy_Motor_Operation_Mode "!MMOD"
#define Get_Fault "?FF"
#define Rpy_Fault "!FF"
#define Fault_Free "0"
#define Over_Voltage_Fault "1"
#define Under_Voltage_Fault "2"
#define Over_AMPS_Fault "4"
#define Stall_Fault "8"
#define Over_Temperature_Fault_65_75 "16"
#define Over_Temperature_Fault_75 "32"
#define Get_Encoder_Speed "?S"
#define Rpy_Encoder_Speed "!S"
#define Get_Hall_Speed "?BS"
#define Rpy_Hall_Speed "!BS"
#define Get_Over_Voltage "?OVL"
#define Set_Over_Voltage "^OVL"
#define Rpy_Over_Voltage "!OVL"
#define Get_Under_Voltage "?UVL"
#define Set_Under_Voltage "^UVL"
#define Rpy_Under_Voltage "!UVL"
#define Get_PID_Speed_P "?KPS"
#define Set_PID_Speed_P "^KPS"
#define Rpy_PID_Speed_P "!KPS"
#define Get_PID_Speed_I "?KIS"
#define Set_PID_Speed_I "^KIS"
#define Rpy_PID_Speed_I "!KIS"
#define Get_PID_Speed_D "?KDS"
#define Set_PID_Speed_D "^KDS"
#define Rpy_PID_Speed_D "!KDS"
#define Get_Inside_Temp "?T"
#define Rpy_Inside_Temp "!TEMP"
#define Get_Encoder_PPR "?EPPR"
#define Set_Encoder_PPR "^EPPR"
#define Rpy_Encoder_PPR "!EPPR"
#define Get_Poles "?POLES"
#define Set_Poles "^POLES"
#define Rpy_Poles "!POLES"
#define Get_Inside_Voltage "?V"
#define Rpy_Inside_Voltage "!V"
#define Get_Power_Voltage "?PV"
#define Rpy_Power_Voltage "!PV"
#define Set_Motor_Position "!P"
#define Get_Controller_ID "?Address"
#define Set_Controller_ID "^Address"
#define Rpy_Controller_ID "!Address"
#define Set_Save_Config_to_Flash "^EESAV"
#define Rpy_Save_Config_to_Flash "!EESAV"
#define Control_Emer_Stop "!EX"
#define Control_Release_Emer_Stop "!MG"
#define Get_Running_Speed "?MVEL"
#define Set_Running_Speed "^MVEL"
#define Rpy_Running_Speed "!MVEL"
#define Get_IO_Eff_Condition "?DINL"
#define Set_IO_Eff_Condition "^DINL"
#define Rpy_IO_Eff_Condition "!DINL"
#define Get_Min_Input_Ana_Vol_Signal "?AMIN"
#define Set_Min_Input_Ana_Vol_Signal "^AMIN"
#define Rpy_Min_Input_Ana_Vol_Signal "!AMIN"
#define Get_Max_Input_Ana_Vol_Signal "?AMAX"
#define Set_Max_Input_Ana_Vol_Signal "^AMAX"
#define Rpy_Max_Input_Ana_Vol_Signal "!AMAX"
#define Get_Mid_Input_Ana_Vol_Signal "?ACTR"
#define Set_Mid_Input_Ana_Vol_Signal "^ACTR"
#define Rpy_Mid_Input_Ana_Vol_Signal "!ACTR"
#define Get_Max_Value_Input_Signal "?PMAX"
#define Set_Max_Value_Input_Signal "^PMAX"
#define Rpy_Max_Value_Input_Signal "!PMAX"
#define Get_Min_Value_Input_Signal "?PMIN"
#define Set_Min_Value_Input_Signal "^PMIN"
#define Rpy_Min_Value_Input_Signal "!PMIN"
#define Get_Mid_Value_Input_Signal "?PCTR"
#define Set_Mid_Value_Input_Signal "^PCTR"
#define Rpy_Mid_Value_Input_Signal "!PCTR"
#define Get_Max_Input_AMPS "?ALIM"
#define Set_Max_Input_AMPS "^ALIM"
#define Rpy_Max_Input_AMPS "!ALIM"
#define Get_Trigger_Input_AMPS "?ATRIG"
#define Set_Trigger_Input_AMPS "^ATRIG"
#define Rpy_Trigger_Input_AMPS "!ATRIG"
#define Get_Trigger_Time "?ATGD" // ms
#define Set_Trigger_Time "^ATGD" // ms
#define Rpy_Trigger_Time "!ATGD" // ms
#define Get_Over_Current_Act "?ATGA"
#define Set_Over_Current_Act "^ATGA"
#define Rpy_Over_Current_Act "!ATGA"
#define Get_Percent_Of_O_I_Vol_Fordward "?MXPF"
#define Set_Percent_Of_O_I_Vol_Fordward "^MXPF"
#define Rpy_Percent_Of_O_I_Vol_Fordward "!MXPF"
#define Get_Percent_Of_O_I_Vol_Rev "?MXPR"
#define Set_Percent_Of_O_I_Vol_Rev "^MXPR"
#define Rpy_Percent_Of_O_I_Vol_Rev "!MXPR"
#define Get_Max_Speed "?MXRPM"
#define Set_Max_Speed "^MXRPM"
#define Rpy_Max_Speed "!MXRPM"
#define Get_Count_Of_Pos_Mode "?C"
#define Rpy_Count_Of_Pos_Mode "!C"
#define Get_Motor_AMPS "?A"
#define Rpy_Motor_AMPS "!A"
#define Get_Input_Analog "?AI"
#define Rpy_Input_Analog "!AI"
#define Get_Input_Analog_Tran "?AIC"
#define Rpy_Input_Analog_Tran "!AIC"
#define Get_BLDC_Speed "?BS"
#define Rpy_BLDC_Speed "!BS"
#define Get_Digital_Input "?D"
#define Rpy_Digital_Input "!D"
#define Get_Digital_Input_Inde "?DI"
#define Rpy_Digital_Input_Inde "!DI"
#define Get_Elec_Ratio "?RATIO"
#define Set_Elec_Ratio "^RATIO"
#define Rpy_Elec_Ratio "!RATIO"
#define Set_Reset "^RESET"
#define Rpy_Reset "!RESET"
#define Get_Mid_Input_Fre_Signal "?FCTR"
#define Set_Mid_Input_Fre_Signal "^FCTR"
#define Rpy_Mid_Input_Fre_Signal "!FCTR"
#define Get_Max_Input_Fre_Signal "?FMAX"
#define Set_Max_Input_Fre_Signal "^FMAX"
#define Rpy_Max_Input_Fre_Signal "!FMAX"
#define Get_Min_Input_Fre_Signal "?FMIN"
#define Set_Min_Input_Fre_Signal "^FMIN"
#define Rpy_Min_Input_Fre_Signal "!FMIN"
#define Get_Brake_Time "?STIM"
#define Set_Brake_Time "^STIM"
#define Rpy_Brake_Time "!STIM"
#define Get_Max_Amps_Bettery "?AMLIM"
#define Set_Max_Amps_Bettery "^AMLIM"
#define Rpy_Max_Amps_Bettery "!AMLIM"
#define Get_IO_Enable_Status "?INEN"
#define Rpy_IO_Enable_Status "!INEN"
#define Get_IO_For_Rev_Status "?INDIR"
#define Rpy_IO_For_Rev_Status "!INDIR"
#define Get_IO_Brake_Status "?INES"
#define Rpy_IO_Brake_Status "!INES"
#define Control_Position "!RUN"

namespace htLib
{

    struct KYDBLValue
    {
        std::string cmd;
        int id;
        double value;
        
        KYDBLValue()
        {
            cmd = "";
            id = 1;
            value = 0;
        }
    };
    
    std::string GetCmdString(std::string cmd, int id)
    {
        std::stringstream sid;
        sid << id;
        return cmd + " " + sid.str() + "\r";
    }

    std::string SetCmdString(std::string cmd, int id, int value)
    {
        std::stringstream sid,sval;
        sid << id;
        sval << value;
        return cmd + " " + sid.str() + " " + sval.str() + "\r";
    }

    std::string SetCmdString(std::string cmd, int id, double value)
    {
        std::stringstream sid,sval;
        sid << id;
        sval << value;
        return cmd + " " + sid.str()+ " "  + sval.str() + "\r";
    }
    
    bool checkFeedBack(const std::string cmdstring,const std::string inputstring,int id)
    {
        bool rs = false;
        std::string cmdstr =  GetCmdString(cmdstring, id);
        std::size_t pos = inputstring.find(cmdstr,0);

        if (std::string::npos == pos)
        {
            rs = false;
        }
        else
        {
            rs = true;
        }
        return rs;
    }
    bool decode_cmd(const std::string cmdstring,const std::string inputstring,int id, KYDBLValue* result)
    {
        bool rs = false;
        std::stringstream sid;
        sid << id;
        std::string str2cpr = cmdstring + " " + sid.str() + " ";
        std::size_t pos = inputstring.find(str2cpr,0);
        if (std::string::npos == pos)
        {
            rs = false;
        }
        else
        {
            std::size_t pos = inputstring.find("\r",0);
            int startValue = str2cpr.length();
            if (std::string::npos != pos && pos > startValue)
            {
                std::string valuestr = inputstring.substr(startValue,pos-startValue);
                try  
                { 
                    double val = std::stod(valuestr);
                    result->cmd = cmdstring;
                    result->id = id;
                    result->value = val;
                    rs = true;
                } 
                catch (std::exception &err)  
                { 
                    rs = false;
                }
            }
            else
            {
                rs = false;
            }
        }
        return rs;
    }

} // namespace htLib

#endif