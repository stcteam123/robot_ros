#ifndef _PERIPHERAL_CONTROL_H_
#define _PERIPHERAL_CONTROL_H_

#include "../../driver/UART/htUart.h"
#include "sys/time.h"

#include <iostream>
#include <thread>

#define SET_MOTOR_POSITION_CMD      "!P"
#define GET_ENCODER_STICK_CMD       "?C"
#define ID_DEFAULT                   1

using namespace htLib;

class control
{
    public:
        control (std::string uartPortPath, int baudrate);
        ~control ();
        bool connect ();
        void set_Dri_Name (std::string name);
        bool set_Motor_Pos (int32_t value);

    private:
        htLib::htUart *port_;
        bool isConnected_;
        int32_t baudrate_;
        std::string uartPortPath_;
        std::string driName_;

        std::string SetCmdString(std::string cmd, int id, int value);
};

control::control (std::string uartPortPath, int baudrate) :
    isConnected_ (false),
    driName_ (""),
    uartPortPath_ (uartPortPath),
    baudrate_ (baudrate)
{
    switch (baudrate)
    {
        case 9600:
            port_ = new htLib::htUart(uartPortPath, Baud9600, ParityNo, StopOne, Char8);
            break;
        case 19200:
            port_ = new htLib::htUart(uartPortPath, Baud19200, ParityNo, StopOne, Char8);
            break;
        case 38400:
            port_ = new htLib::htUart(uartPortPath, Baud38400, ParityNo, StopOne, Char8);
            break;
        case 57600:
            port_ = new htLib::htUart(uartPortPath, Baud57600, ParityNo, StopOne, Char8);
            break;
        case 115200:
            port_ = new htLib::htUart(uartPortPath, Baud115200, ParityNo, StopOne, Char8);
            break;
        case 230400:
            port_ = new htLib::htUart(uartPortPath, Baud230400, ParityNo, StopOne, Char8);
            break;
        case 460800:
            port_ = new htLib::htUart(uartPortPath, Baud460800, ParityNo, StopOne, Char8);
            break;
        case 921600:
            port_ = new htLib::htUart(uartPortPath, Baud921600, ParityNo, StopOne, Char8);
            break;
        default:
            port_ = new htLib::htUart(uartPortPath, Baud115200, ParityNo, StopOne, Char8);
            fprintf(stderr,"ERROR[%s]: int uart, invalid baudrate. Please use a standard baudrate, auto set default baudrate 115200\n", driName_.c_str());
            break;
    }
}

control::~control ()
{
    isConnected_ = false;
    port_->close ();
    delete port_;
}

bool control::connect ()
{
    if (isConnected_ == true)
    {
        return true;
    }

    bool rs = port_->open (htLib::ReadWrite);
    if (rs)
    {
        isConnected_ = true;
        // read data thread

        // send data thread

        // check driver thread
    }
    else
    {
        isConnected_ = false;
    }

    return rs;
}

std::string control::SetCmdString(std::string cmd, int id, int value)
{
    std::stringstream sid,sval;
    sid << id;
    sval << value;
    return cmd + " " + sid.str() + " " + sval.str() + "\r";
}

bool control::set_Motor_Pos (int32_t value)
{
    if (isConnected_ == true)
    {
        printf ("CONNECTED\n");
        double ID = 1;
        std::string strRTcmd = SetCmdString(SET_MOTOR_POSITION_CMD, ID, value);
        if (port_->write(strRTcmd))
        {
            printf ("OKI\n");
            return true;
        }

        fprintf(stderr,"ERROR[%s]: int uart, Write error --- Send thread start KYDBLDRI\n",this->driName_.c_str());
    }

    return false;
}

void control::set_Dri_Name (std::string name)
{
    driName_ = name;
}


#endif // _PERIPHERAL_CONTROL_H_
