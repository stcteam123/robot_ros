#ifndef _HT_DBL_DRI_H__
#define _HT_DBL_DRI_H__

#include <thread>
#include <mutex>
#include <iostream>
#include "../../driver/UART/htUart.h"
#include "htKYDBLProtocol.h"

#include "sys/time.h"

#define ENCODER_PPR 2500
#define POLE_PAIRS 16
#define MEC5_GEAR_RATIO 7


namespace htLib
{
    enum RunningState
    {
        Ain1,  // Analog + Digital value
        Ain2,  // Analog whith midle value
        PWM1,  // PWM + Digital value
        PWM2,  // PWM whith midle value
        RC,    // Radio Control
        RS232, // RS232 Control
        CAN,   // CAN Control
        Fin1,  // Frequancy Control
        Fin2   // Frequancy Control
    };
    
    struct ErrorStatus
    {
        bool OverVoltage;
        bool OverCurrent;
        bool UnderVoltage;
        bool Stall;
        bool OverTemperature;
        bool OverTemperatureMore75;
        bool InsideVoltageErr;
        bool StallPeakCurrent;
        bool ErrFree;

        ErrorStatus()
        {
            OverVoltage = false;
            OverCurrent = false;
            UnderVoltage = false;
            Stall = false;
            OverTemperature = false;
            OverTemperatureMore75 = false;
            InsideVoltageErr = false;
            StallPeakCurrent = false;
            ErrFree= false;
        }
    };

    struct AnalogInput // mV
    {
        double Ain1;
        double PWM;
        double RC;
        double FIN;

        AnalogInput()
        {
            Ain1 = 0;
            PWM = 0;
            RC = 0;
            FIN = 0;
        }
    };
    
    struct EnaRevBra
    {
        bool Enable;
        bool Reversing;
        bool Brake;
        
        EnaRevBra()
        {
            Enable = false;
            Reversing = false;
            Brake = false;
        }
    };
    
    enum RunMode
    {
        OpenLoop,
        SpeedLoop,
        HallLoop,
        TorqueLoop,
        P_Count,
        P_Tracking
    };
    
    enum OverCurrentAct
    {
        NoAct,
        SafeParking,
        EmerStop
    };


    struct Controller
    {
        double MaxInputVoltage; // Default 75
        double MinInputVoltage; // Default 18
        double ControlID; // Default 1
        double TriggerCurrent; // Default 75
        double MaxCurrent; // Default 150
        OverCurrentAct OverCurrentAction; // NoAct
        unsigned int ForwardPercentOutandInVoltage; // value/1000  Default 950
        unsigned int ReversePercentOutandInVoltage; // value/1000  Default 950
        unsigned int MaxSpeed; //rpm Default 1500
        unsigned int SoftStartTime; //ms Default 2000
        unsigned int SoftStopTime; //ms Default 200
        unsigned int BrakeCoefficient; // Default 5
        unsigned int NumOfPolePairs; // Default 16
        RunMode OperationMode; // Default Openloop
        double Speed_P; // Default 0.2
        double Speed_I; // Default 0.2
        double Speed_D; // Default 0.1
        unsigned int PositionRate; // Default 1500
        double Position_P; // Default 0.05
        double Postion_I; // Default 0.8
        double Position_D; // Default 0.05
        double DecCurveGain; // Default 10
        unsigned int ElecGearRatio; // Default 20

        RunningState ControlSignal; // Default Ain1
        unsigned int EMOD_12; // Default 0
        unsigned int EncoderPPR; // Default 2500
        unsigned int Ain1_MaxInput; // Default 3100 mV
        unsigned int Ain1_MinInput; // Default 500 mV
        unsigned int Ain1_MiddleZero; // Default 1800 mV

        bool EnableActionLevel; // Default true (High)
        bool DirectionActionLevel; // Default false (Low)
        bool BrakeActionLevel; // Default true (High)

        unsigned int Fin1_MaxInput; // Default 950 Hz
        unsigned int Fin1_MinInput; // Default 50 Hz
        unsigned int Fin1_MiddleZero; // Default 500 Hz

        Controller()
        {
            MaxInputVoltage = 75; // Default 75
            MinInputVoltage = 18; // Default 18
            ControlID = 1; // Default 1
            TriggerCurrent; // Default 75
            MaxCurrent = 150; // Default 150
            OverCurrentAction = NoAct; // NoAct
            ForwardPercentOutandInVoltage = 950; // value/1000  Default 950
            ReversePercentOutandInVoltage = 950; // value/1000  Default 950
            MaxSpeed = 1500; //rpm Default 1500
            SoftStartTime = 200; //ms Default 2000
            SoftStopTime = 2000; //ms Default 2000
            BrakeCoefficient = 5; // Default 5
            NumOfPolePairs = 16; // Default 16
            OperationMode = OpenLoop; // Default Openloop
            Speed_P = 0.2; // Default 0.2
            Speed_I = 0.2; // Default 0.2
            Speed_D = 0.1; // Default 0.1
            PositionRate = 1500; // Default 1500
            Position_P = 0.05; // Default 0.05
            Postion_I = 0.8; // Default 0.8
            Position_D = 0.05; // Default 0.05
            DecCurveGain = 10; // Default 10
            ElecGearRatio = 20; // Default 20

            ControlSignal = Ain1; // Default Ain1
            EMOD_12 = 0; // Default 0
            EncoderPPR = 2500; // Default 2500
            Ain1_MaxInput = 3100; // Default 3100 mV
            Ain1_MinInput = 500; // Default 500 mV
            Ain1_MiddleZero = 1800; // Default 1800 mV

            EnableActionLevel = true; // Default true (High)
            DirectionActionLevel = false; // Default false (Low)
            BrakeActionLevel = true; // Default true (High)

            Fin1_MaxInput = 950; // Default 950 Hz
            Fin1_MinInput = 50; // Default 50 Hz
            Fin1_MiddleZero = 500; // Default 500 Hz
        }
    };

    struct ServorSatus
    {
        RunningState ControlSignal;
        ErrorStatus Error;
        AnalogInput AnaSignalValueInput;
        EnaRevBra EnableReverseBreak;
        RunMode ControlMode;
        double BatteryVoltage;
        double MottorCurrent;
        double MottorVoltage;
        double Hall;
        double EncoderSpeed;
        double Encoder;
        double Power;
        double Temperature;
        ServorSatus()
        {
            ControlSignal = Ain1;
            Error = ErrorStatus();
            AnaSignalValueInput = AnalogInput();
            EnableReverseBreak = EnaRevBra();
            ControlMode = OpenLoop;
            BatteryVoltage = 0;
            MottorCurrent = 0;
            MottorVoltage = 0;
            Hall = 0;
            EncoderSpeed = 0;
            Encoder = 0;
            Power = 0;
            Temperature = 0;
        }
    };
    
    
    class htKYDBLDRI
    {
    private:
        ServorSatus _servorstatus;
        Controller _controller;
        htUart *_port;
        bool _isconnected;
        bool _outthreadread;
        bool _outthreadsend;
        bool _isalive;
        std::string _uartPortPath;
        void threadHandlerRead();
        void threadHandlerSend();
        void threadHandlerSendSpeed();
        void timeOutThread();
        void timeOutThreadCMD();
        int _baudrate;
        bool _isEnbale;
        std::mutex send_lock;
        std::mutex status_lock;
        std::mutex timeout_lock;
        unsigned int timeout; //[5*ms]
        std::mutex rpm_lock;
        int setpointRPM;

        std::mutex status_CMD_Lock;
        std::mutex timeout_CMD_Lock;
        bool isTimeOutCMD = false;
        int timeOutCMD = 0;
        bool isCMDAlive = false;


// struct timeval start_time, end_time;
// long t1,t2;

    public:
        htKYDBLDRI(std::string uartPortPath, int baudrate);
        ~htKYDBLDRI();
        bool connect();
        bool resetObj();
        int setpointPos;
        ServorSatus getServorSatus();
        bool isEnab();
        bool isAlive();
        int getMaxSpeed();
        std::string dri_name = "";

        // user define set value
        bool Set_RPM_Motor (int32_t value);
        bool Set_Motor_Pos (int32_t value);
    };
    
} // namespace htLib


namespace htLib
{
    
    htKYDBLDRI::htKYDBLDRI(std::string uartPortPath, int baudrate)
    {
        this->_servorstatus = ServorSatus();
        this->_controller = Controller();
        this->_isconnected = false;
        this->_outthreadread = false;
        this->_outthreadsend = false;
        this->_uartPortPath = uartPortPath;
        this->_baudrate = baudrate;
        this->_isalive = false;
        this->setpointPos = 0;
        this->setpointRPM = 0;
        this->_isEnbale = false;
        this->timeout = 0;
        switch (baudrate)
        {
        case 9600:
            this->_port = new htUart(uartPortPath,Baud9600,ParityNo,StopOne,Char8);
            break;
        case 19200:
            this->_port = new htUart(uartPortPath,Baud19200,ParityNo,StopOne,Char8);
            break;
        case 38400:
            this->_port = new htUart(uartPortPath,Baud38400,ParityNo,StopOne,Char8);
            break;
        case 57600:
            this->_port = new htUart(uartPortPath,Baud57600,ParityNo,StopOne,Char8);
            break;
        case 115200:
            this->_port = new htUart(uartPortPath,Baud115200,ParityNo,StopOne,Char8);
            break;
        case 230400:
            this->_port = new htUart(uartPortPath,Baud230400,ParityNo,StopOne,Char8);
            break;
        case 460800:
            this->_port = new htUart(uartPortPath,Baud460800,ParityNo,StopOne,Char8);
            break;
        case 921600:
            this->_port = new htUart(uartPortPath,Baud921600,ParityNo,StopOne,Char8);
            break;
        default:
            this->_port = new htUart(uartPortPath,Baud115200,ParityNo,StopOne,Char8);
            fprintf(stderr,"ERROR[%s]: int uart, invalid baudrate. Please use a standard baudrate, auto set default baudrate 115200\n", this->dri_name.c_str());
            break;
        }
        
    }
    
    htKYDBLDRI::~htKYDBLDRI()
    {
        this->_isconnected = false;
        // đợi cho đến khi nào threadHandler kết thúc
        int timeout = 0;
        while(!this->_outthreadread)
        {
            timeout++;
            usleep(1000);
            if (timeout > 9)
            {
                fprintf(stderr,"ERROR: terminate KYDBLDRI read threadhandler time out\n");
                break;
            }    
        }
        
        timeout = 0;
        while(!this->_outthreadsend)
        {
            timeout++;
            sleep(1);
            if (timeout > 1)
            {
                fprintf(stderr,"ERROR: terminate KYDBLDRI send threadhandler time out\n");
                break;
            }    
        }
        this->_port->close();
        delete this->_port;
    }

    bool htKYDBLDRI::connect()
    {
        bool rs = false;
        // nếu đã kết nối
        if(this->_isconnected) 
            return true;
        // nếu chưa kết nối
        rs = this->_port->open(htLib::ReadWrite);
        if(rs)
        {
            this->_isconnected = true;
            std::thread readThread(&htKYDBLDRI::threadHandlerRead, this);
            readThread.detach ();

            std::thread sendThread(&htKYDBLDRI::threadHandlerSend, this);
            sendThread.detach ();

            std::thread sendThreadSpeed (&htKYDBLDRI::threadHandlerSendSpeed, this);
            sendThreadSpeed.detach ();

            std::thread timeCmdThread(&htKYDBLDRI::timeOutThreadCMD, this);
            timeCmdThread.detach ();
        }
        else
        {
            this->_isconnected = false;
        }
        return rs;
    }

    void htKYDBLDRI::threadHandlerRead()
    {
        std::cout << this->dri_name << " thread start KYDBLDRI" << std::endl;
        char buff[1024];
        size_t leng = 0;
        KYDBLValue dataRevc;
        while(this->_isconnected)
        {    
            memset(buff, 0, 1024);
            leng = this->_port->readlnCR(buff, 1023);
            std::string str(buff);
            //std::cout << str << std::endl;
            timeout_lock.lock();
            this->timeout = 0;
            timeout_lock.unlock();
            if(leng > 2)
            {
                // Get encoder
                if (decode_cmd(Rpy_Count_Of_Pos_Mode,str,this->_controller.ControlID, &dataRevc))
                {
                    this->_servorstatus.Encoder = dataRevc.value;
                    //std::cout << str << std::endl;
                    continue;
                }
                // !ADD
                if (!this->_isalive && checkFeedBack(Check_Connection,str,this->_controller.ControlID))
                {
                    this->status_lock.lock();
                    this->_isalive = true;
                    this->status_lock.unlock();
                    continue;
                }
                // !ENAB
                if (!this->_isEnbale && checkFeedBack(Enable_Controller,str,this->_controller.ControlID))
                {
                    this->status_lock.lock();
                    this->_isEnbale = true;
                    this->status_lock.unlock();
                    continue;
                }
                // Get Fault
                if (decode_cmd(Rpy_Fault,str,this->_controller.ControlID, &dataRevc))
                {
                    this->status_lock.lock();
                    switch ((int)dataRevc.value)
                    {
                        case 0:
                            this->_servorstatus.Error.ErrFree = true;
                            break;
                        case 1:
                            this->_servorstatus.Error.OverVoltage = true;
                            break;
                        case 2:
                            this->_servorstatus.Error.UnderVoltage = true;
                            break;
                        case 4:
                            this->_servorstatus.Error.OverCurrent = true;
                            break;
                        case 8:
                            this->_servorstatus.Error.Stall = true;
                            break;
                        case 16:
                            this->_servorstatus.Error.OverTemperature = true;
                            break;
                        case 32:
                            this->_servorstatus.Error.OverTemperatureMore75 = true;
                            break;
                    }
                    this->status_lock.unlock();
                    continue;
                }
                // Get Motor operation mode
                if (decode_cmd(Rpy_Motor_Operation_Mode,str,this->_controller.ControlID, &dataRevc))
                {
                    this->status_lock.lock();
                    switch ((int)dataRevc.value)
                    {
                        case 0:
                            this->_controller.OperationMode = RunMode::OpenLoop;
                            break;
                        case 1:
                            this->_controller.OperationMode = RunMode::SpeedLoop;
                            break;
                        case 2:
                            this->_controller.OperationMode = RunMode::HallLoop;
                            break;
                        case 3:
                            this->_controller.OperationMode = RunMode::TorqueLoop;
                            break;
                        case 4:
                            this->_controller.OperationMode = RunMode::P_Count;
                            break;
                        case 5:
                            this->_controller.OperationMode = RunMode::P_Tracking;
                            break;
                    }
                    this->status_lock.unlock();
                    continue;
                }
                // Get Inside Voltage
                if (decode_cmd(Rpy_Inside_Voltage,str,this->_controller.ControlID, &dataRevc))
                {
                    this->_servorstatus.MottorVoltage = dataRevc.value;
                    continue;
                }
                // Get Inside AMPS
                if (decode_cmd(Rpy_Motor_AMPS,str,this->_controller.ControlID, &dataRevc))
                {
                    this->_servorstatus.MottorCurrent = dataRevc.value;
                    continue;
                }
                // Get Power Voltage
                if (decode_cmd(Rpy_Power_Voltage,str,this->_controller.ControlID, &dataRevc))
                {
                    this->_servorstatus.BatteryVoltage = dataRevc.value;
                    continue;
                }
                // Get Temperature
                if (decode_cmd(Rpy_Inside_Temp,str,this->_controller.ControlID, &dataRevc))
                {
                    this->_servorstatus.Temperature = dataRevc.value;
                    continue;
                }
                // Get Encoder Speed
                if (decode_cmd(Rpy_Encoder_Speed,str,this->_controller.ControlID, &dataRevc))
                {
                    this->_servorstatus.EncoderSpeed = dataRevc.value;
                    continue;
                }
            }
        }
        this->_outthreadread = true;
        std::cout << this->dri_name << " thread stop htKYDBLDRI" << std::endl;
    }
    
    void htKYDBLDRI::threadHandlerSendSpeed()
    {
        const uint16_t time_intervial = 20000;
        while (this->_isconnected)
        {
            if (this->_isconnected && this->_isalive && this->_isEnbale)
            {
                std::string str2send;
                str2send = GetCmdString(Get_Count_Of_Pos_Mode,this->_controller.ControlID) ;
                if (!this->_port->write(str2send))
                {
                    fprintf(stderr,"ERROR[%s]: int uart, Write error --- Send thread start KYDBLDRI\n",this->dri_name.c_str());
                }
                usleep (time_intervial);

                str2send = GetCmdString(Get_Encoder_Speed, this->_controller.ControlID);
                if (!this->_port->write(str2send))
                {
                    fprintf(stderr,"ERROR[%s]: int uart, Write error --- Send thread start KYDBLDRI\n",this->dri_name.c_str());
                }
            }

            usleep (2 * time_intervial);
        }
    }

    void htKYDBLDRI::threadHandlerSend()
    {
        std::cout << this->dri_name << " Send thread start KYDBLDRI" << std::endl;
        int regular = 0;
        std::stringstream ss;
        ss << this->_controller.ControlID;
        std::string checkalive = "?ADD " + ss.str() + "\r";
        const int time_intervial = 10000;
        const int regularcount = 2;
        std::cout << this->dri_name << " Time out thread start KYDBLDRI" << std::endl;
        std::thread startTimeOutThread(&htKYDBLDRI::timeOutThread,this);
        startTimeOutThread.detach();
        while (this->_isconnected)
        {
            //Check is alive
            while (this->_isconnected && !this->_isalive)
            {
                fprintf(stdout,"[%s] Check is alive: ?ADD\n",this->dri_name.c_str());
                if (!this->_port->write(checkalive))
                {
                    fprintf(stderr,"ERROR[%s]: int uart, Write error --- Send thread start KYDBLDRI\n",this->dri_name.c_str());
                } 
                usleep(500000);
            }

            // Enabale
            while (this->_isconnected && this->_isalive && !this->_isEnbale)
            {
                fprintf(stdout,"[%s] Check is alive: !ENAB\n",this->dri_name.c_str());
                std::string enastr = "!ENAB " + ss.str() + "\r";
                if (!this->_port->write(enastr))
                {
                    fprintf(stderr,"ERROR[%s]: int uart, Write error --- Send thread start KYDBLDRI: !ENAB\n",this->dri_name.c_str());
                } 
                usleep(500000);
            }
            fprintf(stdout,"[%s] regular rautine \n",this->dri_name.c_str());
            while(this->_isconnected && this->_isalive && this->_isEnbale)
            {    
                if (regular >= regularcount)
                {
                    std::string str2send;

                    str2send = GetCmdString(Get_Fault,this->_controller.ControlID);
                    if (!this->_port->write(str2send))
                    {
                        fprintf(stderr,"ERROR[%s]: int uart, Write error --- Send thread start KYDBLDRI\n",this->dri_name.c_str());
                    }
                    usleep (time_intervial);
                    
                    str2send = GetCmdString(Get_Motor_Operation_Mode,this->_controller.ControlID);
                    if (!this->_port->write(str2send))
                    {
                        fprintf(stderr,"ERROR[%s]: int uart, Write error --- Send thread start KYDBLDRI\n",this->dri_name.c_str());
                    }
                    usleep (time_intervial);
                    
                    str2send = GetCmdString(Get_Inside_Voltage,this->_controller.ControlID);
                    if (!this->_port->write(str2send))
                    {
                        fprintf(stderr,"ERROR[%s]: int uart, Write error --- Send thread start KYDBLDRI\n",this->dri_name.c_str());
                    }
                    usleep (time_intervial);
                    
                    str2send = GetCmdString(Get_Power_Voltage,this->_controller.ControlID);
                    if (!this->_port->write(str2send))
                    {
                        fprintf(stderr,"ERROR[%s]: int uart, Write error --- Send thread start KYDBLDRI\n",this->dri_name.c_str());
                    }
                    usleep (time_intervial);
                    
                    str2send = GetCmdString(Get_Motor_AMPS,this->_controller.ControlID);
                    if (!this->_port->write(str2send))
                    {
                        fprintf(stderr,"ERROR[%s]: int uart, Write error --- Send thread start KYDBLDRI\n",this->dri_name.c_str());
                    }
                    usleep (time_intervial);
                    
                    str2send = GetCmdString(Get_Inside_Temp,this->_controller.ControlID);
                    if (!this->_port->write(str2send))
                    {
                        fprintf(stderr,"ERROR[%s]: int uart, Write error --- Send thread start KYDBLDRI\n",this->dri_name.c_str());
                    }

                    regular = 0;
                }

                regular++;
                usleep(time_intervial);
            }
            this->status_lock.lock();
            this->_isEnbale = false;
            this->_isalive = false;
            this->status_lock.unlock();
        }
        this->_outthreadsend = true;
        fprintf(stdout,"[%s] Send thread stop htKYDBLDRI\n",this->dri_name.c_str());
    }
    

    bool htKYDBLDRI::resetObj()
    {
        bool rs = false;
        if (this->_outthreadread)
        {
            this->_outthreadread = false;
        }
        if (this->_outthreadsend)
        {
            this->_outthreadsend = false;
        }
        rs = true;
        return rs;
    }

    ServorSatus htKYDBLDRI::getServorSatus()
    {
        return this->_servorstatus;
    }

    bool  htKYDBLDRI::isEnab()
    {
        return this->_isEnbale;
    }
    
    bool htKYDBLDRI::isAlive()
    {
        return this->_isalive;
    }

    int htKYDBLDRI::getMaxSpeed()
    {
        return this->_controller.MaxSpeed;
    }

    void htKYDBLDRI::timeOutThread()
    {
        usleep(500000);
        while (this->_isconnected)
        {
            timeout_lock.lock();
            this->timeout ++;
            timeout_lock.unlock();
            if (timeout > 10)
            {
                this->status_lock.lock();
                this->_isalive = false;
                this->status_lock.unlock();
            }
            usleep(100000);    
        }
        fprintf(stderr,"[%s] Out of timeOutThread\n",this->dri_name.c_str());
    }

    void htKYDBLDRI::timeOutThreadCMD()
    {
        while (this->_isconnected)
        {
            timeout_CMD_Lock.lock();
            this->timeOutCMD ++;
            timeout_CMD_Lock.unlock();
            if (timeOutCMD > 5)
            {
                if ( this->isCMDAlive)
                {
                    fprintf(stderr,"[%s] CMD Not Work\n",this->dri_name.c_str());
                }
                
                this->isCMDAlive = false;
            }
            else
            {
                this->isCMDAlive = true;
            }
            usleep(20000);    
        }
        fprintf(stderr,"[%s] Out of timeOutThread\n",this->dri_name.c_str());
    }

    // user define set value
    bool htKYDBLDRI::Set_RPM_Motor (int32_t value)
    {
        if (this->_isconnected && this->_isalive && this->_isEnbale)
        {
            std::string strRTcmd = SetCmdString(Control_Motor_Speed, this->_controller.ControlID, value);
            if (this->_port->write(strRTcmd))
            {
                return true;
            }
            
            fprintf(stderr,"ERROR[%s]: int uart, Write error --- Send thread start KYDBLDRI\n",this->dri_name.c_str());
        }

        return false;
    }

    bool htKYDBLDRI::Set_Motor_Pos (int32_t value)
    {
        if (this->_isconnected && this->_isalive && this->_isEnbale)
        {
            std::string strRTcmd = SetCmdString(Set_Motor_Position, this->_controller.ControlID, value);
            if (this->_port->write(strRTcmd))
            {
                return true;
            }

            fprintf(stderr,"ERROR[%s]: int uart, Write error --- Send thread start KYDBLDRI\n",this->dri_name.c_str());
        }

        return false;
    }

} // namespace htLib

#endif
