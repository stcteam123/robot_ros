#ifndef _htUart_H__
#define _htUart_H__

#include <string>
#include <fstream>
#include <cstdint>
#include <unistd.h>

#include <fcntl.h>
#include <termios.h>
#include <stdio.h>
#include <string.h>
#include "htDef.h"

namespace htLib
{
    struct errorUart
    {
       
        /*! @brief TTY file @b opening error.
        *
        *  Its value can change, when opening htUart's tty file, at@n
        *  @li open()
        *
        *  function in htUart class.
        *  @sa htUart::open()
        */
        bool openError;

        /*! @brief TTY file @b closing error.
        *
        *  Its value can change, when closing htUart's tty file, at@n
        *  @li close()
        *
        *  function in htUart class.
        *  @sa htUart::close()
        */
        bool closeError;


        /*! @brief Some htUart operation's direction error.
        *
        *  Its value can change, when doing some operations to htUart, at@n
        *  @li setBaudRate()
        *  @li flush()
        *
        *  functions in htUart class.
        *  @sa htUart::setBaudRate()
        *  @sa htUart::flush()
        */
        bool directionError;


        /*! @brief htUart line flushing error.
        *
        *  Its value can change, when flushing line, at@n
        *  @li flush()
        *
        *  function in htUart class.
        *  @sa htUart::flush()
        */
        bool flushError;


        /*! @brief TTY file @b reading error.
        *
        *  Its value can change, when reading htUart's tty file, at@n
        *  @li read()
        *  @li transfer()
        *  @li operator>>()
        *
        *  functions in htUart class.
        *  @sa htUart::read(char*, size_t)
        *  @sa htUart::read()
        *  @sa htUart::transfer(char*, char*, size_t, uint32_t)
        *  @sa htUart::transfer(std::string, uint32_t)
        *  @sa htUart::operator>>(std::string &)
        */
        bool readError;


        /*! @brief TTY file @b writing error.
        *
        *  Its value can change, when writing htUart's tty file, at@n
        *  @li write()
        *  @li transfer()
        *  @li operator<<()
        *
        *  functions in htUart class.
        *  @sa htUart::write(char*, size_t)
        *  @sa htUart::write(std::string)
        *  @sa htUart::transfer(char*, char*, size_t, uint32_t)
        *  @sa htUart::transfer(std::string, uint32_t)
        *  @sa htUart::operator<<(std::string &)
        */
        bool writeError;


        /*! @brief htUart's @b baud property setting error.
        *
        *  Its value can change, when setting htUart's baud property, at@n
        *  @li getBaudRate()
        *  @li setBaudRate()
        *  @li getProperties()
        *  @li setProperties()
        *
        *  functions in htUart class.
        *  @sa htUart::getBaudRate()
        *  @sa htUart::setBaudRate()
        *  @sa htUart::getProperties()
        *  @sa htUart::setProperties()
        */
        bool baudRateError;


        /*! @brief htUart's @b parity property setting error.
        *
        *  Its value can change, when setting htUart's parity property, at@n
        *  @li getParity()
        *  @li setParity()
        *  @li getProperties()
        *  @li setProperties()
        *
        *  functions in htUart class.
        *  @sa htUart::getParity()
        *  @sa htUart::setParity()
        *  @sa htUart::getProperties()
        *  @sa htUart::setProperties()
        */
        bool parityError;


        /*! @brief htUart's @b stop @b bits property setting error.
        *
        *  Its value can change, when setting htUart's stop bits property, at@n
        *  @li getStopBits()
        *  @li setParity()
        *  @li getProperties()
        *  @li setProperties()
        *
        *  functions in htUart class.
        *  @sa htUart::getStopBits()
        *  @sa htUart::setStopBits()
        *  @sa htUart::getProperties()
        *  @sa htUart::setProperties()
        */
        bool stopBitsError;


        /*! @brief htUart's @b character @b size property setting error.
        *
        *  Its value can change, when setting htUart's character size property, at@n
        *  @li getCharacterSize()
        *  @li setCharacterSize()
        *  @li getProperties()
        *  @li setProperties()
        *
        *  functions in htUart class.
        *  @sa htUart::getCharacterSize()
        *  @sa htUart::setCharacterSize()
        *  @sa htUart::getProperties()
        *  @sa htUart::setProperties()
        */
        bool charSizeError;


        /*! @brief errorUart struct's constructor.
         *
         *  This function clears all flags and initializes errorCore struct.
         */
        errorUart()
        {
            openError       = false;
            closeError      = false;
            directionError  = false;
            flushError      = false;
            readError       = false;
            writeError      = false;
            baudRateError   = false;
            parityError     = false;
            stopBitsError   = false;
            charSizeError   = false;
        }
    };

    /*!
     * enum dùng để lựa chọn tên htUart.
     */
    enum UartName           
    {   
        Uart0 = 0,
        Uart1 = 1,
        Uart2 = 2,
        Uart4 = 4,
        Uart5 = 5
    };

    /*!
     * enum để lựa chọn baudrate.
     */
    enum baudRate           
    {   
        Baud0      = B0,
        Baud50     = B50,
        Baud75     = B75,
        Baud110    = B110,
        Baud134    = B134,
        Baud150    = B150,
        Baud200    = B200,
        Baud300    = B300,
        Baud600    = B600,
        Baud1200   = B1200,
        Baud1800   = B1800,
        Baud2400   = B2400,
        Baud4800   = B4800,
        Baud9600   = B9600,
        Baud19200  = B19200,
        Baud38400  = B38400,
        Baud57600  = B57600,  
        Baud115200 = B115200, 
        Baud230400 = B230400, 
        Baud460800 = B460800, 
        Baud500000 = B500000, 
        Baud576000 = B576000, 
        Baud921600 = B921600 
    };
    
    /*!
     * enum dùng set bit chẵn lẽ.
     */
    enum parity             
    {   
        ParityNo      = 0,
        ParityOdd     = 1,
        ParityEven    = 2,
        ParityDefault = -1
    };
    
    /*!
     * enum xác định kích thước character size.
     */
    enum characterSize      
    {   
        Char5       = 5,
        Char6       = 6,
        Char7       = 7,
        Char8       = 8,
        CharDefault = 0
    };

    
    /*!
     * enum xác định số stop bit(s).
     */
    enum stopBits           
    {   
        StopOne     = 1,
        StopTwo     = 2,
        StopDefault = -1
    };


    /*!
     * enum xác định chế độ cại đặt sựu thay đổi của htUart property.
     */
    enum UartApplyMode      
    {   
        ApplyNow   = 0,
        ApplyDrain = 1,
        ApplyFlush = 2
    };

    // ######################################### Khai báo UartProperties ######################################### //

    /*! @brief Holds properties of htUart.
     *
     *    This struct holds input and output baud rate, parity, stop bits size and character size
     *    properties of htUart. Also it has overloaded constructors and overloaded assign operator.
     *    @sa htLib::baudRate
     *    @sa htLib::parity
     *    @sa htLib::stopBits
     *    @sa htLib::characterSize
     */
    struct UartProperties
    {
        baudRate        htUartBaudIn;     /*!< @brief is used to hold the baud rate of htUart RX */
        baudRate        htUartBaudOut;    /*!< @brief is used to hold the baud rate of htUart TX */
        parity          htUartParity;     /*!< @brief is used to hold the parity type of htUart */
        stopBits        htUartStopBits;   /*!< @brief is used to hold the stop bits size of htUart */
        characterSize   htUartCharSize;   /*!< @brief is used to hold the character size of htUart */

        /*! @brief Default constructor of UartProperties struct.
         *
         *  This function sets default value to variables.
         */
        UartProperties()
        {
            htUartBaudIn      = Baud9600;
            htUartBaudOut     = Baud9600;
            htUartParity      = ParityDefault;
            htUartStopBits    = StopDefault;
            htUartCharSize    = CharDefault;
        }

        /*! @brief Overloaded constructor of UartProperties struct.
         *
         *  This function sets input arguments to variables.
         * @sa htLib::baudRate
         * @sa htLib::parity
         * @sa htLib::stopBits
         * @sa htLib::characterSize
         */
        UartProperties(baudRate S_baudIn, baudRate S_baudOut, parity S_parity, stopBits S_stopBits, characterSize S_charSize)
        {
            htUartBaudIn      = S_baudIn;
            htUartBaudOut     = S_baudOut;
            htUartParity      = S_parity;
            htUartStopBits    = S_stopBits;
            htUartCharSize    = S_charSize;
        }

        /*! @brief Overloaded constructor of UartProperties struct.
         *
         *  This function gets pointer of UartProperties struct and sets input
         *  struct's variables to own variables.
         */
        UartProperties( UartProperties *S_properties )
        {
            htUartBaudIn      = S_properties->htUartBaudIn;
            htUartBaudOut     = S_properties->htUartBaudOut;
            htUartParity      = S_properties->htUartParity;
            htUartStopBits    = S_properties->htUartStopBits;
            htUartCharSize    = S_properties->htUartCharSize;
        }


        /*! @brief Overloaded assign operator of UartProperties struct.
         *
         *  This function assigns input struct's variables to own variables.
         */
        UartProperties& operator=(UartProperties equal)
        {
            htUartBaudIn      = equal.htUartBaudIn;
            htUartBaudOut     = equal.htUartBaudOut;
            htUartCharSize    = equal.htUartCharSize;
            htUartParity      = equal.htUartParity;
            htUartStopBits    = equal.htUartStopBits;

            return *this;
        }
    };
    // ########################################## UartProperties DECLARATION ENDS ########################################### //


    class htUart
    {
    private:
        UartProperties defaultUartProperties;      /*!< @brief is used to hold the default properties of htUart */
        UartProperties currentUartProperties;      /*!< @brief is used to hold the current properties of htUart */
        UartProperties constructorProperties;      /*!< @brief is used to hold the user specified properties of htUart */
        errorUart       htUartErrors;
        std::string     htUartPortPath;                   /*!< @brief is used to hold the htUart's tty port path */
        uint32_t        readBufferSize;                 /*!< @brief is used to hold the size of temporary buffer */
        int             htUartFd;                         /*!< @brief is used to hold the htUart's tty file's file descriptor */
        bool            isOpenFlag;                     /*!< @brief is used to hold the htUart's tty file's state */
        bool            isCurrentEqDefault;             /*!< @brief is used to hold the properties of htUart is equal to default properties */
    public:
        
        /*!
            * This enum is used to define htUart debugging flags.
            */
        enum flags      
        {   
            dtErr       = 1,    /*!< enumeration for @a errorUart::dtError status */
            openErr     = 2,    /*!< enumeration for @a errorUart::openError status */
            closeErr    = 3,    /*!< enumeration for @a errorUart::closeError status */
            directionErr= 4,    /*!< enumeration for @a errorUart::directionError status */
            flushErr    = 5,    /*!< enumeration for @a errorUart::flushError status */
            readErr     = 6,    /*!< enumeration for @a errorUart::readError status */
            writeErr    = 7,    /*!< enumeration for @a errorUart::writeError status */
            baudRateErr = 8,    /*!< enumeration for @a errorUart::baudRateError status */
            parityErr   = 9,    /*!< enumeration for @a errorUart::parityError status */
            stopBitsErr = 10,   /*!< enumeration for @a errorUart::stopBitsError status */
            charSizeErr = 11    /*!< enumeration for @a errorUart::charSizeError status */
        };

        /*! @brief Constructor of htUart class.
         *
         * This function initializes errorUart struct, sets value of constructorProperties struct and local variables.
         * Then calls device tree loading function.
         *
         * @param [in] htUart          name of htUart (enum),(htUartx)
         * @param [in] htUartBaud      baud rate of htUart (enum)
         * @param [in] htUartParity    parity of htUart (enum)
         * @param [in] htUartStopBits  stop bits of htUart (enum)
         * @param [in] htUartCharSize  character size of htUart (enum)
         *
         * @par Example
         *  @code{.cpp}
         *   htLib::htUart  myhtUart(htLib::htUart1,
         *                               htLib::Baud9600,
         *                               htLib::ParityEven,
         *                               htLib::StopOne,
         *                               htLib::Char8 );
         *
         *   htLib::htUart  *myhtUartPtr = new htLib::htUart(htLib::htUart4,
         *                                                             htLib::Baud9600,
         *                                                             htLib::ParityEven,
         *                                                             htLib::StopOne,
         *                                                             htLib::Char8 );
         *
         *   myhtUart.open( htLib::ReadWrite );
         *   myhtUartPtr->open( htLib::ReadWrite );
         *
         * @endcode
         *
         * @sa loadDeviceTree()
         * @sa UartName
         * @sa baudRate
         * @sa parity
         * @sa stopBits
         * @sa characterSize
         */
            
        htUart(UartName htUart, baudRate htUartBaud, parity htUartParity, stopBits htUartStopBits, characterSize htUartCharSize);
        htUart(std::string htUartPortPath, baudRate htUartBaud, parity htUartParity, stopBits htUartStopBits, characterSize htUartCharSize);

        /*! @brief Constructor of htUart class.
         *
         * This function initializes errorUart struct, sets value of constructorProperties struct and local variables.
         * Then calls device tree loading function.
         *
         * @param [in] htUart            name of htUart (enum),(htUartx)
         * @param [in] UartProperties  import properties from outside
         *
         * @par Example
         *  @code{.cpp}
         *
         *   htLib::UartProperties myhtUartProps(htLib::Baud9600,
         *                                             htLib::Baud9600,
         *                                             htLib::ParityEven,
         *                                             htLib::StopOne,
         *                                             htLib::Char8);
         *
         *   htLib::htUart  myhtUart(htLib::htUart1, myhtUartProps);
         *   htLib::htUart  *myhtUartPtr = new htLib::htUart(htLib::htUart4, myhtUartProps);
         *
         *   myhtUart.open( htLib::ReadWrite );
         *   myhtUartPtr->open( htLib::ReadWrite );
         *
         * @endcode
         *
         * @sa loadDeviceTree()
         * @sa UartName
         * @sa UartProperties
         */
        htUart(UartName htUart, UartProperties UartProperties);
        htUart(std::string htUartPortPath, UartProperties UartProperties);

        /*! @brief Constructor of htUart class.
         *
         * This function initializes errorUart struct, sets local variables. Then calls device tree loading function.
         * Objects which are initialized from htUart class with this constructor, uses default htUart properties.
         *
         * @param [in] htUart            name of htUart (enum),(htUartx)
         *
         * @par Example
         *  @code{.cpp}
         *
         *   htLib::htUart  myhtUart(htLib::htUart1);
         *   htLib::htUart  *myhtUartPtr = new htLib::htUart(htLib::htUart4);
         *
         *   myhtUart.open( htLib::ReadWrite );
         *   myhtUartPtr->open( htLib::ReadWrite );
         *
         * @endcode
         * @sa loadDeviceTree()
         * @sa UartName
         */
        htUart(UartName htUart);
        htUart(std::string htUartPortPath);

        /*! @brief Destructor of htUart class.
         *
         * This function closes TTY file and deletes errorUart struct pointer.
         */
        virtual ~htUart();

        /*! @brief Opens TTY file of htUart.
         *
         * This function opens htUart's TTY file with selected open mode, gets default properties of htUart
         * and saves this properties to htUart::defaultUartProperties struct. Then sets properties
         * which are specified at class initialization stage. Users can send "or"ed htLib::openMode
         * enums as parameter to this function.
         * @warning After initialization of htUart class, this function must call. Otherwise users
         * could not use any of data sending or receiving functions.
         *
         * @param [in] openMode          file opening mode
         * @return True if tty file opening successful, else false.
         *
         * @par Example
         *  @code{.cpp}
         *
         *   htLib::htUart  myhtUart(htLib::htUart1,
         *                               htLib::Baud9600,
         *                               htLib::ParityEven,
         *                               htLib::StopOne,
         *                               htLib::Char8 );
         *
         *   myhtUart.open( htLib::ReadWrite | htLib::NonBlock );
         *
         * @endcode
         * @sa openMode
         */
        bool open(uint openMode);

        /*! @brief Closes TTY file of htUart.
         *
         * This function closes htUart's TTY file and changes htUart::isOpenFlag's value.
         * @return True if tty file closing successful, else false.
         *
         * @par Example
         *  @code{.cpp}
         *
         *   htLib::htUart  myhtUart(htLib::htUart1,
         *                               htLib::Baud9600,
         *                               htLib::ParityEven,
         *                               htLib::StopOne,
         *                               htLib::Char8 );
         *
         *   myhtUart.open( htLib::ReadWrite | htLib::NonBlock );
         *   myhtUart.close();
         *
         * @endcode
         * @sa htUart::isOpenFlag
         */
        bool close();

        /*! @brief Flushes htUart line.
         *
         * This function flushes htUart line at specified direction.
         * @param [in] whichDirection flushing direction
         * @return True if flushing successful, else false.
         *
         * @par Example
         *  @code{.cpp}
         *
         *   htLib::htUart  myhtUart(htLib::htUart1,
         *                               htLib::Baud9600,
         *                               htLib::ParityEven,
         *                               htLib::StopOne,
         *                               htLib::Char8 );
         *
         *   myhtUart.open( htLib::ReadWrite | htLib::NonBlock );
         *   myhtUart.flush( htLib::bothDirection );
         *
         * @endcode
         * @sa direction
         */
        bool flush(direction whichDirection);

        /*! @brief Reads values from htUart line.
         *
         * This function reads values from htUart line and returns read value as string.
         * It creates temporary read buffer with specified size and reads values to this
         * buffer. Then resize buffer to read value size. htUart::readBufferSize variable
         * is used to specify temporary buffer size.
         *
         * @return read value if reading successful, else returns htLib::htUart_READ_FAILED string.
         *
         * @par Example
         *  @code{.cpp}
         *
         *   htLib::htUart  myhtUart(htLib::htUart1,
         *                               htLib::Baud9600,
         *                               htLib::ParityEven,
         *                               htLib::StopOne,
         *                               htLib::Char8 );
         *
         *   myhtUart.open( htLib::ReadWrite | htLib::NonBlock );
         *   myhtUart.flush( htLib::bothDirection );
         *
         *   std::string writeTohtUart  = "this is test.\n";
         *   myhtUart.write(writeTohtUart);
         *
         *   sleep(1);
         *
         *   std::string readFromhtUart = myhtUart.read();
         *
         *   std::cout << "Test output on loopback: " << readFromhtUart;
         *
         * @endcode
         * @code{.cpp}
         *   // Possible Output:
         *   // Test output on loopback: this is test.
         * @endcode
         * @sa htUart::readBufferSize
         */
        std::string read();
        size_t readln(char *buffer, size_t maxlen);
        size_t readlnCR(char *buffer, size_t maxlen);
        /*! @brief Reads values from htUart line.
         *
         * This function reads values from htUart line and saves read value to @a @b readBuffer pointer.
         * It creates temporary read buffer with specified size and reads values to this
         * buffer. Then copies buffer to  @a @b readBuffer pointer.
         *
         * @param [out] readBuffer          buffer pointer
         * @param [in] size                 buffer size
         * @return true if reading successful, else false.
         *
         * @par Example
         *  @code{.cpp}
         *
         *   htLib::htUart  myhtUart(htLib::htUart1,
         *                               htLib::Baud9600,
         *                               htLib::ParityEven,
         *                               htLib::StopOne,
         *                               htLib::Char8 );
         *
         *   myhtUart.open( htLib::ReadWrite | htLib::NonBlock );
         *   myhtUart.flush( htLib::bothDirection );
         *
         *   std::string writeTohtUart  = "this is test.\n";
         *   myhtUart.write(writeTohtUart);
         *
         *   sleep(1);
         *
         *   char readBuffer[14];
         *   myhtUart.read(readBuffer, sizeof(readBuffer));
         *
         *   std::cout << "Test output on loopback: " << readBuffer;
         *
         * @endcode
         * @code{.cpp}
         *   // Possible Output:
         *   // Test output on loopback: this is test.
         * @endcode
         */
        bool read(char *readBuffer, size_t size);
        size_t readbytes(char *readBuffer, size_t size);
        size_t readbytes(uint8_t *readBuffer, size_t size);
        /*! @brief Writes values to htUart line.
         *
         * This function writes values to htUart line. Values sent to this function as string type.
         *
         * @param [in] writeBuffer          values buffer
         * @return true if writing successful, else false.
         *
         * @par Example
         *  @code{.cpp}
         *
         *   htLib::htUart  myhtUart(htLib::htUart1,
         *                               htLib::Baud9600,
         *                               htLib::ParityEven,
         *                               htLib::StopOne,
         *                               htLib::Char8 );
         *
         *   myhtUart.open( htLib::ReadWrite | htLib::NonBlock );
         *   myhtUart.flush( htLib::bothDirection );
         *
         *   std::string writeTohtUart  = "this is test.\n";
         *   myhtUart.write(writeTohtUart);
         *
         *   sleep(1);
         *
         *   std::string readFromhtUart = myhtUart.read();
         *
         *   std::cout << "Test output on loopback: " << readFromhtUart;
         *
         * @endcode
         * @code{.cpp}
         *   // Possible Output:
         *   // Test output on loopback: this is test.
         * @endcode
         */
        bool write(std::string writeBuffer);
    
        /*! @brief Writes values to htUart line.
         *
         * This function writes values to htUart line. Values sent to this function as c-style string(char array).
         *
         * @param [in] writeBuffer          values buffer
         * @param [in] size                 buffer size
         * @return true if writing successful, else false.
         *
         * @par Example
         *  @code{.cpp}
         *
         *   htLib::htUart  myhtUart(htLib::htUart1,
         *                               htLib::Baud9600,
         *                               htLib::ParityEven,
         *                               htLib::StopOne,
         *                               htLib::Char8 );
         *
         *   myhtUart.open( htLib::ReadWrite | htLib::NonBlock );
         *   myhtUart.flush( htLib::bothDirection );
         *
         *   char writeBuffer[]  = "this is test.\n";
         *   myhtUart.write(writeBuffer, sizeof(writeBuffer));
         *
         *   sleep(1);
         *
         *   std::string readFromhtUart = myhtUart.read();
         *
         *   std::cout << "Test output on loopback: " << readFromhtUart;
         *
         * @endcode
         * @code{.cpp}
         *   // Possible Output:
         *   // Test output on loopback: this is test.
         * @endcode
         */
        bool write(char *writeBuffer, size_t size);
        size_t writebytes(char *writeBuffer, size_t size);
        /*! @brief Writes and reads values sequentially to/from htUart line.
         *
         * This function writes values to htUart line firstly and then reads values from htUart line and saves read
         * value to @a @b readBuffer pointer. It creates temporary read buffer with specified size and reads
         * values to this buffer. Then it copies buffer to  @a @b readBuffer pointer. This function waits between
         * writing and reading operations.
         *
         * @param [in] writeBuffer          values buffer
         * @param [out] readBuffer          read buffer pointer
         * @param [in] size                 buffer size
         * @param [in] wait_us              sleep time between writing and reading
         * @return true if transfering successful, else false.
         *
         * @par Example
         *  @code{.cpp}
         *
         *   htLib::htUart  myhtUart(htLib::htUart1,
         *                               htLib::Baud9600,
         *                               htLib::ParityEven,
         *                               htLib::StopOne,
         *                               htLib::Char8 );
         *
         *   myhtUart.open( htLib::ReadWrite | htLib::NonBlock );
         *   myhtUart.flush( htLib::bothDirection );
         *
         *   char writeBuffer[]  = "this is test.\n";
         *   char readBuffer[ myhtUart.getReadBufferSize() ];
         *
         *   myhtUart.transfer(writeBuffer, readBuffer, sizeof(writeBuffer), 40000);
         *
         *   std::cout << "Test output on loopback: " << readBuffer;
         *
         * @endcode
         * @code{.cpp}
         *   // Possible Output:
         *   // Test output on loopback: this is test.
         * @endcode
         * @sa htUart::readBufferSize
         */
        bool transfer(char *writeBuffer, char *readBuffer, size_t size, uint32_t wait_us);

        /*! @brief Writes and reads values sequentially to/from htUart line.
         *
         * This function writes values to htUart line firstly and then reads values from htUart line and returns read
         * value as string. It creates temporary read buffer with specified size and reads values to this buffer.
         * Then resize buffer to read value size. htUart::readBufferSize variable is used to specify temporary
         * buffer size.
         *
         * @param [in] writeBuffer          write buffer
         * @param [in] wait_us              sleep time between writing and reading
         * @return read value if reading successful, else returns htLib::htUart_READ_FAILED or
         * htLib::htUart_WRITE_FAILED string.
         *
         * @par Example
         *  @code{.cpp}
         *
         *   htLib::htUart  myhtUart(htLib::htUart1,
         *                               htLib::Baud9600,
         *                               htLib::ParityEven,
         *                               htLib::StopOne,
         *                               htLib::Char8 );
         *
         *   myhtUart.open( htLib::ReadWrite | htLib::NonBlock );
         *   myhtUart.flush( htLib::bothDirection );
         *
         *   std::string writeTohtUart = "this is test.\n";
         *   std::string readFromhtUart;
         *
         *   readFromhtUart = myhtUart.transfer(writeTohtUart, 40000);
         *
         *   std::cout << "Test output on loopback: " << readFromhtUart;
         *
         * @endcode
         * @code{.cpp}
         *   // Possible Output:
         *   // Test output on loopback: this is test.
         * @endcode
         * @sa htUart::readBufferSize
         */
        std::string transfer(std::string writeBuffer, uint32_t wait_us);

        /*! @brief Changes internal temporary buffers' sizes.
         *
         * This function changes internal buffers' sizes which are used at read and transfer operations.
         * This buffer size must be maximum possible read value size. Otherwise read value will truncate.
         *
         * @param [in] newBufferSize        new size of the temporary internal buffer
         *
         * @par Example
         *  @code{.cpp}
         *
         *   htLib::htUart  myhtUart(htLib::htUart1,
         *                               htLib::Baud9600,
         *                               htLib::ParityEven,
         *                               htLib::StopOne,
         *                               htLib::Char8 );
         *
         *   std::cout << "Current temporary buffer size: " << myhtUart.getReadBufferSize() << std::endl;
         *   myhtUart.setReadBufferSize(2048);
         *   std::cout << "Current temporary buffer size: " << myhtUart.getReadBufferSize();
         *
         * @endcode
         * @code{.cpp}
         *   // Possible Output:
         *   // Current temporary buffer size: 1024
         *   // Current temporary buffer size: 2048
         * @endcode
         * @sa htUart::readBufferSize
         */
        void setReadBufferSize(uint32_t newBufferSize);
 
        /*! @brief Changes baud rate of htUart.
         *
         * This function changes baud rate of htUart at specified direction. Also users can select apply
         * condition like ApplyNow, ApplyDrain, ApplyFlush.
         *
         * @param [in] newBaud         new baud rate value
         * @param [in] whichDirection  direction
         * @param [in] applyMode       new value's apply condition
         * @return true if changing operation is successful, else false.
         * @warning Before use this function, users must be called open function. If htUart is not open,
         * this function returns false and sets errorUart::baudRateError and errorUart::openError flags.
         *
         * @par Example
         *  @code{.cpp}
         *
         *   htLib::htUart  myhtUart(htLib::htUart1,
         *                               htLib::Baud9600,
         *                               htLib::ParityEven,
         *                               htLib::StopOne,
         *                               htLib::Char8 );
         *
         *   myhtUart.open( htLib::ReadWrite | htLib::NonBlock );
         *
         *   // 13 means Baud9600. See the htLib::baudRate enums.
         *   std::cout << "Current baud rate: " << myhtUart.getBaudRate(htLib::output) << std::endl;
         *
         *   myhtUart.setBaudRate(htLib::Baud19200, htLib::output, htLib::ApplyNow);
         *
         *   // 14 means Baud19200. See the htLib::baudRate enums.
         *   std::cout << "Current baud rate: " << myhtUart.getBaudRate(htLib::output);
         *
         * @endcode
         * @code{.cpp}
         *   // Possible Output:
         *   // Current baud rate: 13
         *   // Current baud rate: 14
         * @endcode
         *
         * @sa baudRate
         * @sa direction
         * @sa UartApplyMode
         */
        bool setBaudRate(baudRate newBaud, direction whichDirection, UartApplyMode applyMode = ApplyNow );
 
        /*! @brief Changes parity of htUart.
         *
         * This function changes parity of htUart. Also users can select apply condition like ApplyNow,
         * ApplyDrain, ApplyFlush.
         *
         * @param [in] newParity       new parity value
         * @param [in] applyMode       new value's apply condition
         * @return true if changing operation is successful, else false.
         *
         * @warning Before use this function, users must be called open function. If htUart is not open,
         * this function returns false and sets errorUart::parityError and errorUart::openError flags.
         *
         * @par Example
         *  @code{.cpp}
         *
         *   htLib::htUart  myhtUart(htLib::htUart1,
         *                               htLib::Baud9600,
         *                               htLib::ParityEven,
         *                               htLib::StopOne,
         *                               htLib::Char8 );
         *
         *   myhtUart.open( htLib::ReadWrite | htLib::NonBlock );
         *
         *   // 2 means ParityEven. See the htLib::parity enums.
         *   std::cout << "Current parity: " << myhtUart.getParity() << std::endl;
         *
         *   myhtUart.setParity(htLib::ParityOdd, htLib::ApplyNow);
         *
         *   // 1 means ParityOdd. See the htLib::parity enums.
         *   std::cout << "Current parity: " << myhtUart.getParity();
         *
         * @endcode
         * @code{.cpp}
         *   // Possible Output:
         *   // Current parity: 2
         *   // Current parity: 1
         * @endcode
         *
         * @sa parity
         * @sa UartApplyMode
         */
        bool setParity(parity newParity, UartApplyMode applyMode = ApplyNow );
 
        /*! @brief Changes stop bits size of htUart.
         *
         * This function changes stop bits size of htUart. Also users can select apply condition like ApplyNow,
         * ApplyDrain, ApplyFlush.
         *
         * @param [in] newStopBits     new stop bits size value
         * @param [in] applyMode       new value's apply condition
         * @return true if changing operation is successful, else false.
         *
         * @warning Before use this function, users must be called open function. If htUart is not open,
         * this function returns false and sets errorUart::stopBitsError and errorUart::openError flags.
         *
         * @par Example
         *  @code{.cpp}
         *
         *   htLib::htUart  myhtUart(htLib::htUart1,
         *                               htLib::Baud9600,
         *                               htLib::ParityEven,
         *                               htLib::StopOne,
         *                               htLib::Char8 );
         *
         *   myhtUart.open( htLib::ReadWrite | htLib::NonBlock );
         *
         *   // 1 means StopOne. See the htLib::stopBits enums.
         *   std::cout << "Current stop bits size: " << myhtUart.getStopBits() << std::endl;
         *
         *   myhtUart.setStopBits(htLib::StopTwo, htLib::ApplyNow);
         *
         *   // 2 means StopTwo. See the htLib::stopBits enums.
         *   std::cout << "Current stop bits size: " << myhtUart.getStopBits();
         *
         * @endcode
         * @code{.cpp}
         *   // Possible Output:
         *   // Current stop bits size: 1
         *   // Current stop bits size: 2
         * @endcode
         *
         * @sa stopBits
         * @sa UartApplyMode
         */
        bool setStopBits(stopBits newStopBits, UartApplyMode applyMode = ApplyNow );
 
        /*! @brief Changes character size of htUart.
         *
         * This function changes character size of htUart. Also users can select apply condition like ApplyNow,
         * ApplyDrain, ApplyFlush.
         *
         * @param [in] newCharacterSize  new character size value
         * @param [in] applyMode         new value's apply condition
         * @return true if changing operation is successful, else false.
         *
         * @warning Before use this function, users must be called open function. If htUart is not open,
         * this function returns false and sets errorUart::charSizeError and errorUart::openError flags.
         *
         * @par Example
         *  @code{.cpp}
         *
         *   htLib::htUart  myhtUart(htLib::htUart1,
         *                               htLib::Baud9600,
         *                               htLib::ParityEven,
         *                               htLib::StopOne,
         *                               htLib::Char8 );
         *
         *   myhtUart.open( htLib::ReadWrite | htLib::NonBlock );
         *
         *   // 8 means Char8. See the htLib::characterSize enums.
         *   std::cout << "Current character size: " << myhtUart.getCharacterSize() << std::endl;
         *
         *   myhtUart.setCharacterSize(htLib::Char7, htLib::ApplyNow);
         *
         *   // 7 means Char7. See the htLib::characterSize enums.
         *   std::cout << "Current character size: " << myhtUart.getCharacterSize();
         *
         * @endcode
         * @code{.cpp}
         *   // Possible Output:
         *   // Current character size: 8
         *   // Current character size: 7
         * @endcode
         *
         * @sa characterSize
         * @sa UartApplyMode
         */
        bool setCharacterSize(characterSize newCharacterSize, UartApplyMode applyMode = ApplyNow );
 
        /*! @brief Changes properties of htUart.
         *
         * This function changes properties of htUart. Also users can select apply condition like ApplyNow,
         * ApplyDrain, ApplyFlush. These properties are composed of baud rate, parity, stop bits size and
         * character size.
         *
         * @param [in] &props        new properties
         * @param [in] applyMode     new properties' apply condition
         * @return true if changing operation is successful, else false.
         *
         * @warning Before use this function, users must be called open function. If htUart is not open,
         * this function returns false and sets errorUart::baudRateError, errorUart::charSizeError,
         * errorUart::parityError, errorUart::stopBitsError and errorUart::openError flags.
         *
         * @par Example
         *  @code{.cpp}
         *
         *   htLib::htUart  myhtUart(htLib::htUart1,
         *                               htLib::Baud9600,
         *                               htLib::ParityEven,
         *                               htLib::StopOne,
         *                               htLib::Char8 );
         *
         *   myhtUart.open( htLib::ReadWrite | htLib::NonBlock );
         *
         *   htLib::UartProperties currentProps = myhtUart.getProperties();
         *
         *   std::cout << "First bauds(in/out) : " << currentProps.htUartBaudIn   << "/" << currentProps.htUartBaudIn << std::endl
         *             << "First parity        : " << currentProps.htUartParity   << std::endl
         *             << "First stop bits size: " << currentProps.htUartStopBits << std::endl
         *             << "First character size: " << currentProps.htUartCharSize << std::endl;
         *
         *   currentProps = htLib::UartProperties(htLib::Baud19200, htLib::Baud19200,
         *                                                htLib::ParityOdd, htLib::StopTwo, htLib::Char7);
         *
         *   myhtUart.setProperties(currentProps, htLib::ApplyNow);
         *
         *   std::cout << "Second bauds(in/out) : " << currentProps.htUartBaudIn   << "/" << currentProps.htUartBaudIn << std::endl
         *             << "Second parity        : " << currentProps.htUartParity   << std::endl
         *             << "Second stop bits size: " << currentProps.htUartStopBits << std::endl
         *             << "Second character size: " << currentProps.htUartCharSize << std::endl;
         *
         * @endcode
         * @code{.cpp}
         *   // Possible Output:
         *   // First bauds(in/out) : 13/13
         *   // First parity        : 2
         *   // First stop bits size: 1
         *   // First character size: 8
         *   // Second bauds(in/out) : 14/14
         *   // Second parity        : 1
         *   // Second stop bits size: 2
         *   // Second character size: 7
         * @endcode
         *
         * @sa UartProperties
         * @sa UartApplyMode
         */
        bool setProperties(UartProperties &props, UartApplyMode applyMode = ApplyNow );
 
        /*! @brief Exports properties of htUart.
         *
         * This function gets properties of htUart. These properties are composed of baud rate, parity, stop bits size and
         * character size.
         *
         * @return htUart::currentUartProperties struct with updated values.
         *
         * @par Example
         *   Example usage is shown in htUart::setProperties() function's example.
         *
         * @sa UartProperties
         */
        UartProperties getProperties();
 
        /*! @brief Exports htUart's port path.
         *
         * @return htUart's port path as string.
         *
         * @par Example
         *  @code{.cpp}
         *
         *   htLib::htUart  myhtUart(htLib::htUart1,
         *                               htLib::Baud9600,
         *                               htLib::ParityEven,
         *                               htLib::StopOne,
         *                               htLib::Char8 );
         *
         *   myhtUart.open( htLib::ReadWrite | htLib::NonBlock );
         *
         *   std::cout << "Port path: " << myhtUart.getPortName();
         *
         * @endcode
         * @code{.cpp}
         *   // Possible Output:
         *   // Port path: /dev/ttyO1
         * @endcode
         *
         * @sa htUartPortPath
         */
        std::string getPortName();
 
        /*! @brief Exports internal temporary buffers' size.
         *
         * @return size of internal temporary buffers.
         *
         * @par Example
         *   Example usage is shown in htUart::setReadBufferSize() function's example.
         *
         * @sa readBufferSize
         */
        uint32_t getReadBufferSize();
 
        /*! @brief Exports baud rate value of htUart.
         *
         * This function Exports baud rate value of htUart at specified direction.
         *
         * @param [in] whichDirection  direction
         * @return baud rate value of htUart.
         *
         * @par Example
         *   Example usage is shown in htUart::setBaudRate() function's example.
         *
         * @sa baudRate
         * @sa direction
         */
        baudRate getBaudRate(direction whichDirection);
 
        /*! @brief Exports parity value of htUart.
         *
         * @return parity value of htUart.
         *
         * @par Example
         *   Example usage is shown in htUart::setParity() function's example.
         *
         * @sa parity
         */
        parity getParity();
 
        /*! @brief Exports stop bits size value of htUart.
         *
         * @return stop bits size value of htUart.
         *
         * @par Example
         *   Example usage is shown in htUart::setStopBits() function's example.
         *
         * @sa stopBits
         */
        stopBits getStopBits();
 
        /*! @brief Exports character size value of htUart.
         *
         * @return character size value of htUart.
         *
         * @par Example
         *   Example usage is shown in htUart::setCharacterSize() function's example.
         *
         * @sa characterSize
         */
        characterSize getCharacterSize();
 
        /*! @brief Checks htUart's tty file's open state.
         *
         * @return true if tty file is open, else false.
         *
         * @par Example
         *  @code{.cpp}
         *
         *   htLib::htUart  myhtUart(htLib::htUart1,
         *                               htLib::Baud9600,
         *                               htLib::ParityEven,
         *                               htLib::StopOne,
         *                               htLib::Char8 );
         *
         *   std::cout << "Is open?: " << std::boolalpha << myhtUart.isOpen() << std::endl;
         *
         *   myhtUart.open( htLib::ReadWrite | htLib::NonBlock );
         *
         *   std::cout << "Is open?: " << std::boolalpha << myhtUart.isOpen();
         *
         * @endcode
         * @code{.cpp}
         *   // Possible Output:
         *   // Is open?: false
         *   // Is open?: true
         * @endcode
         *
         * @sa htUart::isOpenFlag
         */
        bool isOpen();
 
        /*! @brief Checks htUart's tty file's close state.
         *
         * @return true if tty file is close, else false.
         *
         * @par Example
         *  @code{.cpp}
         *
         *   htLib::htUart  myhtUart(htLib::htUart1,
         *                               htLib::Baud9600,
         *                               htLib::ParityEven,
         *                               htLib::StopOne,
         *                               htLib::Char8 );
         *
         *   std::cout << "Is close?: " << std::boolalpha << myhtUart.isClose() << std::endl;
         *
         *   myhtUart.open( htLib::ReadWrite | htLib::NonBlock );
         *
         *   std::cout << "Is close?: " << std::boolalpha << myhtUart.isClose();
         *
         * @endcode
         * @code{.cpp}
         *   // Possible Output:
         *   // Is close?: true
         *   // Is close?: false
         * @endcode
         *
         * @sa htUart::isOpenFlag
         */
        bool isClose();
 
        /*! @brief Is used for general debugging.
         *
         * @return True if any error occured, else false.
         *
         * @par Example
         *  @code{.cpp}
         *   htLib::htUart  myhtUart(htLib::htUart1,
         *                               htLib::Baud9600,
         *                               htLib::ParityEven,
         *                               htLib::StopOne,
         *                               htLib::Char8 );
         *
         *   myhtUart.open( htLib::ReadWrite );
         *
         *   if( myhtUart.fail() )
         *   {
         *       std::cout << "ERROR OCCURED" << std::endl;
         *   }
         *   else
         *   {
         *       std::cout << "EVERYTHING IS OK" << std::endl;
         *   }
         *
         * @endcode
         * @code{.cpp}
         *   // Possible Output:
         *   // EVERYTHING IS OK
         * @endcode
         *
         * @sa errorUart
         */
        bool fail();
 
        /*! @brief Is used for specific debugging.
         *
         * You can use this function, after call htUart member functions in your code. The
         * input parameter is used for finding out status of selected error.
         * @param [in] f specific error type (enum)
         * @return Value of @a selected error.
         *
         * @par Example
         *  @code{.cpp}
         *   htLib::htUart  myhtUart(htLib::htUart1,
         *                               htLib::Baud9600,
         *                               htLib::ParityEven,
         *                               htLib::StopOne,
         *                               htLib::Char8 );
         *
         *
         *   if( myhtUart.fail(htLib::htUart::dtErr) )
         *   {
         *       std::cout << "htUart INITIALIZATION FAILED" << std::endl;
         *   }
         *   else
         *   {
         *       std::cout << "htUart INITIALIZATION IS OK" << std::endl;
         *   }
         *
         *
         *   myhtUart.open( htLib::ReadWrite | htLib::NonBlock  );
         *
         *   if( myhtUart.fail(htLib::htUart::openErr) )
         *   {
         *       std::cout << "OPENNING ERROR OCCURED" << std::endl;
         *   }
         *   else
         *   {
         *       std::cout << "OPENNING IS OK" << std::endl;
         *   }
         *
         *
         *   myhtUart.flush( htLib::bothDirection );
         *
         *   if( myhtUart.fail(htLib::htUart::directionErr) or myhtUart.fail(htLib::htUart::flushErr) )
         *   {
         *       std::cout << "FLUSHING ERROR OCCURED" << std::endl;
         *   }
         *   else
         *   {
         *       std::cout << "FLUSHING IS OK" << std::endl;
         *   }
         *
         *   std::string writeThis = "Loopback test message.";
         *   std::string readThis  = myhtUart.transfer( writeThis, 40000 );
         *
         *   if( myhtUart.fail(htLib::htUart::readErr) or myhtUart.fail(htLib::htUart::writeErr) )
         *   {
         *       std::cout << "TRANSFER ERROR OCCURED" << std::endl;
         *   }
         *   else
         *   {
         *       std::cout << "TRANSFER IS OK" << std::endl;
         *   }
         *
         * @endcode
         * @code{.cpp}
         *   // Possible Output:
         *   // htUart INITIALIZATION IS OK
         *   // OPENNING IS OK
         *   // FLUSHING IS OK
         *   // TRANSFER IS OK
         * @endcode
         *
         * @sa errorUart
         */
        bool fail(htUart::flags f);
 
        /*! @brief Writes values to htUart line with "<<" operator.
         *
         *  This function writes values to htUart line. Values sent to this function as string type.
         *  @param [in] &writeFromThis to htUart
         *
         *  @par Example
         *  @code{.cpp}
         *
         *   htLib::htUart  myhtUart(htLib::htUart1,
         *                               htLib::Baud9600,
         *                               htLib::ParityEven,
         *                               htLib::StopOne,
         *                               htLib::Char8 );
         *
         *   myhtUart.open( htLib::ReadWrite | htLib::NonBlock );
         *   myhtUart.flush( htLib::bothDirection );
         *
         *   std::string writeTohtUart  = "this is test.\n";
         *   myhtUart << writeTohtUart;
         *
         *   sleep(1);
         *
         *   std::string readFromhtUart;
         *   myhtUart >> readFromhtUart;
         *
         *   std::cout << "Test output on loopback: " << readFromhtUart;
         *
         * @endcode
         * @code{.cpp}
         *   // Possible Output:
         *   // Test output on loopback: this is test.
         * @endcode
         */
        htUart& operator<<(std::string &writeFromThis);
 
        /*! @brief Reads values from htUart line with ">>" operator.
         *
         *  This function reads values from htUart line with ">>" operator. It creates temporary read
         *  buffer with specified size and reads values to this buffer. Then resize buffer to read
         *  value size. htUart::readBufferSize variable is used to specify temporary buffer size.
         *  @param [in] &readToThis from htUart. If reading fails, this functions sets
         *  htLib::htUart_READ_FAILED string to this variable.
         *
         *  @par Example
         *  @code{.cpp}
         *
         *   htLib::htUart  myhtUart(htLib::htUart1,
         *                               htLib::Baud9600,
         *                               htLib::ParityEven,
         *                               htLib::StopOne,
         *                               htLib::Char8 );
         *
         *   myhtUart.open( htLib::ReadWrite | htLib::NonBlock );
         *   myhtUart.flush( htLib::bothDirection );
         *
         *   std::string writeTohtUart  = "this is test.\n";
         *   myhtUart << writeTohtUart;
         *
         *   sleep(1);
         *
         *   std::string readFromhtUart;
         *   myhtUart >> readFromhtUart;
         *
         *   std::cout << "Test output on loopback: " << readFromhtUart;
         *
         * @endcode
         * @code{.cpp}
         *   // Possible Output:
         *   // Test output on loopback: this is test.
         * @endcode
         */
        htUart& operator>>(std::string &readToThis);
    };
}


namespace htLib
{
    htUart::htUart(UartName htUart, baudRate htUartBaud, parity htUartParity, stopBits htUartStopBits, characterSize htUartCharSize)
    {
        this->htUartPortPath              = "/dev/ttyO" + tostr(static_cast<int>(htUart));
        this->readBufferSize            = 1024;
        this->htUartFd                    = -1;
        this->isOpenFlag                = false;
        this->isCurrentEqDefault        = false;
        this->htUartErrors                = errorUart();
        this->constructorProperties     = UartProperties(htUartBaud, htUartBaud, htUartParity, htUartStopBits, htUartCharSize);
    }
    
    htUart::htUart(std::string htUartPortPath, baudRate htUartBaud, parity htUartParity, stopBits htUartStopBits, characterSize htUartCharSize)
    {
        this->htUartPortPath              = htUartPortPath;
        this->readBufferSize            = 1024;
        this->htUartFd                    = -1;
        this->isOpenFlag                = false;
        this->isCurrentEqDefault        = false;
        this->htUartErrors                = errorUart();
        this->constructorProperties     = UartProperties(htUartBaud, htUartBaud, htUartParity, htUartStopBits, htUartCharSize);
    }
    htUart::htUart(std::string htUartPortPath, UartProperties UartProperties)
    {
        
        this->htUartPortPath              = htUartPortPath;
        this->readBufferSize            = 1024;
        this->htUartFd                    = -1;
        this->isOpenFlag                = false;
        this->isCurrentEqDefault        = false;
        this->htUartErrors                = errorUart();
        this->constructorProperties     = UartProperties;
    }
    htUart::htUart(UartName htUart, UartProperties UartProperties)
    {
        
        this->htUartPortPath              = "/dev/ttyO" + tostr(static_cast<int>(htUart));
        this->readBufferSize            = 1024;
        this->htUartFd                    = -1;
        this->isOpenFlag                = false;
        this->isCurrentEqDefault        = false;
        this->htUartErrors                = errorUart();
        this->constructorProperties     = UartProperties;
    }
    
    htUart::htUart(std::string htUartPortPath)
    {
        
        this->htUartPortPath              = htUartPortPath;
        this->readBufferSize            = 1024;
        this->htUartFd                    = -1;
        this->isOpenFlag                = false;
        this->isCurrentEqDefault        = true;
        this->htUartErrors                = errorUart();
    }
    htUart::htUart(UartName htUart)
    {
        
        this->htUartPortPath              = "/dev/ttyO" + tostr(static_cast<int>(htUart));
        this->readBufferSize            = 1024;
        this->htUartFd                    = -1;
        this->isOpenFlag                = false;
        this->isCurrentEqDefault        = true;
        this->htUartErrors                = errorUart();
    }
    htUart::~htUart()
    {
        this->close();
    }
    bool htUart::open(uint openMode)
    {
        int flags = 0;
        if( (openMode & ReadOnly)   == ReadOnly     ){  flags |= O_RDONLY;  }
        if( (openMode & WriteOnly)  == WriteOnly    ){  flags |= O_WRONLY;  }
        if( (openMode & ReadWrite)  == ReadWrite    ){  flags |= O_RDWR;    }
        if( (openMode & Append)     == Append       ){  flags |= O_APPEND;  }
        if( (openMode & Truncate)   == Truncate     ){  flags |= O_TRUNC;   }
        if( (openMode & NonBlock)   == NonBlock     ){  flags |= O_NONBLOCK;}
        this->htUartFd = ::open(this->htUartPortPath.c_str(), flags | O_NOCTTY);
        if( this->htUartFd < 0 )
        {
            this->htUartErrors.openError = true;
            this->isOpenFlag            = false;
            return false;
        }
        this->htUartErrors.openError     = false;
        this->isOpenFlag                = true;
        this->defaultUartProperties     = this->getProperties();
        if( this->isCurrentEqDefault )
        {
            this->currentUartProperties = this->defaultUartProperties;
            return true;
        }
        else
        {
            if( this->setProperties(this->constructorProperties , ApplyNow) )
            {
                this->currentUartProperties = this->constructorProperties;
            }
            else
            {
                this->currentUartProperties = this->defaultUartProperties;
            }
        }
        return true;
    }
    bool htUart::close()
    {
        if( ::close(this->htUartFd) < 0 )
        {
            this->htUartErrors.closeError    = true;
            return false;
        }
        else
        {
            this->htUartErrors.closeError    = false;
            this->isOpenFlag                = false;
            return true;
        }
    }
    bool htUart::flush(direction whichDirection)
    {
        int isFlushed = -1;
        if( whichDirection == input )
        {
            this->htUartErrors.directionError = false;
            isFlushed = tcflush(this->htUartFd, TCIFLUSH);
        }
        else if( whichDirection == output )
        {
            this->htUartErrors.directionError = false;
            isFlushed = tcflush(this->htUartFd, TCOFLUSH);
        }
        else if( whichDirection == bothDirection )
        {
            this->htUartErrors.directionError = false;
            isFlushed = tcflush(this->htUartFd, TCIOFLUSH);
        }
        else
        {
            this->htUartErrors.directionError = true;
        }
        if( isFlushed == 0 )
        {
            this->htUartErrors.flushError = false;
            return true;
        }
        else
        {
            this->htUartErrors.flushError = true;
            return false;
        }
    }
    bool htUart::read(char *readBuffer, size_t size)
    {
        char tempReadBuffer[size];
        memset(&tempReadBuffer, 0, size);
        
        if(::read(this->htUartFd, tempReadBuffer, sizeof(tempReadBuffer)) > 0)
        {
            memcpy(readBuffer, tempReadBuffer, sizeof(tempReadBuffer));
            this->htUartErrors.readError = false;
            return true;
        }
        else
        {
            this->htUartErrors.readError = true;
            return false;
        }
    }
    
    size_t htUart::readbytes(char *readBuffer, size_t size)
    {
        char tempReadBuffer[size];
        memset(&tempReadBuffer, 0, size);
        size_t num = ::read(this->htUartFd, tempReadBuffer, sizeof(tempReadBuffer));
        if(num > 0)
        {
            memcpy(readBuffer, tempReadBuffer, sizeof(tempReadBuffer));
            this->htUartErrors.readError = false;
            return num;
        }
        else
        {
            this->htUartErrors.readError = true;
            return num;
        }
    }

    size_t htUart::readbytes(uint8_t *readBuffer, size_t size)
    {
        uint8_t tempReadBuffer[size];
        memset(&tempReadBuffer, 0, size);
        size_t num = ::read(this->htUartFd, tempReadBuffer, sizeof(tempReadBuffer));
        if(num > 0)
        {
            memcpy(readBuffer, tempReadBuffer, sizeof(tempReadBuffer));
            this->htUartErrors.readError = false;
            return num;
        }
        else
        {
            this->htUartErrors.readError = true;
            return num;
        }
    }
    std::string htUart::read()
    {
        std::string tempReadBuffer;
        tempReadBuffer.resize(this->readBufferSize);
        int readSize = ::read(this->htUartFd, &tempReadBuffer[0], tempReadBuffer.size() );
        if( readSize > 0)
        {
            this->htUartErrors.readError = false;
            tempReadBuffer.resize(readSize);
            return tempReadBuffer;
        }
        else
        {
            this->htUartErrors.readError = true;
            return UART_READ_FAILED;
        }
    }
    // Read a line from htUart.
    // Return a 0 len string in case of problems with htUart
    size_t htUart::readln(char *buffer, size_t maxlen)
    {
        char c;
        //char *b;
        int rx_length = -1;
        int index = 0;
        while(index < maxlen) 
        {
            rx_length = ::read(this->htUartFd, (void*)(&c), 1);
            if (rx_length <= 0) 
            {
                break;
                //wait for messages
                //sleep(1);
            } 
            else 
            {
                if (c == '\n') 
                {
                  //*b++ = '\0';
                  
                  break;
                }
                //*b++ = c;
                buffer[index] = c;
                index++;
            }
        }
        return index;
    }

    size_t htUart::readlnCR(char *buffer, size_t maxlen)
    {
        uint8_t c;
        //char *b;
        int rx_length = -1;
        int index = 0;
        while(index < maxlen)
        {
            rx_length = ::read(this->htUartFd, &c, 1);
            if (rx_length <= 0) 
            {
                break;
                //wait for messages
                //sleep(1);
            } 
            else 
            {
                if (c == 0x0D) 
                {
                  //*b++ = '\0';
                  buffer[index] = c;
                  index++;
                  break;
                }
                //*b++ = c;
                buffer[index] = c;
                index++;
            }
        }
        return index;
    }
    size_t htUart::writebytes(char *writeBuffer, size_t size)
    {
        size_t num = ::write(this->htUartFd, writeBuffer, size);
        if(num > 0)
        {
            this->htUartErrors.writeError = false;
            return num;
        }
        else
        {
            this->htUartErrors.writeError = true;
            return num;
        }
    }
    
    bool htUart::write(char *writeBuffer, size_t size)
    {
        if(::write(this->htUartFd, writeBuffer, size) > 0)
        {
            this->htUartErrors.writeError = false;
            return true;
        }
        else
        {
            this->htUartErrors.writeError = true;
            return false;
        }
    }
    bool htUart::write(std::string writeBuffer)
    {
        if(::write(this->htUartFd, writeBuffer.c_str(), writeBuffer.size() ) > 0)
        {
            this->htUartErrors.writeError = false;
            return true;
        }
        else
        {
            this->htUartErrors.writeError = true;
            return false;
        }
    }
    bool htUart::transfer(char *writeBuffer, char *readBuffer, size_t size, uint32_t wait_us)
    {
        if(::write(this->htUartFd, writeBuffer, size ) > 0)
        {
            this->htUartErrors.writeError = false;
        }
        else
        {
            this->htUartErrors.writeError = true;
            return false;
        }
        usleep(wait_us);
        char tempReadBuffer[ size ];
        memset(&tempReadBuffer, 0, size);
        if(::read(this->htUartFd, tempReadBuffer, sizeof(tempReadBuffer)) > 0)
        {
            memcpy(readBuffer,tempReadBuffer,sizeof(tempReadBuffer));
            this->htUartErrors.readError = false;
            return true;
        }
        else
        {
            this->htUartErrors.readError = true;
            return false;
        }
    }
    std::string htUart::transfer(std::string writeBuffer, uint32_t wait_us)
    {
        if(::write(this->htUartFd,writeBuffer.c_str(),writeBuffer.size() ) > 0)
        {
            this->htUartErrors.writeError = false;
        }
        else
        {
            this->htUartErrors.writeError = true;
            return UART_WRITE_FAILED;
        }
        usleep(wait_us);
        std::string tempReadBuffer;
        tempReadBuffer.resize(this->readBufferSize);
        int readSize = ::read(this->htUartFd, &tempReadBuffer[0], tempReadBuffer.size() );
        if( readSize > 0 )
        {
            this->htUartErrors.readError = false;
            tempReadBuffer.resize(readSize);
            return tempReadBuffer;
        }
        else
        {
            this->htUartErrors.readError = true;
            return UART_READ_FAILED;
        }
    }
    uint32_t htUart::getReadBufferSize()
    {
        return this->readBufferSize;
    }
    void htUart::setReadBufferSize(uint32_t newBufferSize)
    {
        this->readBufferSize = newBufferSize;
    }
    std::string htUart::getPortName()
    {
        return this->htUartPortPath;
    }
    
    baudRate htUart::getBaudRate(direction whichDirection)
    {
        if( !(this->isOpenFlag) )
        {
            this->htUartErrors.baudRateError = true;
            this->htUartErrors.openError     = true;
            return Baud0;
        }
        termios tempProperties;
        if( tcgetattr(this->htUartFd, &tempProperties) != 0 )
        {
            this->htUartErrors.baudRateError = true;
            return Baud0;
        }
        else
        {
            this->htUartErrors.baudRateError = false;
        }
        if( whichDirection == input )
        {
            this->currentUartProperties.htUartBaudIn = static_cast<baudRate>(cfgetispeed(&tempProperties));
            return this->currentUartProperties.htUartBaudIn;
        }
        if( whichDirection == output )
        {
            this->currentUartProperties.htUartBaudOut = static_cast<baudRate>(cfgetospeed(&tempProperties));
            return this->currentUartProperties.htUartBaudOut;
        }
        else
        {
            return Baud0;
        }
    }
    bool htUart::setBaudRate(baudRate newBaud, direction whichDirection, UartApplyMode applyMode )
    {
        if( !(this->isOpenFlag) )
        {
            this->htUartErrors.baudRateError = true;
            this->htUartErrors.openError     = true;
            return false;
        }
        termios tempProperties;
        tcgetattr(this->htUartFd, &tempProperties);
        if( whichDirection == input)
        {
            cfsetispeed(&tempProperties, newBaud);
        }
        else
        if( whichDirection == output )
        {
            cfsetospeed(&tempProperties, newBaud);
        }
        else
        if( whichDirection == bothDirection )
        {
            cfsetispeed(&tempProperties, newBaud);
            cfsetospeed(&tempProperties, newBaud);
        }
        else
        {
            this->htUartErrors.directionError    = true;
            this->htUartErrors.baudRateError     = true;
            return false;
        }
        this->htUartErrors.directionError        = false;
        if( tcsetattr(this->htUartFd, applyMode, &tempProperties) == 0 )
        {
            if( whichDirection == input)
            {
                this->currentUartProperties.htUartBaudIn = newBaud;
            }
            else
            if( whichDirection == output )
            {
                this->currentUartProperties.htUartBaudOut = newBaud;
            }
            else
            if( whichDirection == bothDirection )
            {
                this->currentUartProperties.htUartBaudIn = newBaud;
                this->currentUartProperties.htUartBaudOut = newBaud;
            }
            this->htUartErrors.baudRateError = false;
            return true;
        }
        else
        {
            this->htUartErrors.baudRateError = true;
            return false;
        }
    }
    parity htUart::getParity()
    {
        if( !(this->isOpenFlag) )
        {
            this->htUartErrors.parityError   = true;
            this->htUartErrors.openError     = true;
            return ParityDefault;
        }
        termios tempProperties;
        if( tcgetattr(this->htUartFd, &tempProperties) != 0 )
        {
            this->htUartErrors.parityError = true;
            return ParityDefault;
        }
        else
        {
            this->htUartErrors.parityError = false;
        }
        tcflag_t controlFlag = tempProperties.c_cflag;
        if( (controlFlag & PARENB)      == PARENB )
        {
            if( (controlFlag & PARODD)  == PARODD )
            {
                this->currentUartProperties.htUartParity = ParityOdd;
                return ParityOdd;
            }
            else
            {
                this->currentUartProperties.htUartParity = ParityEven;
                return ParityEven;
            }
        }
        else
        {
            this->currentUartProperties.htUartParity = ParityNo;
            return ParityNo;
        }
    }
    bool htUart::setParity(parity newParity, UartApplyMode applyMode )
    {
        if( !(this->isOpenFlag) )
        {
            this->htUartErrors.parityError   = true;
            this->htUartErrors.openError     = true;
            return false;
        }
        termios tempProperties;
        tcgetattr(this->htUartFd, &tempProperties);
        if( (newParity == ParityOdd) or (newParity == ParityEven) )
        {
            tempProperties.c_cflag |= PARENB;
            if( newParity == ParityOdd )
            {
                tempProperties.c_cflag |= PARODD;
            }
            else
            {
                tempProperties.c_cflag &= ~(PARODD);
            }
        }
        else
        {
            tempProperties.c_cflag &= ~(PARENB);
        }
        if( tcsetattr(this->htUartFd, applyMode, &tempProperties) == 0 )
        {
            this->htUartErrors.parityError = false;
            this->currentUartProperties.htUartParity = (newParity == ParityDefault) ? ParityNo : newParity;;
            return true;
        }
        else
        {
            this->htUartErrors.parityError = true;
            return false;
        }
    }
    stopBits htUart::getStopBits()
    {
        if( !(this->isOpenFlag) )
        {
            this->htUartErrors.stopBitsError = true;
            this->htUartErrors.openError     = true;
            return StopDefault;
        }
        termios tempProperties;
        if( tcgetattr(this->htUartFd, &tempProperties) != 0 )
        {
            this->htUartErrors.stopBitsError = true;
            return StopDefault;
        }
        else
        {
            this->htUartErrors.stopBitsError = false;
        }
        if( (tempProperties.c_cflag & CSTOPB) == CSTOPB )
        {
            this->currentUartProperties.htUartStopBits = StopTwo;
            return StopTwo;
        }
        else
        {
            this->currentUartProperties.htUartStopBits = StopOne;
            return StopOne;
        }
    }
    bool htUart::setStopBits(stopBits newStopBits, UartApplyMode applyMode )
    {
        if( !(this->isOpenFlag) )
        {
            this->htUartErrors.stopBitsError = true;
            this->htUartErrors.openError     = true;
            return false;
        }
        termios tempProperties;
        tcgetattr(this->htUartFd, &tempProperties);
        if( newStopBits == StopTwo)
        {
            tempProperties.c_cflag |= CSTOPB;
        }
        else
        {
            tempProperties.c_cflag &= ~(CSTOPB);
        }
        if( tcsetattr(this->htUartFd, applyMode, &tempProperties) == 0 )
        {
            this->htUartErrors.stopBitsError = false;
            this->currentUartProperties.htUartStopBits = (newStopBits == StopDefault) ? StopOne : newStopBits;
            return true;
        }
        else
        {
            this->htUartErrors.stopBitsError = true;
            return false;
        }
    }
    characterSize htUart::getCharacterSize()
    {
        if( !(this->isOpenFlag) )
        {
            this->htUartErrors.charSizeError = true;
            this->htUartErrors.openError     = true;
            return CharDefault;
        }
        termios tempProperties;
        if( tcgetattr(this->htUartFd, &tempProperties) != 0 )
        {
            this->htUartErrors.charSizeError = true;
            return CharDefault;
        }
        else
        {
            this->htUartErrors.charSizeError = false;
        }
        tcflag_t controlFlag = tempProperties.c_cflag;
        if( (controlFlag & CS8) == CS8  ){ this->currentUartProperties.htUartCharSize = Char8; return Char8; }
        if( (controlFlag & CS6) == CS6  ){ this->currentUartProperties.htUartCharSize = Char6; return Char6; }
        if( (controlFlag & CS7) == CS7  ){ this->currentUartProperties.htUartCharSize = Char7; return Char7; }
        else                             { this->currentUartProperties.htUartCharSize = Char5; return Char5; }
    }
    bool htUart::setCharacterSize(characterSize newCharacterSize, UartApplyMode applyMode )
    {
        if( !(this->isOpenFlag) )
        {
            this->htUartErrors.charSizeError = true;
            this->htUartErrors.openError     = true;
            return false;
        }
        termios tempProperties;
        tcgetattr(this->htUartFd, &tempProperties);
        switch (newCharacterSize)
        {
            case Char5:
            {
                tempProperties.c_cflag &= ~(CSIZE);
                tempProperties.c_cflag |= CS5;
                break;
            }
            case Char6:
            {
                tempProperties.c_cflag &= ~(CSIZE);
                tempProperties.c_cflag |= CS6;
                break;
            }
            case Char7:
            {
                tempProperties.c_cflag &= ~(CSIZE);
                tempProperties.c_cflag |= CS7;
                break;
            }
            case Char8:
            {
                tempProperties.c_cflag &= ~(CSIZE);
                tempProperties.c_cflag |= CS8;
                break;
            }
            case CharDefault:
            {
                tempProperties.c_cflag &= ~(CSIZE);
                tempProperties.c_cflag |= CS8;
                break;
            }
            default:
                break;
        }
        if( tcsetattr(this->htUartFd, applyMode, &tempProperties) == 0 )
        {
            this->currentUartProperties.htUartCharSize = (newCharacterSize == CharDefault) ? Char8 : newCharacterSize;
            this->htUartErrors.charSizeError = false;
            return true;
        }
        else
        {
            this->htUartErrors.charSizeError = true;
            return false;
        }
    }
    UartProperties htUart::getProperties()
    {
        if( !(this->isOpenFlag) )
        {
            this->htUartErrors.baudRateError = true;
            this->htUartErrors.charSizeError = true;
            this->htUartErrors.parityError   = true;
            this->htUartErrors.stopBitsError = true;
            this->htUartErrors.openError     = true;
            return UartProperties();
        }
        termios tempProperties;
        if( tcgetattr(this->htUartFd, &tempProperties) != 0 )
        {
            this->htUartErrors.baudRateError = true;
            this->htUartErrors.charSizeError = true;
            this->htUartErrors.parityError   = true;
            this->htUartErrors.stopBitsError = true;
        }
        else
        {
            this->htUartErrors.baudRateError = false;
            this->htUartErrors.charSizeError = false;
            this->htUartErrors.parityError   = false;
            this->htUartErrors.stopBitsError = false;
        }
        tcflag_t controlFlag = tempProperties.c_cflag;
        baudRate        currentBaudIn   = static_cast<baudRate>(cfgetispeed(&tempProperties));
        baudRate        currentBaudOut  = static_cast<baudRate>(cfgetospeed(&tempProperties));
        stopBits        currentStopBits = ((controlFlag & CSTOPB)==CSTOPB) ? StopTwo : StopOne;
        parity          currentParity   = ((controlFlag & PARENB)==PARENB) ? ( ((controlFlag & PARODD)==PARODD) ? ParityOdd : ParityEven ) : ParityNo;
        characterSize   currentCharSize;
        if     ( (controlFlag & CS8) == CS8  ){ currentCharSize = Char8; }
        else if( (controlFlag & CS6) == CS6  ){ currentCharSize = Char6; }
        else if( (controlFlag & CS7) == CS7  ){ currentCharSize = Char7; }
        else                                  { currentCharSize = Char5; }
        this->currentUartProperties = UartProperties(currentBaudIn, currentBaudOut, currentParity, currentStopBits, currentCharSize);
        return this->currentUartProperties;
    }
    bool htUart::setProperties(UartProperties &props, UartApplyMode applyMode)
    {
        if( !(this->isOpenFlag) )
        {
            this->htUartErrors.baudRateError = true;
            this->htUartErrors.charSizeError = true;
            this->htUartErrors.parityError   = true;
            this->htUartErrors.stopBitsError = true;
            this->htUartErrors.openError     = true;
            return false;
        }
        termios tempProperties;
        //memset(&tempProperties,0,sizeof(tempProperties));
        tcgetattr(this->htUartFd, &tempProperties);
        cfsetispeed(&tempProperties, props.htUartBaudIn);
        cfsetospeed(&tempProperties, props.htUartBaudOut);
        if( (props.htUartParity == ParityOdd) or (props.htUartParity == ParityEven) )
        {
            tempProperties.c_cflag |= PARENB;
            if( props.htUartParity == ParityOdd )
            {
                tempProperties.c_cflag |= PARODD;
            }
            else
            {
                tempProperties.c_cflag &= ~(PARODD);
            }
        }
        else
        {
            tempProperties.c_cflag &= ~(PARENB);
        }
        if( props.htUartStopBits == StopTwo)
        {
            tempProperties.c_cflag |= CSTOPB;
        }
        else
        {
            tempProperties.c_cflag &= ~(CSTOPB);
        }
        switch (props.htUartCharSize)
        {
            case Char5:
            {
                tempProperties.c_cflag &= ~(CSIZE);
                tempProperties.c_cflag |= CS5;
                break;
            }
            case Char6:
            {
                tempProperties.c_cflag &= ~(CSIZE);
                tempProperties.c_cflag |= CS6;
                break;
            }
            case Char7:
            {
                tempProperties.c_cflag &= ~(CSIZE);
                tempProperties.c_cflag |= CS7;
                break;
            }
            case Char8:
            {
                tempProperties.c_cflag &= ~(CSIZE);
                tempProperties.c_cflag |= CS8;
                break;
            }
            case CharDefault:
            {
                tempProperties.c_cflag &= ~(CSIZE);
                tempProperties.c_cflag |= CS8;
                break;
            }
            default:
                break;
        }
        tempProperties.c_cflag |= CLOCAL;
        tempProperties.c_cflag |= CREAD;
        //tempProperties.c_iflag |= ICRNL;
        //tempProperties.c_cflag &= ~INLCR;
        tempProperties.c_iflag &= ~(ICRNL | IGNCR );

        tempProperties.c_cflag &= ~ICRNL;
        tempProperties.c_oflag = 0;
        tempProperties.c_lflag = 0;
        tempProperties.c_cc[VTIME] = 0;
        tempProperties.c_cc[VMIN]  = 1;
        if( tcsetattr(this->htUartFd, applyMode, &tempProperties) == 0 )
        {
            this->htUartErrors.baudRateError = false;
            this->htUartErrors.charSizeError = false;
            this->htUartErrors.parityError   = false;
            this->htUartErrors.stopBitsError = false;
            this->currentUartProperties.htUartBaudIn   = props.htUartBaudIn;
            this->currentUartProperties.htUartBaudOut  = props.htUartBaudOut;
            this->currentUartProperties.htUartCharSize = (props.htUartCharSize == CharDefault)   ? Char8    : props.htUartCharSize;
            this->currentUartProperties.htUartParity   = (props.htUartParity   == ParityDefault) ? ParityNo : props.htUartParity;
            this->currentUartProperties.htUartStopBits = (props.htUartStopBits == StopDefault)   ? StopOne  : props.htUartStopBits;
            return true;
        }
        else
        {
            this->htUartErrors.baudRateError = true;
            this->htUartErrors.charSizeError = true;
            this->htUartErrors.parityError   = true;
            this->htUartErrors.stopBitsError = true;
            return false;
        }
    }
    bool htUart::isOpen()
    {
        return this->isOpenFlag;
    }
    bool htUart::isClose()
    {
        return !(this->isOpenFlag);
    }
    bool htUart::fail()
    {
        return (
                this->htUartErrors.readError or
                this->htUartErrors.writeError or
                this->htUartErrors.flushError or
                this->htUartErrors.openError or
                this->htUartErrors.closeError or
                this->htUartErrors.directionError or
                this->htUartErrors.baudRateError or
                this->htUartErrors.charSizeError or
                this->htUartErrors.stopBitsError or
                this->htUartErrors.parityError
                );
    }
    bool htUart::fail(htUart::flags f)
    {
    
        if(f==readErr)          { return this->htUartErrors.readError;       }
        if(f==writeErr)         { return this->htUartErrors.writeError;      }
        if(f==flushErr)         { return this->htUartErrors.flushError;      }
        if(f==closeErr)         { return this->htUartErrors.closeError;      }
        if(f==openErr)          { return this->htUartErrors.openError;       }
        if(f==directionErr)     { return this->htUartErrors.directionError;  }
        if(f==parityErr)        { return this->htUartErrors.parityError;     }
        if(f==baudRateErr)      { return this->htUartErrors.baudRateError;   }
        if(f==charSizeErr)      { return this->htUartErrors.charSizeError;   }
        if(f==stopBitsErr)      { return this->htUartErrors.stopBitsError;   }
        return true;
    }
    htUart& htUart::operator<<(std::string &writeFromThis)
    {
        if(::write(this->htUartFd,writeFromThis.c_str(),writeFromThis.size() ) > 0)
        {
            this->htUartErrors.writeError = false;
        }
        else
        {
            this->htUartErrors.writeError = true;
        }
        return *this;
    }
    htUart& htUart::operator>>(std::string &readToThis)
    {
        std::string tempReadBuffer;
        tempReadBuffer.resize(this->readBufferSize);
        int readSize = ::read(this->htUartFd, &tempReadBuffer[0], tempReadBuffer.size() );
        if( readSize > 0)
        {
            this->htUartErrors.readError = false;
            tempReadBuffer.resize(readSize);
            readToThis = tempReadBuffer;
        }
        else
        {
            this->htUartErrors.readError = true;
            readToThis = UART_READ_FAILED;
        }
        return *this;
    }
} // namespace htLib
#endif 



