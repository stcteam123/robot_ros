#ifndef _HT_TCP_SERVER_H_
#define _HT_TCP_SERVER_H_


#include <iostream>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <thread>
#include <algorithm>
#include <cctype>
#include <mutex>

using namespace std;

#define MAXPACKETSIZE 40960
#define MAX_CLIENT 1000
//#define CODA_MSG 4

namespace htLib
{
    // socket descriptions
    struct descript_socket
    {
	    int socket     = -1;
	    string ip      = "";
	    int id         = -1; 
	    std::string message;
	    bool enable_message_runtime = false;
    };

    class htTCPServer
    {
    	public:
        htTCPServer()
        {

        }
    	bool setup(int port, vector<int> opts = vector<int>());
    	vector<descript_socket*> getMessage();
    	void accepted();
    	void Send(string msg, int id);
    	void detach(int id);
    	void clean(int id);
        bool is_online();
    	string get_ip_addr(int id);
    	int get_last_closed_sockets();
    	void closed();

    	private:
        // sockfd: server file description
    	int sockfd, n, pid;
    	struct sockaddr_in serverAddress;
    	struct sockaddr_in clientAddress;
    	//pthread_t serverThread[ MAX_CLIENT ];

        // socket list, when a new connection is established
    	vector<descript_socket*> newsockfd;
        
    	char msg[ MAXPACKETSIZE ];
        // packet store
    	vector<descript_socket*> Message;//[CODA_MSG];

    	bool isonline;
    	int last_closed;
    	int num_client;
    	std::mutex mt;
    	void Task(void * argv);
    };
} // namespace htLib

namespace htLib
{
    bool htTCPServer::setup(int port, vector<int> opts)
    {
        int opt = 1;
        this->isonline = false;
        this->last_closed = -1;
        this->sockfd = socket(AF_INET,SOCK_STREAM,0);

        for (unsigned int i = 0; i < opts.size();i++)
        {
            if (setsockopt(this->sockfd, SOL_SOCKET, opts.size(),(char*)&opts,sizeof(opt)) < 0)
            {
                fprintf(stderr,"Error setsockopt\n");
                return false;
            }
            
        }

        serverAddress.sin_family      = AF_INET;
        // accept any address
	    serverAddress.sin_addr.s_addr = htonl(INADDR_ANY);
        // accept this port
	    serverAddress.sin_port        = htons(port);
        // Force attacch to this port
        int check = bind(this->sockfd,(struct sockaddr*)&this->serverAddress,sizeof(this->serverAddress));
        if (check < 0)
        {
            fprintf(stderr,"Errors bind\n");
            return false;
        }
        
        // listen maximum 5 clients;
        check = listen(this->sockfd,5);
        if (check<0)
        {
            fprintf(stderr,"Errors listen\n");
            return false;
        }/* */
        
        this->num_client = 0;
        this->isonline = true;
        return true;
    }

    void htTCPServer::accepted()
    {
        socklen_t sosize = sizeof(this->clientAddress);
        descript_socket *so = new descript_socket;
        so->socket = accept(this->sockfd,(struct sockaddr*)&clientAddress,&sosize);
        so->id              = num_client;
	    so->ip              = inet_ntoa(clientAddress.sin_addr);
        this->newsockfd.push_back( so );
        std::cerr << "accept client[ id:" << this->newsockfd[num_client]->id << 
	                      " ip:" << this->newsockfd[num_client]->ip << 
		              " handle:" << this->newsockfd[num_client]->socket << " ]" << std::endl;
        std::thread threadforserver(&htTCPServer::Task,this,(void *)newsockfd[num_client]);      
        threadforserver.detach();        
        this->isonline = true;
        this->num_client ++;
    }

    void htTCPServer::Task(void* argv)
    {
        int n;
        struct descript_socket *desc = (struct descript_socket*) argv;
        cerr << "open client[ id:"<< desc->id <<" ip:"<< desc->ip <<" socket:"<< desc->socket<<" send:"<< desc->enable_message_runtime <<" ]" << endl;
        while (1)
        {
            n = recv(desc->socket, msg, MAXPACKETSIZE, 0);
            if (n != -1)
            {
                if (n == 0)
                {
                    isonline = false;
                    cerr << "close client[ id:"<< desc->id <<" ip:"<< desc->ip <<" socket:"<< desc->socket<<" ]" << endl;
                    last_closed = desc->id;
			        close(desc->socket);
                    int id = desc->id;
                    auto new_end = std::remove_if(newsockfd.begin(), newsockfd.end(),
                				           		   [id](descript_socket *device)
		                              				   { return device->id == id; });
                    newsockfd.erase(new_end, newsockfd.end());
                    if(num_client>0) num_client--;
			        break;                                      
                }
                msg[n]=0;
			    desc->message = string(msg);
	            std::lock_guard<std::mutex> guard(mt);
			    Message.push_back( desc );
            }
            usleep(600); 
        }
        if(desc != NULL) free(desc);
        cerr << "exit thread: " << this_thread::get_id() << endl;
    }

    vector<descript_socket*> htTCPServer::getMessage()
    {
	    std::lock_guard<std::mutex> guard(mt);
	    return Message;
    }

    void htTCPServer::Send(string msg, int id)
    {
	    send(newsockfd[id]->socket,msg.c_str(),msg.length(),0);
    }

    int htTCPServer::get_last_closed_sockets()
    {
	    return last_closed;
    }

    void htTCPServer::clean(int id)
    {
    	Message[id]->message = "";
    	memset(msg, 0, MAXPACKETSIZE);
    }

    string htTCPServer::get_ip_addr(int id)
    {
    	return newsockfd[id]->ip;
    }

    bool htTCPServer::is_online() 
    {
    	return isonline;
    }

    void htTCPServer::detach(int id)
    {
    	close(newsockfd[id]->socket);
    	newsockfd[id]->ip = "";
    	newsockfd[id]->id = -1;
    	newsockfd[id]->message = "";
    } 

    void htTCPServer::closed() 
    {
    	close(sockfd);
    }
} // namespace htLib


#endif



