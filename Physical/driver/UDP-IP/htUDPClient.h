#ifndef _HT_UDP_CLIENT_H_
#define _HT_UDP_CLIENT_H_

#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h> 
#include <string.h> 
#include <sys/types.h> 
#include <sys/socket.h> 
#include <arpa/inet.h> 
#include <netinet/in.h> 
#include <string>

namespace htlib
{
    class htUDPClient
    {
    private:
        int sockfd;
        std::string serveraddress;
        int port;
        struct sockaddr_in serveraddr;
    public:
        htUDPClient(/* args */);
        ~htUDPClient();
        bool setup(std::string address, int port);
        bool send(std::string data);
        std::string receive(int size = 4096);
        int read(uint8_t* buff, int len);
        std::string readln();
        int read(char* buff, int len);
        int send(char* buff, int len);
        int send(uint8_t* buff, int len);
        void exit();
    };
    
} // namespace htlib


namespace htlib
{
    htUDPClient::htUDPClient(/* args */)
    {
        this->sockfd = -1;
	    this->port = 0;
	    this->serveraddress = "";
    }
    
    htUDPClient::~htUDPClient()
    {
    }
    
    bool htUDPClient::setup(std::string address, int port)
    {
        if(this->sockfd == -1)
	    {
	    	this->sockfd = socket(AF_INET , SOCK_DGRAM , 0);
	    	if (this->sockfd == -1)
	    	{
                fprintf(stderr,"Could not create socket\n");
                return false;
        	}
        }

        memset(&this->serveraddr, 0, sizeof(&this->serveraddr));
        // Filling server information 
        this->serveraddr.sin_family = AF_INET; 
        this->serveraddr.sin_port = htons(port); 
        
        if (address.compare("INADDR_ANY"))
        {
            this->serveraddr.sin_addr.s_addr = INADDR_ANY;
            this->serveraddress = "INADDR_ANY";
        } 
        else
        {
            if (inet_pton(AF_INET,address.c_str(),&this->serveraddr.sin_addr) < 0)
            {
                fprintf(stderr,"Error: Invalid IP address\n");
                return false;
            }
        }
        this->serveraddress = address;
        this->port = port;
        return true;
    }

    bool htUDPClient::send(std::string data)
    {
        if(this->sockfd != -1) 
	    {
            
            ssize_t checklne = sendto(this->sockfd, (const char *)data.c_str(), strlen(data.c_str()), MSG_CONFIRM, (const struct sockaddr *) &this->serveraddr, sizeof(this->serveraddr)); 
	    	if( checklne < 0)
	    	{
                fprintf(stderr,"Error: UDP send failed!\n");
	    		return false;
	    	}
	    }
        else
        {
            fprintf(stderr,"Error: UDP Socket failed!\n");
            return false;
        }
        
		return true;
    }

    int htUDPClient::send(char* buff, int len)
    {
    	return sendto(this->sockfd, (const char *)buff, len, MSG_CONFIRM, (const struct sockaddr *) &this->serveraddr, sizeof(this->serveraddr)); 
    }

    int htUDPClient::send(uint8_t* buff, int len)
    {
    	return sendto(this->sockfd, (const char *)buff, len, MSG_CONFIRM, (const struct sockaddr *) &this->serveraddr, sizeof(this->serveraddr)); 
    }

    std::string htUDPClient::receive(int size)
    {
      	char buffer[size+1];
    	memset(&buffer[0], 0, sizeof(buffer));
      	std::string reply = "";
        socklen_t leng = sizeof(this->serveraddr);
        size_t len = recvfrom(this->sockfd , buffer , size,MSG_WAITALL,(struct sockaddr*)&this->serveraddr, &leng);
    	if(len < 0)
      	{
            fprintf(stderr,"Error: UDP receive failed!\n");
    		return nullptr;
      	}
        else
        {
            buffer[len]='\0';
      	    reply = buffer;
      	    return reply;
        }
    }

    std::string htUDPClient::readln()
    {
      	char buffer[1] = {};
      	std::string reply;
      	while (buffer[0] != '\n') 
        {
            socklen_t leng = sizeof(this->serveraddr);
            size_t len = recvfrom(this->sockfd , buffer , sizeof(buffer),MSG_WAITALL,(struct sockaddr*)&this->serveraddr, &leng);
        	if(len < 0)
        	{
          		fprintf(stderr,"Error: UDP read failed!\n");
    		    return nullptr;
        	}
    		reply += buffer[0];
    	}
    	return reply;
    }

    int htUDPClient::read(char* buff, int len)
    {
        socklen_t leng = sizeof(this->serveraddr);
        size_t rslen = recvfrom(this->sockfd , buff , len,MSG_WAITALL,(struct sockaddr*)&this->serveraddr, &leng);
        return rslen;
        //recv(sock , buffer , sizeof(buffer) , 0) < 0);
    }

    int htUDPClient::read(uint8_t* buff, int len)
    {
        socklen_t leng = sizeof(this->serveraddr);
        size_t rslen = recvfrom(this->sockfd , buff , len,MSG_WAITALL,(struct sockaddr*)&this->serveraddr, &leng);
        return rslen;
        //recv(sock , buffer , sizeof(buffer) , 0) < 0);
    }

    void htUDPClient::exit()
    {
        close( this->sockfd );
    }
} // namespace htlib
#endif