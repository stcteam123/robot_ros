#ifndef _HT_UDP_SERVER_H_
#define _HT_UDP_SERVER_H_

#include <assert.h>
#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h>
#include <string.h> 
#include <sys/types.h> 
#include <sys/socket.h> 
#include <arpa/inet.h> 
#include <netinet/in.h> 
#include <string>

namespace htlib
{
    class htUDPServer
    {
    private:
        int sockfd;
        std::string addressFillter;
        int port;
        struct sockaddr_in serveraddr;
        struct sockaddr_in clientaddr;
    public:
        htUDPServer(/* args */);
        ~htUDPServer();
        bool setup(std::string address, int port);
        bool send(std::string data);
        std::string receive(int size = 4096);
        int read(uint8_t* buff, int len);
        std::string readln();
        int read(char* buff, int len);
        int send(char* buff, int len);
        int send(uint8_t* buff, int len);
        void exit();
    };
    
} // namespace htlib


namespace htlib
{
    htUDPServer::htUDPServer(/* args */)
    {
        this->sockfd = -1;
	    this->port = 0;
	    this->addressFillter = "";
    }
    
    htUDPServer::~htUDPServer()
    {
    }
    
    bool htUDPServer::setup(std::string address, int port)
    {
        if(this->sockfd == -1)
	    {
	    	this->sockfd = socket(AF_INET , SOCK_DGRAM , 0);
	    	if (this->sockfd == -1)
	    	{
                fprintf(stderr,"Could not create socket\n");
                return false;
        	}
        }
        
        memset(&this->serveraddr, 0, sizeof(&this->serveraddr));
        memset(&this->clientaddr, 0, sizeof(this->clientaddr)); 
        // Filling server information 
        this->serveraddr.sin_family = AF_INET; 
        this->serveraddr.sin_port = htons(port); 
        
        if (address.compare("INADDR_ANY"))
        {
            this->serveraddr.sin_addr.s_addr = INADDR_ANY;
            this->addressFillter = "INADDR_ANY";
        } 
        else
        {
            if (inet_pton(AF_INET,address.c_str(),&this->serveraddr.sin_addr) < 0)
            {
                fprintf(stderr,"Error: Invalid IP address\n");
                return false;
            }
        }

        // Bind the socket with the server address 
        int check = bind(sockfd, (const struct sockaddr *)&this->serveraddr,  sizeof(this->serveraddr));
        if ( check < 0 ) 
        { 
            fprintf(stderr,"Error: bind failed!\n");
            return false;
        }
        this->addressFillter = address;
        this->port = port;
        return true;
    }

    bool htUDPServer::send(std::string data)
    {
        if(this->sockfd != -1) 
	    {
            ssize_t checklen = sendto( this->sockfd, /* socket */
                    (const char *)data.c_str(), /* buffer to send */
                    strlen(data.c_str()), /* number of bytes to send */
                    MSG_CONFIRM, 
                    /* flags=0: bare−bones use case*/
                    (const struct sockaddr*)&this->clientaddr, /* the destination */
                    sizeof(this->clientaddr)); /* size of the destination struct */
	    	if( checklen < 0)
	    	{
                fprintf(stderr,"Error: UDP send failed!\n");
	    		return false;
	    	}
	    }
        else
        {
            fprintf(stderr,"Error: UDP Socket failed!\n");
            return false;
        }
        
		return true;
    }

    int htUDPServer::send(char* buff, int len)
    {
    	return sendto( this->sockfd, /* socket */
                    (const char *)buff, /* buffer to send */
                    len, /* number of bytes to send */
                    MSG_CONFIRM, 
                    /* flags=0: bare−bones use case*/
                    (const struct sockaddr*)&this->clientaddr, /* the destination */
                    sizeof(this->clientaddr)); /* size of the destination struct */
    }

    int htUDPServer::send(uint8_t* buff, int len)
    {
        return sendto( this->sockfd, /* socket */
                    (const char *)buff, /* buffer to send */
                    len, /* number of bytes to send */
                    MSG_CONFIRM, 
                    /* flags=0: bare−bones use case*/
                    (const struct sockaddr*)&this->clientaddr, /* the destination */
                    sizeof(this->clientaddr)); /* size of the destination struct */
    }

    std::string htUDPServer::receive(int size)
    {
      	char buffer[size+1];
    	memset(&buffer[0], 0, sizeof(buffer));
      	std::string reply = "";
        socklen_t addrlen = sizeof(struct sockaddr_in);
        size_t len = recvfrom(this->sockfd, /*Socket*/
                                    buffer, /*buffer*/
                                    size, /*Size of buffer*/
                                    MSG_WAITALL, /*Flag*/
                                    (struct sockaddr*)&this->clientaddr, /* who’s sending */
                                      &addrlen/* length of buffer to receive peer info */);
        //assert(addrlen == sizeof(struct sockaddr_in));
    	if(len < 0)
      	{
            fprintf(stderr,"Error: UDP receive failed!\n");
    		return nullptr;
      	}
        else
        {
            buffer[len]='\0';
      	    reply = buffer;
      	    return reply;
        }
    }

    std::string htUDPServer::readln()
    {
      	char buffer[1] = {};
      	std::string reply;
      	while (buffer[0] != '\n') 
        {
            socklen_t addrlen = sizeof(struct sockaddr_in);
            size_t len = recvfrom(this->sockfd, /*Socket*/
                                        buffer, /*buffer*/
                                        1, /*Size of buffer*/
                                        MSG_WAITALL, /*Flag*/
                                        (struct sockaddr*)&this->clientaddr, /* who’s sending */
                                        &addrlen/* length of buffer to receive peer info */);
            //assert(addrlen == sizeof(struct sockaddr_in));
        	if(len < 0)
        	{
          		fprintf(stderr,"Error: UDP read failed!\n");
    		    return nullptr;
        	}
    		reply += buffer[0];
    	}
    	return reply;
    }

    int htUDPServer::read(char* buff, int len)
    {
        socklen_t addrlen = sizeof(struct sockaddr_in);
        size_t len = recvfrom(this->sockfd, /*Socket*/
                                    buff, /*buffer*/
                                    len, /*Size of buffer*/
                                    MSG_WAITALL, /*Flag*/
                                    (struct sockaddr*)&this->clientaddr, /* who’s sending */
                                    &addrlen/* length of buffer to receive peer info */);
        //assert(addrlen == sizeof(struct sockaddr_in));
        return len;
        //recv(sock , buffer , sizeof(buffer) , 0) < 0);
    }

    int htUDPServer::read(uint8_t* buff, int len)
    {
        socklen_t addrlen = sizeof(struct sockaddr_in);
        size_t len = recvfrom(this->sockfd, /*Socket*/
                                    buff, /*buffer*/
                                    len, /*Size of buffer*/
                                    MSG_WAITALL, /*Flag*/
                                    (struct sockaddr*)&this->clientaddr, /* who’s sending */
                                    &addrlen/* length of buffer to receive peer info */);
        //assert(addrlen == sizeof(struct sockaddr_in));
        return len;
        //recv(sock , buffer , sizeof(buffer) , 0) < 0);
    }

    void htUDPServer::exit()
    {
        close( this->sockfd );
    }
} // namespace htlib
#endif