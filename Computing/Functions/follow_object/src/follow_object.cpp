#include <iostream>
#include <math.h>

// Boost
#include <boost/thread.hpp>

// Ros Lib
#include <ros/ros.h>
#include <tf/tf.h>
#include <geometry_msgs/Twist.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/Quaternion.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/PoseArray.h>

// Custom messages
// #include <leg_tracker/Leg.h>
// #include <leg_tracker/LegArray.h>

class follow_obj
{
    private:
        /* data */
        ros::NodeHandle         nh_;
        ros::Publisher          velocity_pub_;

        ros::Subscriber         odometry_sub_;
        ros::Subscriber         leg_detected_sub_;

        nav_msgs::Odometry      odom_;
        bool                    isNodeReady;

        void    odom_callback           ( const nav_msgs::Odometry::ConstPtr& msg_odom );
        void    leg_clusters_callback   ( const geometry_msgs::PoseArray::ConstPtr& msg_leg );
        double  Quaternion2Yaw          (const geometry_msgs::Quaternion& orien);

    public:
        void run ();

        follow_obj(/* args */) : nh_ (""), isNodeReady (false) {}
        
        ~follow_obj() {}
};

void follow_obj::odom_callback ( const nav_msgs::Odometry::ConstPtr& msg_odom )
{
    if (isNodeReady == false)
    {
        ROS_INFO ("NODE readly");
        isNodeReady = true;
    }

    odom_ = *msg_odom;
}

/*
state: WAIT PEOPLE
    - detect people
    - check in 2 second: people don't move
    - change state: FOLLOW PEOPLE

state: FOLLOW PEOPLE
    - to check people don't move to fast
    - if people ivisible -> chage state to WAIT PEOPLE
    - if receive event ROBOT_WAIT -> change state to WAIT PEOPLE

state: STOP
    - handled when receive a event: ROBOT_STOP
 */
void follow_obj::leg_clusters_callback ( const geometry_msgs::PoseArray::ConstPtr& msg_leg )
{
    if (isNodeReady == false)
    {
        return;
    }

    geometry_msgs::Twist msg_vel;
    msg_vel.linear.x = 0.0;
    msg_vel.angular.z = 0.0;

    double yaw_robot = 0.0; //Quaternion2Yaw (odom_.pose.pose.orientation);
    double robot_x = 0.0; //odom_.pose.pose.position.x;
    double robot_y = 0.0; //odom_.pose.pose.position.y;

    bool isRobotRun = false;
    std::pair <double, double> info; // first : yaw, second: dist
    info.first = M_PI;
    info.second = 10.0;

    ROS_INFO ("------------------------[%d]-------------------------", (uint16_t) msg_leg->poses.size ());

#if 1
    const double MAX_DIS = 4.5;
    const double MIN_DIS = 0.5;
    const double THRESH_LOW = 1.5;
    const double THRESH_HIGH = 2.0;
#else
    const double MAX_DIS = 3.0;
    const double MIN_DIS = 0.2;
    const double THRESH_LOW = 1.0;
    const double THRESH_HIGH = 1.3;
#endif

    for (int i = 0; i < msg_leg->poses.size (); i++)
    {
        double goal_x = msg_leg->poses[i].position.x;
        double goal_y = msg_leg->poses[i].position.y;

        float temp_goal_th = atan( (goal_y - robot_y) / (goal_x - robot_x) );
        if(goal_x < robot_x)
        { 
            temp_goal_th = temp_goal_th + M_PI;
        }
        temp_goal_th = yaw_robot - temp_goal_th;

        if (std::fabs(temp_goal_th) < (M_PI_4))
        {
            double dist_local =         \
                std::sqrt( std::pow (goal_x - robot_x, 2) + std::pow (goal_y - robot_y, 2) );

            if (dist_local > MIN_DIS && dist_local < MAX_DIS)
            {
                ROS_INFO ("[%d], temp_goal_th = %f, dist_local = %f", i, temp_goal_th * 180 / M_PI, dist_local);
                isRobotRun = true;

                if (dist_local > THRESH_LOW && dist_local < THRESH_HIGH)
                {
                    if (std::fabs(temp_goal_th) >= (M_PI / 10))
                    {
                        info.first = temp_goal_th;
                    }
                    else
                    {
                        info.first = 0.0;
                    }

                    info.second = 0.0;
                    ROS_INFO ("return: set vel linear = 0");
                    break;
                }

                if (std::fabs (temp_goal_th) < std::fabs (info.first))
                {
                    info.first = temp_goal_th;
                    info.second = dist_local;
                    ROS_INFO ("get new goal");
                }
            }
        }
    }

    if (isRobotRun == true)
    {
        if (info.second > 0.0)
        {
            double vel_linear = 0.0;
            int8_t sig = (info.second > THRESH_LOW) ? 1 : -1; 
            if (sig == 1)
            {
                vel_linear = 0.6 + (info.second - THRESH_LOW) / (THRESH_HIGH - THRESH_LOW) * 1.4; // max = 2.0
            }
            else
            {
                vel_linear = 0.5 + (info.second - MIN_DIS) / (THRESH_LOW - MIN_DIS) * 1.0; // max = 1.5
            }

            msg_vel.linear.x = sig * vel_linear;
        }
        else
        {
            msg_vel.linear.x = 0.0;
        }
        
        msg_vel.angular.z = (-1) * info.first;
        ROS_INFO ("linear = %f, angular = %f", msg_vel.linear.x, msg_vel.angular.z);
    }
    else
    {
        ROS_INFO ("No cluster Leg"); // don't have clusters
    }

    velocity_pub_.publish (msg_vel);
}

double follow_obj::Quaternion2Yaw (const geometry_msgs::Quaternion& orien)
{
    double roll, pitch, yaw;
    tf::Quaternion q ( orien.x, orien.y, orien.z, orien.w);
    tf::Matrix3x3 m(q);
    m.getRPY (roll, pitch, yaw);

    return yaw;
}

void follow_obj::run ()
{
    velocity_pub_ = nh_.advertise <geometry_msgs::Twist> ("/follower/cmd_vel", 10);
    odometry_sub_ = nh_.subscribe ("/odom", 1, &follow_obj::odom_callback, this);
    // leg_detected_sub_= nh_.subscribe ("/detected_leg_clusters", 1, &follow_obj::leg_clusters_callback, this);
    leg_detected_sub_= nh_.subscribe ("/edge_leg_detector", 1, &follow_obj::leg_clusters_callback, this);

    ROS_INFO ("follow_obj pkg is running");
}

int main(int argc, char *argv[])
{
    /* code */
    ros::init(argc, argv, "follow_obj");
    follow_obj follow;
    follow.run ();
    ros::spin ();

    return 0;
}