#include <iostream>
#include <math.h>

// Boost
#include <boost/thread.hpp>

// Ros Lib
#include <ros/ros.h>
#include <tf/tf.h>
#include <geometry_msgs/Twist.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/Quaternion.h>
#include <geometry_msgs/Point.h>

class Machine;

class State
{
    public:
        virtual void keyA (Machine* m)
        {
            std::cout << "KeyA default function" << std::endl;
        }

        virtual void keyB (Machine* m)
        {
            std::cout << "KeyB default function" << std::endl;
        }

        virtual void keyC (Machine* m)
        {
            std::cout << "KeyB default function" << std::endl;
        }

        virtual void PrintNameState (Machine* m) = 0;
};

class Machine 
{
    private:
        State* current_state;
    
    public:
        Machine ()
        {
            std::cout << "Machine default constructor" << std::endl;
        }

        ~Machine ()
        {
            if (current_state != nullptr) 
            {
                delete current_state;
                current_state = nullptr;
            }
        }

        void setCurrentState (State* s)
        {
            current_state = s;
            this->printState ();
        }

        State* getCurrentState ()
        {
            return this->current_state;
        }

        void receiveKeyA ()
        {
            std::cout << "receive Key A" << std::endl;
            current_state->keyA (this);
        }

        void receiveKeyB ()
        {
            std::cout << "receive Key B" << std::endl;
            current_state->keyB (this);
        }

        void receiveKeyC ()
        {
            std::cout << "receive Key C" << std::endl;
            current_state->keyC (this);
        }

        void printState ()
        {
            current_state->PrintNameState (this);
        }
};

class Stop_State : public State
{
    void keyA (Machine* m);

    void PrintNameState (Machine* m)
    {
        std::cout << "Stop_State" << std::endl;
    }
};

class Wait_State : public State
{
    void keyB (Machine* m);

    void keyC (Machine* m);

    void PrintNameState (Machine* m)
    {
        std::cout << "Wait_State" << std::endl;
    }
};

class Follow_State : public State
{
    void keyA (Machine* m);

    void keyC (Machine* m);

    void PrintNameState (Machine* m)
    {
        std::cout << "Follow_State" << std::endl;
    }
};

void Stop_State::keyA (Machine* m)
{
    m->setCurrentState (new Wait_State());
    delete this;
}

void Wait_State::keyB (Machine* m)
{
    m->setCurrentState (new Follow_State());
    delete this;
}

void Wait_State::keyC (Machine* m)
{
    m->setCurrentState (new Stop_State());
    delete this;
}

void Follow_State::keyA (Machine* m)
{
    m->setCurrentState (new Wait_State());
    delete this;
}

void Follow_State::keyC (Machine* m)
{
    m->setCurrentState (new Stop_State());
    delete this;
}

int main(int argc, char *argv[])
{
    Machine* client = new Machine ();
    client->setCurrentState (new Stop_State());

    client->receiveKeyA ();

    client->receiveKeyB ();

    client->receiveKeyC ();

    client->receiveKeyB ();

    client->receiveKeyC ();
    delete client;

    return 0;
}