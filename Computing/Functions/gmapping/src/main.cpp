#include <ros/ros.h>
#include <iostream>
#include "gmapping.h"

int main(int argc, char** argv)
{
  ros::init(argc, argv, "gmapping");
  std::cout << "Gampping is running" << std::endl;

  SlamGMapping gSlam;
  gSlam.run();
  ros::spin();

  return(0);
}

