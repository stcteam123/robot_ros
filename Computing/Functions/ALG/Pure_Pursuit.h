// clear memory std::vector
// v.shrink_to_fit();
// std::vector<int>().swap(foo);

/*
    [FIELD_IMAGE]
    FILE_LOCATION=HalfField.png
    PIXELS_PER_UNIT=1.992

    [POINT_INJECTION]
    POINT_DIST=10
    WEIGHT_DATA=0.2
    WEIGHT_SMOOTH=0.8
    TOLERANCE=0.001

    [VELOCITY]
    MAX_VEL=250
    TURNING_CONST=6
    STARTING_VEL=36
    MAX_ACCEL=70

    [PATH]
    FILE_LOCATION=path.csv
    LOOKAHEAD=20

    [ROBOT]
    TRACKWIDTH = 36
    LENGTH = 28
    MAX_VEL_CHANGE=800

 */

#include <iostream>
#include <cmath>
#include <vector>
#include <algorithm>

namespace  purePursuit
{
    #define WEIGH_DATA_SMOOTH       0.2
    #define WEIGH_SMOOTH            0.8
    #define TOLERANCE_SMOOTH        0.001

    #define POW2(x)                 std::pow (x, 2)

    typedef std::pair <double, double> pPure;

    // FUNCTIONS

    /*          
     *          CREATE PATH BY PURE PURSUIT ALG
     * 
     * */

    // add points on between two points a space -> path inject
    void Inject_Point ( const std::vector<pPure>& path, std::vector<pPure> pathInject, double space = 0.3)
    {
        pPure p;
        for (int i = 0; i < path.size () - 1; i++)
        {
            double dist = std::sqrt (std::pow(path[i + 1].first - path[i].first, 2) + std::pow(path[i + 1].second - path[i].second, 2));
            double j = 0;

            double a = path[i].first;
            double b = path[i].second;

            double c = path[i + 1].first;
            double d = path[i + 1].second;

            while (j < dist)
            {
                p.first = a + j / dist * (c - a);
                p.second = b + j / dist * (d - b);
                pathInject.push_back (p);

                j += space;
            }
        }

        pathInject.push_back (path.back());
    }


    // W[0] and W[1]:  PathSmooth
    //  in:  Path Inject
    //  out: path smooth
    void SmootherPath(  const std::vector<pPure>&   path, 
                        std::vector<pPure>&         PathSmooth, 
                        double                      weight_data, 
                        double                      weight_smooth, 
                        double                      tolerance)
    {
        // Copying vector by assign function 
        PathSmooth.assign(path.begin(), path.end());

        double change = tolerance;
        while(change >= tolerance)
        {
            change = 0.0;
            for(int i = 1; i < path.size() - 1; i++)
            {
                // x 
                double aux = PathSmooth[i].first;
                PathSmooth[i].first += weight_data * (path[i].first - PathSmooth[i].first) + 
                                        weight_smooth * (PathSmooth[i-1].first + PathSmooth[i+1].first - (2.0 * PathSmooth[i].first));
                change += std::abs(aux - PathSmooth[i].first);

                // y
                double auy = PathSmooth[i].second;
                PathSmooth[i].second += weight_data * (path[i].second - PathSmooth[i].second) + 
                                        weight_smooth * (PathSmooth[i-1].second + PathSmooth[i+1].second - (2.0 * PathSmooth[i].second));
                change += std::abs(auy - PathSmooth[i].second);	
            }		
        }
    }

    // CALCULATE PATH DISTANCE
    // W[2]         dist
    // khoang cach tu diem bat dau den point hien tai
    // in:      path smooth
    // out:     dist  
    bool Calculate_Path_Distance (  const std::vector<pPure>&    pathSmooth, 
                                    std::vector<double>&         dist)
    {
        uint16_t lenPath = pathSmooth.size ();
        if (lenPath == 0)
        {
            return false;
        }

        dist.resize (lenPath);
        dist[0] = 0;
        double distanceTmp = 0;
        for (int i = 1; i < lenPath; i++)
        {
            distanceTmp = dist[i - 1] +         \
                                std::sqrt (std::pow(pathSmooth[i - 1].first - pathSmooth[i].first, 2) +     \
                                    std::pow(pathSmooth[i - 1].second - pathSmooth[i].second, 2));
            dist[i] = distanceTmp;
        }

        return true;
    }

    // CALCULATE CURVATURE
    // W[3]     curvature

    // in: 
    //      path:       path smooth
    //      dist:       distance 
    // out:
    //      curvature:  radius path at current point
    bool Calculate_Curvature (  std::vector <pPure>&         path,
                                const std::vector<double>&   dist, 
                                uint16_t                     lenPath, 
                                std::vector<double>&         curvature)
    {
        uint16_t lenPath = path.size ();
        if (lenPath == 0)
        {
            return false;
        }

        curvature.resize (lenPath);
        curvature[0] = 0.0001;
        curvature[lenPath - 1] = 0.0001;

        for (int i = 1; i < lenPath - 1; i++)
        {
            path[i].first += 0.0001;
            path[i].second += 0.0001;

            double k1 = 0.5 * (POW2 (path[i].first) + POW2 (path[i].second) - POW2 (path[i-1].first) - POW2 (path[i-1].second))   
                            / (path[i].first - path[i-1].first);
            
            double k2 = (path[i].second - path[i-1].second) / (path[i].first - path[i-1].first);

            double b = 0.5 * (POW2(path[i-1].first) - 2 * path[i-1].first * k1 + POW2(path[i-1].second) 
                       - POW2(path[i+1].first) + 2 * path[i+1].first * k1 - POW2(path[i+1].second)) 
                       / (path[i+1].first * k2 - path[i+1].second + path[i-1].second - path[i-1].first * k2); 
            
            double a = k1 - k2 * b;

            double r = std::sqrt (POW2(path[i].first - a) + POW2(path[i].second - b));
            curvature[i] = (double)(1 / r);
        }

        return true;
    }

    // CALCULATE DESIRED VELOCITY
    /*
    * where turning_const is a constant around 1-5, based on how
    * slow you want the robot to go around turns
    * */

    // W[4]        desired_velocity
    bool Calculate_Desired_Velocity ( const std::vector<double>&    curvature, 
                                      std::vector<double>&          desired_velocity,
                                      double                        max_vel,
                                      double                        turning_const)
    {
        uint16_t lenPath = curvature.size ();
        if (lenPath == 0)
        {
            return false;
        }

        desired_velocity.resize (lenPath);
        for (int i = 0; i < lenPath; i++)
        {
            desired_velocity[i] = std::min(max_vel, turning_const / curvature[i]);
        }

        return true;
    }

    // W[5]         acce_limits_vel
    bool Add_Acce_Limits_Velocity ( const std::vector<double>&    dist,
                                    const std::vector<double>&    desired_velocity,
                                    std::vector<double>&          acce_limits_vel,
                                    double                        max_acce,
                                    double                        lastVelPoint)
    {
        uint16_t lenPath = desired_velocity.size ();
        if (lenPath == 0)
        {
            return false;
        }

        acce_limits_vel.resize (lenPath);
        acce_limits_vel[lenPath - 1] = lastVelPoint;

        for (int i = lenPath - 2; i >= 0; i--)
        {
            double vel_tmp = std::sqrt(POW2(acce_limits_vel[i+1]) + 2 * max_acce * (dist[i+1] - dist[i]));
            acce_limits_vel[i] = std::min (desired_velocity[i], vel_tmp);
        }

        return true;
    }

    /*          
     *          FOLLOW POINTS ON PATH BY PURE PURSUIT ALG
     * */

    int16_t Get_Closest_Point (const std::vector<pPure>& pathSmooth, pPure robot_pose, uint16_t iStart, uint16_t iEnd)
    {
        int16_t index = -1;
        uint16_t lenPath = pathSmooth.size ();
        if (lenPath == 0)
        {
            return index;
        }

        if (iStart >= lenPath - 1 || iEnd >= lenPath - 1)
        {
            return index;
        }

        float dist = std::sqrt (POW2(robot_pose.first - pathSmooth[iStart].first) 
                                + POW2(robot_pose.second - pathSmooth[iStart].second));
        index = iStart;

        for (int i = iStart + 1; i < iEnd; i++)
        {
            float distTmp = std::sqrt (POW2(robot_pose.first - pathSmooth[i].first) 
                                + POW2(robot_pose.second - pathSmooth[i].second));

            if (distTmp < dist)
            {
                dist = distTmp;
                index = i;
            }
        }
        return index;
    }

    bool lookahead ()
    {
        // do not using
        return true;
    }

    void test ()
    {
        // do nothing
    }
}