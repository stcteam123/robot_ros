// clear memory std::vector
// v.shrink_to_fit();
// std::vector<int>().swap(foo);

/*
    [FIELD_IMAGE]
    FILE_LOCATION=HalfField.png
    PIXELS_PER_UNIT=1.992

    [POINT_INJECTION]
    POINT_DIST=10
    WEIGHT_DATA=0.2
    WEIGHT_SMOOTH=0.8
    TOLERANCE=0.001

    [VELOCITY]
    MAX_VEL=250
    TURNING_CONST=6
    STARTING_VEL=36
    MAX_ACCEL=70

    [PATH]
    FILE_LOCATION=path.csv
    LOOKAHEAD=20

    [ROBOT]
    TRACKWIDTH = 36
    LENGTH = 28
    MAX_VEL_CHANGE=800

 */

#include <iostream>
#include <cmath>
#include <vector>
#include <algorithm>

namespace  purePursuit
{
    #define WEIGH_DATA_SMOOTH       0.4
    #define WEIGH_SMOOTH            0.6
    #define TOLERANCE_SMOOTH        0.01

    #define POW2(x)                 std::pow (x, 2)

    typedef std::pair <double, double> pPure;

    float diff_angle_to_line (pPure point1, pPure point2, pPure point3);

    // FUNCTIONS

    /*          
     *          CREATE PATH BY PURE PURSUIT ALG
     * 
     * */

    // add points on between two points a space -> path inject
    void Inject_Point ( const std::vector<pPure>& path, std::vector<pPure>& pathInject, double space = 0.4)
    {
        pPure p;
        for (int i = 0; i < path.size () - 1; i++)
        {
            double dist = std::sqrt (std::pow(path[i + 1].first - path[i].first, 2) 
                                + std::pow(path[i + 1].second - path[i].second, 2));
            double j = 0;

            double a = path[i].first;
            double b = path[i].second;

            double c = path[i + 1].first;
            double d = path[i + 1].second;

            while (j < dist)
            {
                p.first = a + j / dist * (c - a);
                p.second = b + j / dist * (d - b);
                pathInject.push_back (p);

                j += space;
            }
        }

        pathInject.push_back (path.back());
    }


    // W[0] and W[1]:  PathSmooth
    //  in:  Path Inject
    //  out: path smooth
    void SmootherPath(  const std::vector<pPure>&   path, 
                        std::vector<pPure>&         PathSmooth, 
                        double                      weight_data, 
                        double                      weight_smooth, 
                        double                      tolerance)
    {
        // Copying vector by assign function 
        PathSmooth.assign(path.begin(), path.end());

        double change = tolerance;
        while(change >= tolerance)
        {
            change = 0.0;
            for(int i = 1; i < path.size() - 1; i++)
            {
                // x 
                double aux = PathSmooth[i].first;
                PathSmooth[i].first += weight_data * (path[i].first - PathSmooth[i].first) + 
                                        weight_smooth * (PathSmooth[i-1].first + PathSmooth[i+1].first - (2.0 * PathSmooth[i].first));
                change += std::abs(aux - PathSmooth[i].first);

                // y
                double auy = PathSmooth[i].second;
                PathSmooth[i].second += weight_data * (path[i].second - PathSmooth[i].second) + 
                                        weight_smooth * (PathSmooth[i-1].second + PathSmooth[i+1].second - (2.0 * PathSmooth[i].second));
                change += std::abs(auy - PathSmooth[i].second);	
            }		
        }
    }

    // CALCULATE PATH DISTANCE
    // W[2]         dist
    // khoang cach tu diem bat dau den point hien tai
    // in:      path smooth
    // out:     dist  
    bool Calculate_Path_Distance (  const std::vector<pPure>&    pathSmooth, 
                                    std::vector<double>&         dist)
    {
        uint16_t lenPath = pathSmooth.size ();
        if (lenPath == 0)
        {
            return false;
        }

        dist.resize (lenPath);
        dist[0] = 0;
        double distanceTmp = 0;
        for (int i = 1; i < lenPath; i++)
        {
            distanceTmp = dist[i - 1] +         \
                                std::sqrt (std::pow(pathSmooth[i - 1].first - pathSmooth[i].first, 2) +     \
                                    std::pow(pathSmooth[i - 1].second - pathSmooth[i].second, 2));
            dist[i] = distanceTmp;
        }

        return true;
    }

    // CALCULATE RADIUS
    // W[3]     radius

    // in: 
    //      path:       path smooth
    //      dist:       distance 
    // out:
    //      radius:  radius path at current point
    bool Calculate_Radius ( std::vector <pPure>&         path,
                            const std::vector<double>&   dist,
                            std::vector <double>&        radius,
                            std::vector <double>&        diff_angle)
    {
        uint16_t lenPath = path.size ();
        if (lenPath == 0)
        {
            return false;
        }

        radius.resize (lenPath);
        diff_angle.resize (lenPath);

        radius[0] = radius[lenPath - 1] = 1000;
        diff_angle[0] = diff_angle[lenPath - 1] = 0.0001;

        for (int i = 1; i < lenPath - 1; i++)
        {
            path[i].first += 0.0001;
            path[i].second += 0.0001;

            double k1 = 0.5 * (POW2 (path[i].first) + POW2 (path[i].second) - POW2 (path[i-1].first) - POW2 (path[i-1].second))   
                            / (path[i].first - path[i-1].first);
            
            double k2 = (path[i].second - path[i-1].second) / (path[i].first - path[i-1].first);

            double b = 0.5 * (POW2(path[i-1].first) - 2 * path[i-1].first * k1 + POW2(path[i-1].second) 
                       - POW2(path[i+1].first) + 2 * path[i+1].first * k1 - POW2(path[i+1].second)) 
                       / (path[i+1].first * k2 - path[i+1].second + path[i-1].second - path[i-1].first * k2); 
            
            double a = k1 - k2 * b;

            double r = std::sqrt (POW2(path[i].first - a) + POW2(path[i].second - b));
            radius[i] = r;

            float yaw = diff_angle_to_line (path[i-1], path[i], path[i+1]);  // rad
            diff_angle[i] = (double)yaw;
        }

        return true;
    }

    // CALCULATE DESIRED VELOCITY
    /*
    * where turning_const is a constant around 1-5, based on how
    * slow you want the robot to go around turns
    * */

    // W[4]        desired_velocity
    bool Calculate_Desired_Velocity ( const std::vector<double>&    radius,                 // m
                                      std::vector<double>&          desired_velocity,       // m/s
                                      double                        max_vel,                // m/s
                                      double                        turning_const = 0.6)
    {
        uint16_t lenPath = radius.size ();
        if (lenPath == 0)
        {
            return false;
        }

        desired_velocity.resize (lenPath);
        for (int i = 0; i < lenPath; i++)
        {
            double vel_by_radius = turning_const * std::sqrt (radius[i]);
            desired_velocity[i] = std::min(max_vel, vel_by_radius);
        }

        return true;
    }

    // W[5]         acce_limits_vel
    bool Add_Acce_Limits_Velocity ( const std::vector<double>&    dist,
                                    const std::vector<double>&    desired_velocity,
                                    std::vector<double>&          acce_limits_vel,
                                    double                        max_acce_down,
                                    double                        lastVelPoint,
                                    double                        max_acce_up,
                                    double                        startVelPoint)
    {
        uint16_t lenPath = desired_velocity.size ();
        if (lenPath == 0)
        {
            return false;
        }

        acce_limits_vel.resize (lenPath);
        acce_limits_vel[lenPath - 1] = lastVelPoint;

        // giam toc: max_acce_down = -0.3
        for (int i = lenPath - 2; i >= 0; i--)
        {
            double vel_tmp = std::sqrt(POW2(acce_limits_vel[i+1]) - 2 * max_acce_down * (dist[i+1] - dist[i]));
            acce_limits_vel[i] = std::min (desired_velocity[i], vel_tmp);
        }

        // tang toc: max_acce_up = 0.8
        acce_limits_vel[0] = startVelPoint;
        for (int i = 1; i <= lenPath - 1; i++)
        {
            double vel_tmp = std::sqrt(POW2(acce_limits_vel[i-1]) + 2 * max_acce_up * (dist[i] - dist[i-1]));
            acce_limits_vel[i] = std::min (acce_limits_vel[i], vel_tmp);
        }

        if (lenPath > 1)
        {
            acce_limits_vel[0] = acce_limits_vel[1];
        }

        return true;
    }

    /*          
     *          FOLLOW POINTS ON PATH BY PURE PURSUIT ALG
     * */

    int16_t Get_Closest_Point (const std::vector<pPure>& pathSmooth, pPure robot_pose, uint16_t iStart, uint16_t iEnd)
    {
        int16_t index = -1;
        uint16_t lenPath = pathSmooth.size ();
        if (lenPath == 0)
        {
            printf ("ERROR LEN = 0\n");
            return index;
        }

        if (iStart >= lenPath || iEnd >= lenPath)
        {
            printf ("ERROR LEN\n");
            return index;
        }

        float dist = std::sqrt (POW2(robot_pose.first - pathSmooth[iStart].first) 
                                + POW2(robot_pose.second - pathSmooth[iStart].second));
        index = iStart;

        for (int i = iStart + 1; i < iEnd; i++)
        {
            float distTmp = std::sqrt (POW2(robot_pose.first - pathSmooth[i].first) 
                                + POW2(robot_pose.second - pathSmooth[i].second));

            if (distTmp < dist)
            {
                dist = distTmp;
                index = i;
            }
        }
        return index;
    }

    // line1: p1 -> p2
    // line2: p1 -> p3
    // https://www.youtube.com/watch?v=JSEPDJfl8m8
    float diff_angle_to_line (pPure point1, pPure point2, pPure point3)
    {
        float angle = 0;
        float m1 = (point2.second - point1.second) / (point2.first - point1.first);
        float m2 = (point3.second - point1.second) / (point3.first - point1.first);

        float tanPhi = (m2 - m1) / (1 + m2 * m1);
        angle = std::atan (tanPhi);

        return angle;
    }

    bool smoothing_velocity_for_robot ( const std::vector <double>& vel,
                                        const std::vector <double>& radius,
                                        const std::vector <double>& diff_angle, 
                                        std::vector <double>& smth_vel,
                                        double threshold_angle = 0.015,
                                        double threshold_radius = 50)
    {
        uint16_t lenPath = vel.size ();

        if (lenPath <= 0)
        {
            return false;
        }

        // Copying vector by assign function 
        smth_vel.assign (vel.begin(), vel.end());
        
        if (lenPath < 8)
        {
            return true;
        }

        std::vector <uint16_t> index;
        index.clear ();

        printf ("lanPath = [%d]\n", lenPath);
        // < 0 : false; > 0 : true
        for (uint16_t i = 0; i <= lenPath - 8; i++)
        {
            uint8_t arr[8];
            for (int ii = 0; ii < 8; ii++)
            {
                arr[ii] = (diff_angle[i + ii] >= 0) ? 1 : 0;
            }

            uint8_t total_pre = (arr[0] + arr[1] + arr[2] + arr[3]);
            uint8_t total_aft = (arr[4] + arr[5] + arr[6] + arr[7]);

            if (total_pre == 0 && total_aft == 4 ||
                total_pre == 4 && total_aft == 0)
            {
                uint8_t cnt = 0;
                for (int jj = 1; jj < 7; jj++)
                {
                    if (std::fabs(diff_angle[jj + i]) > threshold_angle)
                    {
                        cnt++;
                    }
                }

                if (cnt > 3)
                {
                    index.push_back (i);
                }
            }
        }
        
        uint16_t lenIndex = index.size ();
        printf ("----> lenIndex = [%d] <----\n", lenIndex);
        for (int i = 0; i < lenIndex; i++)
        {
            printf ("index = [%d]\n", index[i]);

            uint16_t idxGet = index[i];
            for (int ii = idxGet; ii < idxGet + 8; ii++)
            {
                if (ii - idxGet >= 4)
                {
                    if (radius[ii] < threshold_radius)
                    {
                        smth_vel[ii] = std::min (smth_vel[ii], vel[ii] * 0.9);
                    }
                }
                else
                {
                    uint8_t percent = 100;
                    if (radius[ii] < 5)
                    {
                        percent = 60;
                    }
                    else if (radius[ii] < 15)
                    {
                        percent = 70;
                    }
                    else if (radius[ii] < 30)
                    {
                        percent = 80;
                    }
                    else if (radius[ii] < 50)
                    {
                        percent = 90;
                    }

                    smth_vel[ii] = std::min (smth_vel[ii], vel[ii] * percent / 100);
                }
            }
        }

        return true;
    }

#if 0
    // tim diem uon
    void find_twist_point ()
    {

    }

    bool lookahead ()
    {
        // do not using
        return true;
    }

    void test ()
    {
        // do nothing
    }
#endif 

}