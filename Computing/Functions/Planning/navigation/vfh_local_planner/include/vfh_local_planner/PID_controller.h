/* *
 *  $author: design by hieuta
 *  $date: 02-28-2020
 *  $ref: https://gist.github.com/bradley219/5373998
 *        https://en.cppreference.com/w/cpp/language/pimpl
 *        https://github.com/spgitmonish/PIDController
 * */


#ifndef _HTLIB_PID_H_
#define _HTLIB_PID_H_

// Your code
namespace htLib
{
    class PIDImpl;

    class PID
    {
        private:
            /* data */
            PIDImpl *pimpl;
            bool Initialize_;

        public:
            
            // Kp -  proportional gain
            // Ki -  Integral gain
            // Kd -  derivative gain
            // dt -  loop interval time
            // max - maximum value of manipulated variable
            // min - minimum value of manipulated variable
            PID ();
            ~PID();

            void PID_Init (double max, double min, double kp, double kd, double ki);
            // Returns the manipulated variable given a setpoint and current process value
            double PID_Calculate( double setpoint, double pv, double dt );
            void PID_Reset ();
    };
    
} // namespace pid

#endif // _HTLIB_PID_H        