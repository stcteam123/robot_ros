#ifndef RDP_LINE_SIMP_ALG_H_
#define RDP_LINE_SIMP_ALG_H_

#include <iostream>
#include <vector>
#include <math.h>

using namespace std;

namespace ALG
{
	typedef std::pair<double, double> Point;

	double PerpendicularDistance(const Point &pt, const Point &lineStart, const Point &lineEnd);

	void RamerDouglasPeucker(const vector<Point> &pointList, double epsilon, vector<Point> &out);

	void fun_Test();
}

#endif  //RDP_LINE_SIMP_ALG_H_