#include "PID_controller.h"

#include <iostream>
#include <cmath>

namespace htLib
{
    class PIDImpl
    {
        public:
            PIDImpl (double _max, double _min, double _kp, double _kd, double _ki);
            ~PIDImpl ();
            double calculate (double setpoint, double pv, double dt);
            double Output_PID ();
            void pid_reset ();

            double get_pOut ();
            double get_iOut ();
            double get_dOut ();

        private:
            /*
             * Error p, i, d
             */
            double pOut;
            double iOut;
            double dOut;
            
            /*
             * Coefficients
             */
            double kp, ki, kd;

            /*
             *  variable
             * */
            double max, min;
            double pre_error;
            double integral;
    };

    /**
     *  Implementation PID class
     * */
    PID::PID () : Initialize_ (false) {}

    void PID::PID_Init (double max, double min, double kp, double kd, double ki)
    {
        pimpl = new PIDImpl(max, min, kp, kd, ki);
        Initialize_ = true;
    }

    PID::~PID ()
    {
        Initialize_ = false;
        delete pimpl;
    }

    double PID::PID_Calculate ( double setpoint, double pv, double dt )
    {
        if (Initialize_ == false)
        {
            return 0;
        }

        return pimpl->calculate (setpoint, pv, dt);
    }

    void PID::PID_Reset ()
    {
        if (Initialize_ == true)
        {
            pimpl->pid_reset ();
        }
    }


    /**
     *  Implementation PIDImpl class
     * */
    PIDImpl::PIDImpl (double _max, double _min, double _kp, double _kd, double _ki) :
                    max(_max),
                    min(_min),
                    kp(_kp),
                    kd(_kd),
                    ki(_ki),
                    pre_error (0),
                    integral (0)
    {}

    PIDImpl::~PIDImpl () {}

    double PIDImpl::calculate (double setpoint, double pv, double dt)
    {
        // calculate error
        double error = setpoint - pv;

        // Proportional term
        pOut = kp * error;

        // Integral term
        integral += error * dt;
        iOut = ki * integral;

        // Derivative
        // if(dt == 0.0)
        //     throw std::exception("Impossible to create a PID regulator with a null loop interval time.");

        double derivative = 0;
        if (dt)
        {
            derivative = (error - pre_error) / dt;
        }
        dOut = kd * derivative;

        // Calculate total output
        double output = Output_PID();

        // Restrict to max/min
        if (output > this->max)
        {
            output = this->max;
        }
        else if (output < this->min)
        {
            output = this->min;
        }

        pre_error = error;

        return output;
    }

    double PIDImpl::Output_PID ()
    {
        return this->pOut + this->iOut + this->dOut;
    }

    void PIDImpl::pid_reset ()
    {
        pre_error = 0;
        integral = 0;
    }

    double PIDImpl::get_pOut ()
    {
        return this->pOut;
    }

    double PIDImpl::get_iOut ()
    {
        return this->iOut;
    }

    double PIDImpl::get_dOut ()
    {
        return this->dOut;
    }
}