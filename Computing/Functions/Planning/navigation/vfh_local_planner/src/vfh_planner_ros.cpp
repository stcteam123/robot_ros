#include <pluginlib/class_list_macros.h>
#include <Eigen/Core>
#include <cmath>

#include <base_local_planner/goal_functions.h>
#include <pluginlib/class_list_macros.h>
#include <std_msgs/Bool.h>

#include "vfh_local_planner/vfh_planner_ros.h"
#include <tf2/utils.h>

//register this planner as a BaseLocalPlanner plugin
PLUGINLIB_EXPORT_CLASS(vfh_local_planner::VFHPlannerROS, nav_core::BaseLocalPlanner)

namespace vfh_local_planner 
{
	//start initialize
	void VFHPlannerROS::initialize( std::string 				name, 
									tf2_ros::Buffer* 			tf,
      								costmap_2d::Costmap2DROS* 	costmap_ros)
	{
		if(!initialized_)
		{
			ROS_INFO("Initializing VFH");

			path_Pp_.clear ();
			vel_acce_Pp_.clear ();
			isVelPureReady_ = false;
			
			costmap_ros_ = costmap_ros;
			tf_ = tf;

			global_frame_ = costmap_ros_->getGlobalFrameID();
			robot_base_frame_ = costmap_ros_->getBaseFrameID();
			// inflation_radius_ = costmap_ros_->getInflationRadius();

			ros::NodeHandle nh_private_("~/" + name);

			g_plan_pub_ =  nh_private_.advertise<nav_msgs::Path>("global_plan", 1);
			l_plan_pub_ =  nh_private_.advertise<nav_msgs::Path>("local_plan", 1);

			// Pure Pursuit ALG
			pathRDP_pub_ 		= nh_private_.advertise<nav_msgs::Path>("/pathRDP_test", 10);
			pathSmooth_pub_ 	= nh_private_.advertise<nav_msgs::Path>("/pathSmooth_test", 10);
			pathPoseArray_pub_ 	= nh_private_.advertise<geometry_msgs::PoseArray>("/poseArr_test", 10);
			directGoal_pub_		= nh_private_.advertise<nav_msgs::Odometry>("/goal_direction", 10);

			//setto i parametri fissi
			m_cell_size = 50;							// mm, cell dimension
			m_window_diameter = 150;					// number of cells
			m_sector_angle = 1;             			// deg, sector angle

			//leggo i parametri intercambiabili
			if (!nh_private_.getParam ("m_safety_dist_0ms", m_safety_dist_0ms))
				m_safety_dist_0ms = 50; 				// mm, double, safe distance at 0 m/s

			if (!nh_private_.getParam ("m_safety_dist_1ms", m_safety_dist_1ms))
				m_safety_dist_1ms = 200; 				// mm, double, safe distance at 1 m/s

			if (!nh_private_.getParam ("m_max_speed", m_max_speed))
				m_max_speed = 2000;						// mm/sec, int, max speed

			if (!nh_private_.getParam ("m_max_speed_narrow_opening", m_max_speed_narrow_opening))
				m_max_speed_narrow_opening = 1600;		// mm/sec, int, max speed in the narrow opening

			if (!nh_private_.getParam ("m_max_speed_wide_opening", m_max_speed_wide_opening))
				m_max_speed_wide_opening = 2000; 		// mm/sec, int, max speed in the wide opening

			if (!nh_private_.getParam ("m_max_acceleration", m_max_acceleration))
				m_max_acceleration = 800;    			// mm/sec^2, int, max acceleration

			if (!nh_private_.getParam ("m_min_turnrate", m_min_turnrate))
				m_min_turnrate = 20;	 				// deg/sec, int, min turn rate <--- not used

			// m_max_turnrate_0ms >= m_max_turnrate_1ms
			if (!nh_private_.getParam ("m_max_turnrate_0ms", m_max_turnrate_0ms))
				m_max_turnrate_0ms = 40;				// deg/sec, int, max turn rate at 0 m/s

			if (!nh_private_.getParam ("m_max_turnrate_1ms", m_max_turnrate_1ms))
				m_max_turnrate_1ms = 35;				// deg/sec, int, max turn rate at 1 m/s

			m_min_turn_radius_safety_factor = 0.05; 	// double ????

			// threshold cutoff low 
			if (!nh_private_.getParam ("m_free_space_cutoff_0ms", m_free_space_cutoff_0ms))
				m_free_space_cutoff_0ms = 8000.0; 		//double, low threshold free space at 0 m/s

			if (!nh_private_.getParam ("m_obs_cutoff_0ms", m_obs_cutoff_0ms))
				m_obs_cutoff_0ms = 14000.0;				//double, high threshold obstacle at 0 m/s

			// threshold cutoff high
			if (!nh_private_.getParam ("m_free_space_cutoff_1ms", m_free_space_cutoff_1ms))
				m_free_space_cutoff_1ms = 6500.0; 		//double, low threshold free space at 1 m/s

			if (!nh_private_.getParam ("m_obs_cutoff_1ms", m_obs_cutoff_1ms))
				m_obs_cutoff_1ms = 10000.0;				//double, high threshold obstacle at 1 m/s

			if (!nh_private_.getParam ("m_weight_desired_dir", m_weight_desired_dir))
				m_weight_desired_dir = 10.0;			//double, weight desired direction

			if (!nh_private_.getParam ("m_weight_current_dir", m_weight_current_dir))
				m_weight_current_dir = 1.0;				//double, weight current direction

			if (!nh_private_.getParam ("m_robot_radius", m_robot_radius))
				m_robot_radius = 680.0;					// robot radius in mm

			if (!nh_private_.getParam ("odom_topic_", odom_topic_))
				odom_topic_ = "odom";

			if (!nh_private_.getParam ("scan_topic_", scan_topic_))
				scan_topic_ = "/scan";

			if (!nh_private_.getParam ("path_global_topic_", path_global_topic_))
				path_global_topic_ = "/move_base/NavfnROS/plan";

			ROS_INFO("topic: %s , %s", scan_topic_.c_str(), odom_topic_.c_str());
			nh_private_.param("yaw_goal_tolerance", yaw_goal_tolerance_, 2.35);
			nh_private_.param("xy_goal_tolerance", xy_goal_tolerance_, 0.2);

			//inizializzo l'oggetto vfh algorithm
			m_vfh = new VFH_Algorithm(m_cell_size, m_window_diameter, m_sector_angle,
					m_safety_dist_0ms, m_safety_dist_1ms, m_max_speed,
					m_max_speed_narrow_opening, m_max_speed_wide_opening,
					m_max_acceleration, m_min_turnrate, m_max_turnrate_0ms,
					m_max_turnrate_1ms, m_min_turn_radius_safety_factor,
					m_free_space_cutoff_0ms, m_obs_cutoff_0ms, m_free_space_cutoff_1ms,
					m_obs_cutoff_1ms, m_weight_desired_dir, m_weight_current_dir);

			m_vfh->SetRobotRadius(m_robot_radius);
			m_vfh->Init();

			// pid.PID_Init (m_max_turnrate_0ms, -m_max_turnrate_0ms, 1.5, 0.8, 0.005); // p, d, i
			pid.PID_Init (m_max_turnrate_0ms, -m_max_turnrate_0ms, 1.2, 0.8, 0.01); // p, d, i

			reached_goal_ = false;
			isShoted = false;

			ros::NodeHandle nh;
			// subscribe to topics
			scan_subscriber_ = nh.subscribe(
					scan_topic_, 1, &VFHPlannerROS::scanCallback, this);
			odom_subscriber_ = nh.subscribe(
					odom_topic_, 1, &VFHPlannerROS::odomCallback, this);
			path_global_subscriber_ = nh.subscribe(
					path_global_topic_, 1, &VFHPlannerROS::pathGlobalCallback, this);
			shoted_subscriber_ = nh.subscribe(
					"/sensor/Shoted", 1, &VFHPlannerROS::shotedCallback, this);
			setMaxSpeed_subscriber_ = nh.subscribe(
					"/sensor/setMaxVel", 1, &VFHPlannerROS::setMaxSpeedCallback, this);
      

			initialized_ = true;
		}
		else
		{
			ROS_WARN("This planner has already been initialized, doing nothing.");
		}
	}
	//end initialize

	void VFHPlannerROS::pathGlobalCallback (const nav_msgs::Path::ConstPtr& path_msg)
	{
		isVelPureReady_ = false;
		pid.PID_Reset ();
		uint16_t lenPath = path_msg->poses.size ();
		if (lenPath == 0)
		{
			return;
		}

		// RPD ALG
		// warning: Point of vfh and Point of ALG
		std::vector<ALG::Point>   pointList;
		std::vector<ALG::Point>   pointListOut;	

		pointList.resize (lenPath);
		for (int i = 0; i < lenPath; i++)
		{
			pointList[i].first = path_msg->poses[i].pose.position.x;
			pointList[i].second = path_msg->poses[i].pose.position.y;
		}

		ALG::RamerDouglasPeucker(pointList, 0.08, pointListOut);
		uint16_t lenPathRPD = (uint16_t)pointListOut.size ();

		std::vector<pPure> path;
		path.resize (lenPathRPD);

		for (int i = 0; i < lenPathRPD; i++)
		{
			path[i].first = pointListOut[i].first;
			path[i].second = pointListOut[i].second;
		}

		/*
			free pointList and pointListOut
		 */
		pointList.clear ();
		pointList.shrink_to_fit ();

		pointListOut.clear ();
		pointListOut.shrink_to_fit ();

#if 1
		/* 
				PUBLISH path RDP
		*/
		nav_msgs::Path  path_RDP;
		for (size_t i = 0; i < lenPathRPD; i++)
		{
			geometry_msgs::PoseStamped lPose;

			lPose.pose.position.x = path[i].first;
			lPose.pose.position.y = path[i].second;

			path_RDP.poses.push_back (lPose);
		}

		path_RDP.header.frame_id = "odom";
		pathRDP_pub_.publish (path_RDP);

#endif

		// Inject Point
		std::vector<pPure> pathInject;
		Inject_Point (path, pathInject, 0.5);

		// free path (path RDP)
		path.clear ();
		path.shrink_to_fit ();

		// smoothing path Inject        w[0], w[1]
		std::vector <pPure> pathSmooth;
		SmootherPath (pathInject, pathSmooth, WEIGH_DATA_SMOOTH, WEIGH_SMOOTH, TOLERANCE_SMOOTH);

		// free pathInject 	
		pathInject.clear ();
		pathInject.shrink_to_fit ();

	#if 1
		/* 
			PUBLISH path Smooth
		*/
		nav_msgs::Path  path_Smooth;
		for (size_t i = 0; i < (uint16_t)(pathSmooth.size ()); i++)
		{
			geometry_msgs::PoseStamped pose;

			pose.pose.position.x = pathSmooth[i].first;
			pose.pose.position.y = pathSmooth[i].second;

			path_Smooth.poses.push_back (pose);
		}

		path_Smooth.header.frame_id = "odom";
		pathSmooth_pub_.publish (path_Smooth);

		// publish pose array path smooth
		geometry_msgs::PoseArray poseArr;
		for (size_t i = 0; i < (uint16_t)(pathSmooth.size ()); i++)
		{
			geometry_msgs::Pose pose;

			pose.position.x = pathSmooth[i].first;
			pose.position.y = pathSmooth[i].second;

			poseArr.poses.push_back (pose);
		}

		poseArr.header.frame_id = "odom";
		pathPoseArray_pub_.publish (poseArr);

	#endif

		// Calculate_Path_Distance      w[2]
		std::vector <double> dist;
		if (!Calculate_Path_Distance (pathSmooth, dist))
		{
			printf ("ERROR Calculate_Path_Distance \n");
			return;
		}
		
		// CALCULATE RADIUS          	w[3]
		std::vector <double> radius;
		std::vector <double> diff_angle;
 		if (!Calculate_Radius (pathSmooth, dist, radius, diff_angle))
		{
			printf ("ERROR Calculate_Radius \n");
			return;	
		}

		// CALCULATE DESIRED VELOCITY   w[4]
		std::vector <double> desired_velocity;
		double max_vel = (double)(m_max_speed / 1000.0);
		// ROS_INFO ("max_vel = %f", max_vel);
		if (!Calculate_Desired_Velocity (radius, desired_velocity, max_vel, 0.6))
		{
			printf ("ERROR Calculate_Desired_Velocity \n");
			return;
		}

		// Add_Acce_Limits_Velocity     w[5]
		std::vector <double> acce_limits_vel;
		// if (!Add_Acce_Limits_Velocity (dist, desired_velocity, acce_limits_vel, 
		// 										-0.3, 0.2, 0.8, 0.01))
		if (!Add_Acce_Limits_Velocity (dist, desired_velocity, acce_limits_vel, 
												-0.25, 0.1, 0.4, 0.01))
		{
			printf ("ERROR Add_Acce_Limits_Velocity \n");
			return;
		}

		std::vector <double> smth_vel;
		if (!smoothing_velocity_for_robot (	acce_limits_vel, radius, diff_angle, smth_vel))
		{
			printf ("ERROR smoothing_velocity_for_robot \n");
			return;
		}

#if 0
		printf ("Calculate_Path_Distance\n");
		printf_data_Pp (dist, false);

		printf ("CALCULATE RADIUS\n");
		printf_data_Pp (radius, true);
		printf_data_Pp (diff_angle, true);

		printf ("CALCULATE DESIRED VELOCITY\n");
		printf_data_Pp (desired_velocity, false);

		printf ("Add_Acce_Limits_Velocity\n");
		printf_data_Pp (acce_limits_vel, false);

		printf ("DONE\n");
#endif

		/*
				ASSIGN DATA

		*/
		path_Pp_.clear ();
		path_Pp_.resize ((uint16_t)(pathSmooth.size ()));
		for (int i = 0; i < (uint16_t)(pathSmooth.size ()); i++)
		{
			path_Pp_[i].first = pathSmooth[i].first;
			path_Pp_[i].second = pathSmooth[i].second;
		}

		vel_acce_Pp_.clear ();
		vel_acce_Pp_.resize ((uint16_t)(smth_vel.size()));
		for (int i = 0; i < (uint16_t)(smth_vel.size()); i++)
		{
			vel_acce_Pp_[i] = smth_vel[i];
		}
		
		isVelPureReady_ = true;
	}

	void VFHPlannerROS::shotedCallback (const std_msgs::Bool& msg)
	{
		ROS_INFO ("Shoted Callback");
		isShoted = msg.data;
	}

	void VFHPlannerROS::setMaxSpeedCallback (const std_msgs::Float32& msg)
	{
		ROS_INFO ("setMaxSpeed Callback");
		m_max_speed = msg.data;
	}

	// todo: mo phong gia tri van toc cua robot bi sai -> khong dung 
	// m_robotVel khi mo phong (chua co thoi gian Fix)
	void VFHPlannerROS::odomCallback (const nav_msgs::Odometry::ConstPtr& msg)
	{
		boost::mutex::scoped_lock lock(odom_mutex_);
		base_odom_.twist.twist.linear.x = msg->twist.twist.linear.x;
		base_odom_.twist.twist.linear.y = msg->twist.twist.linear.y;
		base_odom_.twist.twist.angular.z = msg->twist.twist.angular.z;
		m_robotVel = msg->twist.twist.linear.x * 1000.0;
	}

    // Tinh tien khoang cach tu laser ve tam Robot
	// need update: (step degree: 0.5)
	void VFHPlannerROS::scanCallback (const sensor_msgs::LaserScan::ConstPtr& scan_msg)
	{
		unsigned int n = scan_msg->ranges.size();  // n = 720 - 180 degree - PI

		int step 		= 1;
		int startIndex 	= (- M_PI/2 - scan_msg->angle_min) /scan_msg->angle_increment;

		float laserSpan 		= scan_msg->angle_max - scan_msg->angle_min;
		float rays_per_degree 	= (M_PI/180.0)/scan_msg->angle_increment;

		// assign m_laser is range max
		for (unsigned i = 0; i <= 180; i++)
		{
			double r = scan_msg->range_max * 1000.0;
			m_laser_ranges[i*2][0] = r;
			m_laser_ranges[i*2 + 1][0] = r;
		}

		double r_start = scan_msg->range_max * 1000;
		double r_end = r_start;

		int tf_phi = 0;
		int index_start = 180;
		int index_end = tf_phi;
		for (unsigned i = 0; i < 180; i++)
		{
			step = int(rays_per_degree * i);
			int iGet = startIndex + step;

			if (iGet <= (n - 1) && iGet >= 0)
			{
				int r_laser = 0;
				if (scan_msg->ranges[iGet] > scan_msg->range_max)
				{
					r_laser = (int) (scan_msg->range_max*1000);
					// continue;
				}
				else
				{
					r_laser = (int) (scan_msg->ranges[iGet]*1000);
				}

				double phi = i * M_PI / 180;
				int x = r_laser * std::cos (phi);
				int y = r_laser * std::sin (phi);

				int dis = y + TF_LASER_DISTANCE;

				double r = sqrt(x * x + dis * dis);
				double p_laser = atan (fabs(x) / dis);
				if (i < 90)
				{	
					tf_phi = (int)((M_PI_2 - p_laser) * 180 / M_PI);
				}
				else
				{
					tf_phi = (int)((M_PI_2 + p_laser) * 180 / M_PI);
				}

				if (m_laser_ranges[tf_phi*2][0] > r)
				{
					m_laser_ranges[tf_phi*2][0] = r;
					m_laser_ranges[tf_phi*2 + 1][0] = r;
				}

				if (index_start > tf_phi)
				{
					index_start = tf_phi;
					r_start = r;
				}

				if (index_end < tf_phi)
				{
					index_end = tf_phi;
					r_end = r;
				}
			}
		}

		for (int i = 0; i < index_start; i++)
		{
			m_laser_ranges[i*2][0] = r_start;
		}

		for (int i = index_end; i <= 180; i++)
		{
			m_laser_ranges[i*2][0] = r_end;
		}

		//#tag: thuc te: #if 0
		// mo phong gazebo: #if 1
#if 0   //HieuTa add: sai khac do mo phong (nguoc chieu) (tesed)
		for (int i = 0; i < 90; i++)
		{
			double tmp_range = m_laser_ranges[i * 2][0];
			m_laser_ranges[i * 2][0] = m_laser_ranges[(180 - i) * 2][0];
			m_laser_ranges[(180 - i) * 2][0] = tmp_range;

			tmp_range = m_laser_ranges[i * 2 + 1][0];
			m_laser_ranges[i * 2 + 1][1] = m_laser_ranges[(180 - i) * 2 + 1][0];
			m_laser_ranges[(180 - i) * 2 + 1][0] = tmp_range;
		}
#endif
	}

	// rate: 30 Hz
	bool VFHPlannerROS::computeVelocityCommands(geometry_msgs::Twist& cmd_vel)
	{
		if(!initialized_)
		{
			ROS_ERROR("This planner has not been initialized, please call initialize() before using this planner");
			return false;
		}

		static uint16_t cnt_is_pure_ready = 1;
		if (isVelPureReady_ == false)
		{
			ROS_INFO ("isVelPureReady_ = FALSE");
			cnt_is_pure_ready ++;

			if (cnt_is_pure_ready > 100)
			{
				return false;
			}

			return true;
		}
		cnt_is_pure_ready = 1;

		if (path_Pp_.empty () || vel_acce_Pp_.empty())
		{
			ROS_INFO ("path_Pp_ OR vel_acce_Pp_ are EMPTY");
			return false;
		}

		std::vector<geometry_msgs::PoseStamped> local_plan;
		geometry_msgs::PoseStamped global_pose;
		if(!costmap_ros_->getRobotPose(global_pose)) 
		{ 
			return false; 
		}

#if 0
		// wait signal when Robot was shoted by human
		if (isShoted == true)
		{
			return true;
		}
#endif
		
		// get the global plan in our frame
		/**transformGlobalPlan(const tf::TransformListener& tf, const std::vector<geometry_msgs::PoseStamped>& global_plan, 
			 const costmap_2d::Costmap2DROS& costmap, const std::string& global_frame, 
			std::vector<geometry_msgs::PoseStamped>& transformed_plan);
		* @brief  Transforms the global plan of the robot from the planner frame to the local frame
		* @param tf A reference to a transform listener
		* @param global_plan The plan to be transformed
		* @param costmap A reference to the costmap being used so the window size for transforming can be computed
		* @param global_frame The frame to transform the plan to
		* @param transformed_plan Populated with the transformed plan
		*/
		std::vector<geometry_msgs::PoseStamped> transformed_plan;
		if(!base_local_planner::transformGlobalPlan(*tf_, global_plan_, global_pose, *costmap_ros_->getCostmap(), global_frame_, transformed_plan))
		{
			ROS_INFO("Could not transform the global plan to the frame of the controller");
			return false;
		}

		base_local_planner::prunePlan(global_pose, transformed_plan, global_plan_);
		if(transformed_plan.empty())
		{
			ROS_INFO ("GLOBAL PATH IS EMPTY");
			return false;
		}

		costmap_2d::Costmap2D* costmap_ = costmap_ros_->getCostmap ();
		geometry_msgs::PoseStamped Ggoal_end = transformed_plan.back();
		geometry_msgs::PoseStamped Ggoal = Ggoal_end;

		unsigned int mx_ = 0, my_ = 0;
		double wx_ = 0, wy_ = 0;

		bool getGoal = false;
		uint8_t cnt_occped = 0;
		std::vector<geometry_msgs::PoseStamped>::iterator it = transformed_plan.begin();
		while(it != transformed_plan.end())
		{
			const geometry_msgs::PoseStamped& w = *it;
			double wx_ = w.pose.position.x;
			double wy_ = w.pose.position.y;

			if(costmap_->worldToMap(wx_, wy_, mx_, my_))
			{
				int16_t costPoint = costmap_->getCost(mx_, my_);
				if (costPoint > 240 && costPoint <= 254) 	// costPoint invalid: 240 < x <= 254
				{
					cnt_occped++;
					if (cnt_occped > 3)
					{
						ROS_INFO ("<--------REPLANNING--------->");
						return false;
					}
				}
				else
				{
					cnt_occped = 0;
				}

				if (getGoal == false)
				{
					double distance_sq = 					\
								base_local_planner::getGoalPositionDistance( global_pose, w.pose.position.x, w.pose.position.y );
					double dis_goal = m_robot_radius * 1.5 / 1000;  // 0.7 * 1.5 = 1.05m
					if(distance_sq > dis_goal)
					{
						getGoal = true;			// get goal look ahead for radius = m_robot_radius * 1.5
						Ggoal = w;
					}
				}
			}

			++it;
		}

		//we assume the global goal is the last point in the global plan
		double goal_x = Ggoal.pose.position.x;
		double goal_y = Ggoal.pose.position.y;

		double robot_global_x  = global_pose.pose.position.x;
		double robot_global_y  = global_pose.pose.position.y;
		double robot_global_th = tf2::getYaw(global_pose.pose.orientation);

		float temp_goal_th = atan((goal_y - robot_global_y)/(goal_x - robot_global_x));
		if(goal_x < robot_global_x)
		{ 
			temp_goal_th = temp_goal_th + M_PI;
		}

		double dist_local = base_local_planner::getGoalPositionDistance(
					global_pose, Ggoal_end.pose.position.x, Ggoal_end.pose.position.y);

		if(dist_local <= xy_goal_tolerance_)
		{
			double diff_yaw = base_local_planner::getGoalOrientationAngleDifference(global_pose, tf2::getYaw(Ggoal_end.pose.orientation));
			if(diff_yaw <= yaw_goal_tolerance_)
			{
				ROS_INFO("Goal pose and orientazione is Reached");
				cmd_vel.linear.x = cmd_vel.linear.y = cmd_vel.angular.z = 0.0;

				//publish an empty plan because we've reached our goal position
				base_local_planner::publishPlan(transformed_plan, g_plan_pub_);
				base_local_planner::publishPlan(local_plan, l_plan_pub_);

				reached_goal_ = true;
			}
			else
			{
				cmd_vel.linear.x = cmd_vel.linear.y = 0;
				cmd_vel.angular.z = (DEG2RAD(m_min_turnrate));
			}

			return true;
		}
		
		float desiredAngle = angles::to_degrees(angles::shortest_angular_distance(tf2::getYaw(global_pose.pose.orientation), temp_goal_th)); // degree
		float desiredDist = base_local_planner::getGoalPositionDistance(global_pose, goal_x, goal_y) * 1000; // (mm)
		float currGoalDistanceTolerance = 80;

        int16_t indexClosest = -1;

        pPure pose;
        pose.first = robot_global_x;
        pose.second = robot_global_y;

		if (!path_Pp_.empty ())
		{
			indexClosest = Get_Closest_Point    ( 	path_Pp_, 
													pose, 
													0, 
													(uint16_t)(path_Pp_.size ()) - 1);
		}
		else
		{
			return false;
		}

		ROS_INFO ("indexClosest = [%d], max_vel = [%f]", indexClosest, vel_acce_Pp_[indexClosest]);
		m_vfh->SetMaxSpeed ((int)(vel_acce_Pp_[indexClosest] * 1000), 0.75);

		double diffSeconds = 0.02;
		int turnrate = 0;
		int speed = 0;
		if (false == m_vfh->Update_VFH (	m_laser_ranges,
											(int) (m_robotVel),
											desiredAngle + 90,
											desiredDist, 
											currGoalDistanceTolerance, 
											speed,
											turnrate, 
											diffSeconds))
		{
			ROS_INFO ("VFH false get vel");
			return false;	
		}

		// ROS_INFO("(END):chosen_speed %d, turnrate %d", speed, turnrate);

#if 1	// View direction chosen_turnrate

		nav_msgs::Odometry dict_msg;
		dict_msg.header.frame_id = "odom";
		dict_msg.pose.pose.position.x = robot_global_x;
		dict_msg.pose.pose.position.y = robot_global_y;

		double yaw_direct = tf2::getYaw(global_pose.pose.orientation) + DEG2RAD(turnrate);
		dict_msg.pose.pose.orientation = tf::createQuaternionMsgFromYaw(yaw_direct);
		directGoal_pub_.publish (dict_msg);

#endif

#if 0	// using PID

		if (speed <= 0)
		{
			ROS_INFO("(END):chosen_speed %d, chosen_turnrate %d", speed, turnrate);

			cmd_vel.linear.x = (float)(speed) / 1000.0;
			cmd_vel.angular.z = DEG2RAD(turnrate);

			base_local_planner::publishPlan(transformed_plan, g_plan_pub_);
			base_local_planner::publishPlan(local_plan, l_plan_pub_);
			return true;
		}

		int isDirect = (turnrate >= 0) ? 1 : -1;
		turnrate = std::abs (turnrate);

		double dt = diffSeconds;
		if ( (diffSeconds > 0.25) || (diffSeconds < 0.015) )
		{
			dt = 0.03; // 30hz
		}

		double target_pid = pid.PID_Calculate (0, turnrate, dt);
		ROS_INFO("(END_NEXT):chosen_speed %d, turnrate %d", speed, int (isDirect * target_pid));

		cmd_vel.linear.x = (float)(speed) / 1000.0;
		cmd_vel.angular.z = -isDirect * DEG2RAD(target_pid);

		// if (std::abs (turnrate) > m_max_turnrate_0ms)
		// {
		// 	ROS_INFO("(END_2):chosen_speed %d, turnrate %d", speed, turnrate);
		// 	return false;
		// }

#else
		ROS_INFO("(END):chosen_speed %d, chosen_turnrate %d", speed, turnrate);

		cmd_vel.linear.x = (float)(speed) / 1000.0;
		cmd_vel.angular.z = DEG2RAD(turnrate);
#endif

		base_local_planner::publishPlan(transformed_plan, g_plan_pub_);
		base_local_planner::publishPlan(local_plan, l_plan_pub_);
		return true;
  	}

	bool VFHPlannerROS::isGoalReached()
	{
		if(!initialized_)
		{
			ROS_ERROR("This planner has not been initialized, please call initialize() before using this planner");
			return false;
		}

		//return flag set in controller
		return reached_goal_; 
	}

	bool VFHPlannerROS::setPlan(const std::vector<geometry_msgs::PoseStamped>& orig_global_plan)
	{
		if(!initialized_)
		{
			ROS_ERROR("This planner has not been initialized, please call initialize() before using this planner");
			return false;
		}

		//reset the global plan
		global_plan_.clear();
		global_plan_ = orig_global_plan;

		ROS_INFO("SET NEW PLAN");
		//when we get a new plan, we also want to clear any latch we may have on goal tolerances
		xy_tolerance_latch_ = false;

		reached_goal_ = false;

		// isShoted = false;

		return true;
	}

	/*  DRP algorithm  */
	// yaw_origin: (rad) in start
	float VFHPlannerROS::diff_angle_line_2_angle_origin (ALG::Point start, ALG::Point end, float yaw_origin)
	{
		float angle = atan((end.second - start.second)/(end.first - start.first));
		if(end.first < start.first)
		{ 
			angle = angle + M_PI;
		}
		angle = angles::to_degrees(angles::shortest_angular_distance (yaw_origin, angle));

		return angle;
	}

	// khoang cach tu mot diem den mot duong thang
	float VFHPlannerROS::distance_to_line (ALG::Point start, ALG::Point end, ALG::Point p)
    {
        // p: position point
        double up_eq = fabs ((end.second - start.second) * p.first           	\
                                    - (end.first - start.first) * p.second  	\
                                    + (end.first * start.second)                \
                                    - (end.second * start.first));

        double lo_eq = sqrt (pow (end.second - start.second, 2) + pow(end.first - start.first, 2));

        double distance = 0;
		if (lo_eq != 0)
		{
			distance = up_eq / lo_eq;
		}
		else
		{
			distance = 0;
		}

        return distance;
    }

	/*
          print_data pure pursuit
      */
	void VFHPlannerROS::printf_data_Pp (const std::vector<double>& data, bool isPrint)
	{
		if (isPrint)
		{
			printf ("----------------------\n");
			uint16_t len = data.size ();
			for (int i = 0; i < len; i++)
			{
				printf ("[%d] - [%f]\n", i, data[i]);
			}
		}
	}
};