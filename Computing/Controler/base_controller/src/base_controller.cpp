#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <std_msgs/Float64.h>
#include <stdlib.h>
#include <thread>
#include <mutex>
#include <math.h>

// #include "base_controller/subctr_simple_status.h"

class BaseControl
{
public:
    BaseControl(): private_nh("~")
    {
        private_nh.param<std::string>("left_rpm_topic", left_rpm_topic, "left_drive/velocity");
        private_nh.param<std::string>("right_rpm_topic", right_rpm_topic, "right_drive/velocity");
        private_nh.param<std::string>("cmd_vel_topic", cmd_vel_topic, "cmd_vel");
        private_nh.param<std::string>("simple_status_topic", simple_status_topic, "simple_status_topic");
        private_nh.param("drive_distance", dis, 0.765);
        private_nh.param("wheel_radius", radius, 0.1957);
        private_nh.param("RATIO", RATIO, 7);
        
        /* publisher topic to drMotor */
        left_rpm_pub        = nh.advertise<std_msgs::Float64>(left_rpm_topic, 10);
        right_rpm_pub       = nh.advertise<std_msgs::Float64>(right_rpm_topic, 10);

        /* subscriber topic from control_mux */
        cmd_vel_sub         = nh.subscribe(cmd_vel_topic,10,&BaseControl::CmdVelocityCallback,this);

        /*  LOG param to console  */
        ROS_INFO_STREAM("left_rpm_topic: " << this->left_rpm_topic );
        ROS_INFO_STREAM("right_rpm_topic: " << this->right_rpm_topic );
	    ROS_INFO_STREAM("cmd_vel_topic: " << this->cmd_vel_topic );
        ROS_INFO_STREAM("simple_status_topic: " << this->simple_status_topic );
        ROS_INFO_STREAM("drive_distance: " << this->dis );
        ROS_INFO_STREAM("wheel_radius: " << this->radius );
        ROS_INFO_STREAM("RATIO: " << this->RATIO );
    }

    ~BaseControl() {}

private:
    ros::NodeHandle nh, private_nh;
    ros::Timer updateTimer;
    ros::Publisher left_rpm_pub, right_rpm_pub;
    ros::Subscriber cmd_vel_sub;
    ros::Subscriber simple_status_sub;

    // Parameters
    std::string left_rpm_topic,right_rpm_topic,cmd_vel_topic, simple_status_topic;
    
    double dis;                 // Khoảng cách giữa Left Drive Và Right Drive [m]
    double radius;              // Bán kính bánh xe [m]
    int RATIO;

    double left_rpm = 0;
    double right_rpm = 0;

    bool left_soft_brake_status = true;
    bool left_hard_brake_status = true;
    bool right_soft_brake_status = true;
    bool right_hard_brake_status = true;
    bool sctr_is_alive = false;

    // Methos
    void CmdVelocityCallback(const geometry_msgs::Twist& msg)
    {
        double linear = 0;
        double angular = 0;

        linear = msg.linear.x;
        angular = -msg.angular.z;

        double vr, vl;  // [m/s]

	    vr = linear - angular*this->dis * 0.5;
	    vl = linear + angular*this->dis * 0.5;

        //todo: base controller
        this->left_rpm = (vl / this->radius) * 60 * RATIO / (2 * M_PI);
        this->right_rpm = (vr / this->radius) * 60 * RATIO / (2 * M_PI);

        std_msgs::Float64 msgleft, msgright;
        msgleft.data = this->left_rpm;
        msgright.data = this->right_rpm;
        
        left_rpm_pub.publish(msgleft);
        right_rpm_pub.publish(msgright);
    }
};

int main(int argc, char *argv[])
{
    ros::init(argc, argv, "BaseController");
    BaseControl baseController;
    //baseController.run();
    ros::spin();
    ROS_INFO_STREAM("Out of node: BaseController");
    return 0;
}