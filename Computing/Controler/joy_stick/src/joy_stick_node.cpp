#include "joy_stick_node/joy_stick_node.h"

namespace JOYSTICK
{
    joy_stick_node::joy_stick_node () :
        vel_liner_robot_ (0),
        vel_argu_robot_ (0),
        nh_private_ ("~")
    {
        status_data = STATUS_ERROR;

        time_button_joystick_ = ros::Time::now ();

        nh_private_.param ("linear_max_vel", linear_max_vel_, 1.0);
        nh_private_.param ("linear_min_vel", linear_min_vel_, 0.0);
        nh_private_.param ("angular_vel",    angular_vel_, 0.5);
        nh_private_.param ("freq",           freq_,    20.0);

        status_data = STATUS_OK;
    }

    joy_stick_node::~joy_stick_node () {}

    void joy_stick_node::joy_call_back (const sensor_msgs::Joy::ConstPtr& joy_msg)
    {

        const double  TIME_UNIT = 0.02;
        const uint8_t PERCENT_INC = 10;
        const double  MAX_LINEAR = 3.0;
        const double  MIN_LINEAR = 0.2;
        
        const double  MAX_ANGULAR = 1.5;
        const double  MIN_ANGULAR = 0.2;
        
        ros::Time current_time = ros::Time::now ();

        double dt = (current_time - time_button_joystick_).toSec ();

        if (dt > TIME_UNIT && 
                (joy_msg->buttons[4] + joy_msg->buttons[5] + joy_msg->buttons[6] + joy_msg->buttons[7]))
        {
            if (joy_msg->buttons[5])  // inc linear
            {
                linear_max_vel_ += linear_max_vel_ * PERCENT_INC / 100;

                if (linear_max_vel_ > MAX_LINEAR)
                {
                    linear_max_vel_ = MAX_LINEAR;
                }
            }
            if (joy_msg->buttons[7])
            {
                linear_max_vel_ -= linear_max_vel_ * PERCENT_INC / 100;

                if (linear_max_vel_ < MIN_LINEAR)
                {
                    linear_max_vel_ = MIN_LINEAR;
                }
            }
            if (joy_msg->buttons[4])
            {
                angular_vel_ += angular_vel_ * PERCENT_INC / 100;

                if (angular_vel_ > MAX_ANGULAR)
                {
                    angular_vel_ = MAX_ANGULAR;
                }
            }
            if (joy_msg->buttons[6])
            {
                angular_vel_ -= angular_vel_ * PERCENT_INC / 100;

                if (angular_vel_ < MIN_ANGULAR)
                {
                    angular_vel_ = MIN_ANGULAR;
                }
            }

            time_button_joystick_ = current_time;

            ROS_INFO ("vel_lin = [%f] ------- vel_ang = [%f]", linear_max_vel_, angular_vel_);
        }

        status_data = STATUS_ERROR;

        vel_liner_robot_ = (linear_max_vel_ - linear_min_vel_) * joy_msg->axes[1];

        vel_argu_robot_  = (joy_msg->axes[3]) * angular_vel_;
        
        status_data = STATUS_OK;
    }

    status_t joy_stick_node::joy_publish ()
    {
        if (    vel_.linear.x       == 0.0       &&   \
                vel_liner_robot_    == 0.0       &&   \
                vel_.angular.z      == 0.0       &&   \
                vel_argu_robot_     == 0.0)
        {
            return STATUS_OK;
        }

        // cal vel_pub_ from joy_msg_ptr
        if (status_data == STATUS_OK)
        {
            vel_.linear.x = vel_liner_robot_;
            vel_.angular.z = vel_argu_robot_;
        }

        vel_pub_.publish (vel_);

        status_data = STATUS_OK;

        return STATUS_OK;
    }

    void joy_stick_node::spin_timer (const ros::TimerEvent& e)
    {
        if(status_data == STATUS_OK)
        {
            joy_publish ();
        }        
    }

    void joy_stick_node::joy_run ()
    {
        ROS_INFO ("joy run");

        vel_pub_ = nh_private_.advertise <geometry_msgs::Twist> ("/Joystick/cmd_vel", 10);

        joy_stick_sub_ = nh_.subscribe ("joy", 1, &joy_stick_node::joy_call_back, this);

        timer_ = nh_private_.createTimer (ros::Duration (1.0 / std::max (freq_, 1.0)), &joy_stick_node::spin_timer, this);
    }
}

int main (int argc, char** argv)
{
    ros::init(argc, argv, "joy_stick_node");

    JOYSTICK::joy_stick_node  joyStick;

    joyStick.joy_run ();

    ros::spin ();

    return 0;
}