#ifndef JOY_STICK_NODE_H_
#define JOY_STICK_NODE_H_

// #ifdef __cplusplus
// extern "C"
// {
// #endif

#include <ros/ros.h>
#include <sensor_msgs/Joy.h>
#include <geometry_msgs/Twist.h>

#include <algorithm> 
#include <iostream>
#include <fcntl.h>
#include <stdint.h>
#include <stdlib.h> 
#include <math.h>

namespace JOYSTICK
{
    typedef enum status
    {
        STATUS_OK,
        STATUS_ERROR

    } status_t;

    class joy_stick_node 
    {
        private:
            ros::NodeHandle         nh_private_, nh_;

            ros::Timer              timer_;

            ros::Time               time_button_joystick_;

            ros::Publisher          vel_pub_;

            ros::Subscriber         joy_stick_sub_;

            sensor_msgs::Joy        joy_;

            geometry_msgs::Twist    vel_;

            double                  vel_liner_robot_;

            double                  vel_argu_robot_;

            int8_t                  status_data;

            double                  linear_max_vel_;

            double                  linear_min_vel_;

            double                  angular_vel_;

            double                  freq_;

            void    joy_call_back (const sensor_msgs::Joy::ConstPtr& joy_msg);

            status_t joy_publish ();

            void spin_timer (const ros::TimerEvent& e);

        public:
            joy_stick_node ();

            ~joy_stick_node ();

            void joy_run ();
    };
}

// #ifdef __cplusplus
// }
// #endif

#endif // JOY_STICK_NODE_H_

