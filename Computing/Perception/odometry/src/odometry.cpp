#include "odometry/odometry.h"

using namespace tf;

namespace ns_odom_enocder
{
    odometry::odometry() : 
            nh_                         (""),
            nh_private_                 ("~"),
            PrevLeftEncoderCounts_      (0),
            PrevRightEncoderCounts_     (0),
            t_left_                     (0),
            t_right_                    (0),
            speed_left_                 (0.0),
            speed_right_                (0.0),
            isActiveNode_               (false),
            yaw_imu_                    (0.0),
            imu_offSet_                 (0.0),
            calcOdom_thread_            (NULL),
            last_time_tick_             (ros::Time::now ())
    {
                // parameter
        if(!nh_private_.getParam("freq", freq_))
            freq_ = 30.0;
        if(!nh_private_.getParam("imu_enable", imu_enable_))
            imu_enable_ = true;
        if(!nh_private_.getParam("isPublish_tfOdom", isPublish_tfOdom_))
            isPublish_tfOdom_ = true;
        if(!nh_private_.getParam("isPublish_Odom", isPublish_Odom_))
            isPublish_Odom_ = true;
        if(!nh_private_.getParam("odom_frame_id", odom_frame_id_))
            odom_frame_id_ = "odom_encoder";
        if(!nh_private_.getParam("odom_child_frame_id", odom_child_frame_id_))
            odom_child_frame_id_ = "base_link";
    }

    odometry::~odometry() 
    {
        if(calcOdom_thread_)
        {
            calcOdom_thread_->join();
            delete calcOdom_thread_;
        }

        delete tfB_;
    }

    /* initial */
    void odometry::run ()
    {   
        InitOdom ();

        odom_pub_        = nh_.advertise <nav_msgs::Odometry> ("/odom_encoder", 10);
        imu_pub_         = nh_.advertise <sensor_msgs::Imu> ("/imu", 10);

        imu_sub_         = nh_.subscribe ("/imu_raw", 1, &odometry::imu_cb, this);
        tickLeft_sub_    = nh_.subscribe                  \
                                ("left_drive/status/encoder_stick", 1, &odometry::tickLeft_cb, this);
        tickRight_sub_   = nh_.subscribe                  \
                                ("right_drive/status/encoder_stick", 1, &odometry::tickRight_cb, this);
        speedLeft_sub_   = nh_.subscribe                  \
                                ("left_drive/status/encoder_speed", 1, &odometry::speedLeft_cb, this);
        speedRight_sub_  = nh_.subscribe                  \
                                ("right_drive/status/encoder_speed", 1, &odometry::speedRight_cb, this);
        imu_offSet_sub_  = nh_.subscribe                  \
                                ("IMU/offSet", 1, &odometry::ImuOffSet_cb, this);
        odom_offSet_sub_ = nh_.subscribe                  \
                                ("ODOM/offSet", 1, &odometry::OdomOffSet_cb, this);

        tfB_ = new tf::TransformBroadcaster();
        calcOdom_thread_ = new boost::thread(boost::bind(&odometry::CalcOdomThread, this, freq_));

        if (imu_enable_ == false)
        {
            isActiveNode_ = true;
        }
    }

    double odometry::Quaternion2Yaw (const geometry_msgs::Quaternion& orien)
    {
        double roll, pitch, yaw;
        tf::Quaternion q ( orien.x, orien.y, orien.z, orien.w);
        tf::Matrix3x3 m(q);
        m.getRPY (roll, pitch, yaw);

        return yaw;
    }

    void odometry::CalcOdomThread (float freq)
    {
        if(freq == 0)
        {
            ROS_INFO ("Freq = 0Hz");
            return; 
        }

        ros::Rate r(freq);
        while(ros::ok())
        {
            static bool init_prin = true;
            if (isActiveNode_)
            {
                if (init_prin == false)
                {
                    ROS_INFO ("Node is active");
                    init_prin = true;
                }

                ros::Time cur_time = ros::Time::now ();
                double dt = (cur_time - last_time_tick_).toSec ();
                if (dt > 0.0)
                {
                    odom_mtx_.lock ();
#ifdef  USE_STICK
                    // Calc odom from encoder
                    CalcOdomEncoderStick (odom_.pose.pose, t_left_, t_right_, 
                                        speed_left_, speed_right_, 0.0001, dt);
#elif   USE_SPEED
                    // Calc odom from encoder
                    CalcOdomEncoderSpeed (odom_.pose.pose, t_left_, t_right_, 
                                        speed_left_, speed_right_, 0.0001, dt);
#endif
                    odom_mtx_.unlock ();
                }

                publish_tf ();
                last_time_tick_ = cur_time;
            }
            else
            {
                if (init_prin && imu_enable_)
                {
                    ROS_INFO ("Waiting IMU");
                    init_prin = false;
                }
            }

            r.sleep();
        }
    }

    void odometry::imu_cb (const sensor_msgs::Imu::ConstPtr& imu_msg)
    {
        if (imu_enable_)
        {
            if (isActiveNode_ == false)
            {
                isActiveNode_ = true;
            }

            yaw_imu_ = Quaternion2Yaw (imu_msg->orientation) + imu_offSet_ * M_PI / 180;

            sensor_msgs::Imu imu = *imu_msg;
            imu.orientation = tf::createQuaternionMsgFromYaw(yaw_imu_);
            imu_pub_.publish (imu);
        }
    }

    void odometry::tickLeft_cb (const std_msgs::Int64& msg)
    {
        t_left_ = msg.data;
    }

    void odometry::tickRight_cb (const std_msgs::Int64& msg)
    {
        t_right_ = msg.data;
    }

    void odometry::InitOdom ()
    {
        odom_.pose.pose.position.x = 0;
        odom_.pose.pose.position.y = 0;
        odom_.pose.pose.position.z = 0;
        odom_.pose.pose.orientation = tf::createQuaternionMsgFromYaw(yaw_imu_);
    }

#ifdef  USE_STICK
void 
    odometry::CalcOdomEncoderStick ( const geometry_msgs::Pose& pose, int32_t tick_left,
                                          int32_t tick_right, double speed_left, 
                                          double speed_right, double error, double dt)
    {
        double L_distance = (tick_left - PrevLeftEncoderCounts_) * DISTANCE_PER_COUNT;
        double R_distance = (tick_right - PrevRightEncoderCounts_) * DISTANCE_PER_COUNT;
        double yaw_prev = Quaternion2Yaw (pose.orientation);

#if 1
        if (L_distance == 0 && R_distance == 0)
        {
            odom_.twist.twist.linear.x      = 0.0;
            odom_.twist.twist.linear.y      = 0.0;
            odom_.twist.twist.angular.z     = 0.0;
            
            if (imu_enable_)
            {
                odom_.pose.pose.orientation = tf::createQuaternionMsgFromYaw(yaw_imu_);
            }

            return;
        }
#endif

        double v_left = speed_left;
        double v_right = speed_right;

        double alpha, radius, Ox, Oy, Px_n, Py_n, Pth_n; 
        if (std::fabs (R_distance - L_distance) < (double)(error * LEN_DISTANCE_TWO_WHEELS))  //di thang
        {
            if (imu_enable_)
            {
                Pth_n = yaw_imu_;
            }
            else 
            {
                Pth_n = yaw_prev;
            }

            Px_n = pose.position.x + (L_distance + R_distance) * std::cos (Pth_n) / 2;
            Py_n = pose.position.y + (L_distance + R_distance) * std::sin (Pth_n) / 2;
        }
        else
        {
            // alpha need calib for envirment
            alpha = (double) (R_distance - L_distance) / LEN_DISTANCE_TWO_WHEELS;
            double R = (L_distance / alpha) + (LEN_DISTANCE_TWO_WHEELS / 2.0);

            Ox = pose.position.x - R * std::sin (yaw_prev);
            Oy = pose.position.y + R * std::cos (yaw_prev);

            Pth_n = std::fmod((yaw_prev + alpha), (2 * PI));
            Px_n = Ox + R * std::sin (Pth_n);
            Py_n = Oy - R * std::cos (Pth_n);

            if (imu_enable_)
            {
                Pth_n = yaw_imu_;
            }
        }
        
        ROS_INFO ("Px_n = %f, Py_n = %f, Pth_n = %f", Px_n, Py_n, Pth_n * 180 / PI);
        ROS_INFO ("Speed: x = [%.3f] - [%.3f], dt = [%.3f]", (v_left + v_right)/2.0, (speed_left_ + speed_right_) / 2.0, dt);

        geometry_msgs::Quaternion odom_quat = tf::createQuaternionMsgFromYaw(Pth_n);

        /* set the position */
        odom_.pose.pose.position.x      = Px_n;
        odom_.pose.pose.position.y      = Py_n;
        odom_.pose.pose.position.z      = 0.0;
        odom_.pose.pose.orientation     = odom_quat;

        /* set the velocity */
        odom_.twist.twist.linear.x      = (v_left + v_right) / 2.0;
        odom_.twist.twist.linear.y      = 0.0;
        odom_.twist.twist.angular.z     = (v_left - v_right) / LEN_DISTANCE_TWO_WHEELS;

        PrevLeftEncoderCounts_ = tick_left;
        PrevRightEncoderCounts_ = tick_right;
    }

#elif   USE_SPEED
void 
    odometry::CalcOdomEncoderSpeed ( const geometry_msgs::Pose& pose, int32_t tick_left,
                                          int32_t tick_right, double speed_left, 
                                          double speed_right, double  error, double dt)
    {
        double L_distance = (tick_left - PrevLeftEncoderCounts_) * DISTANCE_PER_COUNT;
        double R_distance = (tick_right - PrevRightEncoderCounts_) * DISTANCE_PER_COUNT;
        double yaw_prev = Quaternion2Yaw (pose.orientation);

        if (L_distance == 0 && R_distance == 0)
        {
            odom_.twist.twist.linear.x      = (speed_left + speed_right) / 2.0;
            odom_.twist.twist.linear.y      = 0.0;
            odom_.twist.twist.angular.z     = (speed_left - speed_right) / LEN_DISTANCE_TWO_WHEELS;

            if (imu_enable_)
            {
                odom_.pose.pose.orientation = tf::createQuaternionMsgFromYaw(yaw_imu_);
            }

            return;
        }

        double v_left = speed_left;
        double v_right = speed_right;

        double alpha, radius, Ox, Oy, Px_n, Py_n, Pth_n; 
        if (std::fabs (R_distance - L_distance) < (double)(error * LEN_DISTANCE_TWO_WHEELS))  //di thang
        {
            if (imu_enable_)
            {
                Pth_n = yaw_imu_;
            }
            else 
            {
                Pth_n = yaw_prev;
            }

            Px_n = pose.position.x + (L_distance + R_distance) * std::cos (Pth_n) / 2;
            Py_n = pose.position.y + (L_distance + R_distance) * std::sin (Pth_n) / 2;
        }
        else
        {
            // alpha need calib for envirment
            alpha = (double) (R_distance - L_distance) / LEN_DISTANCE_TWO_WHEELS;
            double R = (L_distance / alpha) + (LEN_DISTANCE_TWO_WHEELS / 2.0);

            Ox = pose.position.x - R * std::sin (yaw_prev);
            Oy = pose.position.y + R * std::cos (yaw_prev);

            Pth_n = std::fmod((yaw_prev + alpha), (2 * PI));
            Px_n = Ox + R * std::sin (Pth_n);
            Py_n = Oy - R * std::cos (Pth_n);

            if (imu_enable_)
            {
                Pth_n = yaw_imu_;
            }
        }
        
        ROS_INFO ("Px_n = %f, Py_n = %f, Pth_n = %f", Px_n, Py_n, Pth_n * 180 / PI);

        geometry_msgs::Quaternion odom_quat = tf::createQuaternionMsgFromYaw(Pth_n);

        /* set the position */
        odom_.pose.pose.position.x      = Px_n;
        odom_.pose.pose.position.y      = Py_n;
        odom_.pose.pose.position.z      = 0.0;
        odom_.pose.pose.orientation     = odom_quat;

        /* set the velocity */
        odom_.twist.twist.linear.x      = (v_left + v_right) / 2.0;
        odom_.twist.twist.linear.y      = 0.0;
        odom_.twist.twist.angular.z     = (v_left - v_right) / LEN_DISTANCE_TWO_WHEELS;

        PrevLeftEncoderCounts_ = tick_left;
        PrevRightEncoderCounts_ = tick_right;
    }
#endif

    void odometry::speedLeft_cb (const std_msgs::Float64& msg)
    {
        speed_left_ = msg.data * R_WHEEL * 2 * PI / 420;   // 60 * 7 (60s * ti so truyen)
    }

    void odometry::speedRight_cb (const std_msgs::Float64& msg)
    {
        speed_right_ = msg.data * R_WHEEL * 2 * PI / 420;   // 60 * 7
    }

    void odometry::ImuOffSet_cb (const std_msgs::Float64& msg)
    {
        imu_offSet_ = msg.data;
    }

    void odometry::OdomOffSet_cb (const geometry_msgs::Point& msg)
    {
        odom_mtx_.lock ();
        odom_.pose.pose.position.x += msg.x;
        odom_.pose.pose.position.y += msg.y;
        odom_mtx_.unlock ();
    }

    void odometry::publish_tf ()
    {
        ros::Time current_time = ros::Time::now ();
        /* header odom */
        odom_.header.stamp      = current_time;
        odom_.header.frame_id   = odom_frame_id_;
        odom_.child_frame_id    = odom_child_frame_id_;

        if (isPublish_tfOdom_)
        {
            tf::Transform odom_to_baselink;
            odom_to_baselink = tf::Transform(tf::createQuaternionFromRPY
                    (0, 0, Quaternion2Yaw(odom_.pose.pose.orientation)), tf::Vector3(odom_.pose.pose.position.x, odom_.pose.pose.position.y, 0.0));
            tfB_->sendTransform( tf::StampedTransform (odom_to_baselink, current_time, odom_frame_id_, odom_child_frame_id_));
        }

        if (isPublish_Odom_)
        {
            odom_pub_.publish (odom_);
        }
    }
}