#include "odometry/odometry.h"

int main(int argc, char *argv[])
{
    ros::init(argc, argv, "odometry_node");
    ns_odom_enocder::odometry    odomEncoder;
    odomEncoder.run ();
    ros::spin ();

    return 0;
}