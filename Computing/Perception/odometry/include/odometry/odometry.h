#ifndef ODOMETRY_H_
#define ODOMETRY_H_

/* ros and tf */
#include <ros/ros.h>
#include <tf/tf.h>
#include <tf/transform_listener.h>
#include <tf/transform_broadcaster.h>

/* msgs */
#include <geometry_msgs/Pose.h>
#include <sensor_msgs/Imu.h>
#include <geometry_msgs/Twist.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/Quaternion.h>
#include <std_msgs/Int64.h>
#include <std_msgs/Float64.h>

/* type data */
#include <iostream>
#include <fcntl.h>
#include <stdio.h> 
#include <stdlib.h> 
#include <math.h>

/* ethernet */
#include <string.h> 
#include <unistd.h>

/* boost */
#include <boost/chrono.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread.hpp>

namespace ns_odom_enocder
{
    #define     USE_STICK                   
    // #define     USE_SPEED                   


    // #define     R_WHEEL                     0.1359          // = 0.1902 / 1.4
    // #define     LEN_DISTANCE_TWO_WHEELS     0.765

    #define     R_WHEEL                     0.1625             // = 0.2275 / 1.4
    #define     LEN_DISTANCE_TWO_WHEELS     0.900

    #define     PI                          3.14159265
    #define     N_ENCODER                   70000
    #define     DISTANCE_PER_COUNT          (double) ((2 * PI * R_WHEEL) / N_ENCODER)

    class odometry
    {
        public:
            odometry();
            ~odometry();

            /* initial */
            void run ();

        private:
            void imu_cb (const sensor_msgs::Imu::ConstPtr& imu_msg);
            void tickLeft_cb   (const std_msgs::Int64& msg);
            void tickRight_cb  (const std_msgs::Int64& msg);
            void speedLeft_cb (const std_msgs::Float64& msg);
            void speedRight_cb (const std_msgs::Float64& msg);
            void ImuOffSet_cb (const std_msgs::Float64& msg);
            void OdomOffSet_cb (const geometry_msgs::Point& msg);

            void InitOdom ();
            double Quaternion2Yaw (const geometry_msgs::Quaternion& orien);
            void CalcOdomThread (float freq);

#ifdef  USE_STICK
            void CalcOdomEncoderStick ( const geometry_msgs::Pose& pose, int32_t tick_left,
                                        int32_t tick_right, double speed_left, 
                                        double speed_right, double  error, double dt);
#elif   USE_SPEED
            void CalcOdomEncoderSpeed ( const geometry_msgs::Pose& pose, int32_t tick_left,
                                        int32_t tick_right, double speed_left, 
                                        double speed_right, double  error, double dt);
#endif

            void publish_tf ();

            /* variable */
            ros::NodeHandle         nh_;
            ros::NodeHandle         nh_private_;
            ros::Publisher          odom_pub_;
            ros::Publisher          imu_pub_;
            ros::Subscriber         imu_sub_, imu_offSet_sub_, odom_offSet_sub_;
            ros::Subscriber         tickRight_sub_, tickLeft_sub_;
            ros::Subscriber         speedLeft_sub_, speedRight_sub_;

            tf::TransformBroadcaster* tfB_;

            nav_msgs::Odometry      odom_;
            double                  yaw_imu_;
            double                  imu_offSet_;    // if use imu

            double                  freq_;
            volatile bool           isActiveNode_;
            bool                    isPublish_tfOdom_;
            bool                    isPublish_Odom_;

            std::string             odom_frame_id_;
            std::string             odom_child_frame_id_;

            boost::thread*          calcOdom_thread_;

            /* encoder */
            int32_t                 PrevLeftEncoderCounts_;
            int32_t                 PrevRightEncoderCounts_;
            int32_t                 t_left_, t_right_;

            double                  speed_left_, speed_right_;          // v(t)
            double                  speed_zero_left, speed_zero_right;  // v(t0)

            ros::Time               last_time_tick_;

            /* imu */
            bool                    imu_enable_;

            // odom mutex
            boost::mutex            odom_mtx_;
    };
};

#endif // ODOMETRY_H_