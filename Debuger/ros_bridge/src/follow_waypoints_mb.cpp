#include <ros/ros.h>
#include <tf/tf.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>
#include <geometry_msgs/PoseArray.h>    // std_msgs/Header header
                                        // geometry_msgs/Pose[] poses
#include <std_msgs/String.h>
#include <std_msgs/Int32.h>
#include <std_msgs/Int8.h>
                                        
#include <iostream>
#include <boost/thread.hpp>
#include <thread>
#include <mutex>

using namespace std;


class follow_wp
{
    public:
        typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;

        typedef enum state
        {
            UNKNOW,
            GETPATH,
            FOLLOW_PATH,
            PATH_COMPLETE,

        } e_state;

        typedef enum state_rb
        {
            STOP,       // Robot in state GETPATH or PATH_COMPLETE
            RUN,        // Robot in state FOLLOW_PATH and is running
            WAIT_RUN,   // In time wait send goal next
            PENDING,    // Robot in state FOLLOW_PATH and stoped

        } e_state_rb;

        follow_wp();
    
        ~follow_wp();

        void run ();

    private:

        geometry_msgs::PoseArray    waypoints_rsv;

        MoveBaseClient*             ac;

        e_state                     sm_robot;

        e_state_rb                  s_robot;

        geometry_msgs::PoseArray    Poses;

        ros::NodeHandle             nh;

        ros::Subscriber             wayPoints_sub, control_sub, string_test_sub;

        ros::Publisher              status_pub;
        
        void wayPoints_callback     (const geometry_msgs::PoseArray::ConstPtr& msg);

        void control_callback       (const std_msgs::Int32::ConstPtr& msg);

        void str_callback           (const std_msgs::String::ConstPtr msg);

        void cal_orientation        (const geometry_msgs::PoseArray::ConstPtr& src,
                                           geometry_msgs::PoseArray& dst);

        void threadAcMission        ();
};

follow_wp::follow_wp () : 
    sm_robot (e_state::UNKNOW), 
    s_robot (e_state_rb::STOP)
{
    ac = new MoveBaseClient("/move_base", true);
}

follow_wp::~follow_wp() 
{
    delete ac;
}

void follow_wp::run ()
{
    sm_robot = e_state::GETPATH;
    s_robot = e_state_rb::STOP;

    waypoints_rsv.poses.clear ();

    // sub
    wayPoints_sub = nh.subscribe("/user/wp_mission", 10, &follow_wp::wayPoints_callback, this);
    control_sub = nh.subscribe("/user/control_rb", 10, &follow_wp::control_callback, this);
    string_test_sub = nh.subscribe("/user/str", 10, &follow_wp::str_callback, this);

    // pub
    status_pub = nh.advertise<std_msgs::Int8>("/mission_status", 10);

    int wait_count = 0;

    while(!ac->waitForServer(ros::Duration(5.0)))
    {
        wait_count++;
        if(wait_count > 3)
        {
            ROS_ERROR("move_base action server did not come up.");
            break;
        }
        ROS_INFO("Waiting for the move_base action server to come up");
    }
    
    ROS_INFO ("Connected");
}

void follow_wp::str_callback (const std_msgs::String::ConstPtr msg)
{
    ROS_INFO ("received callback");

    ROS_INFO ("data = [%s]", msg->data.c_str());
}

void follow_wp::cal_orientation (const geometry_msgs::PoseArray::ConstPtr& src,
                                       geometry_msgs::PoseArray& dst)
{
    unsigned int len = src->poses.size ();

    for (int i = 0; i < len; i++)
    {
        // geometry_msgs::Pose
    }
}

void follow_wp::wayPoints_callback (const geometry_msgs::PoseArray::ConstPtr& msg)
{
#if 0

    int LEN = msg->poses.size();

    ROS_INFO ("LEN = [%d]", LEN);

    for (int i = 0; i < LEN; i++)
    {
        ROS_INFO ("x = [%.2f] -- y = [%.2f]", msg->poses[i].position.x, msg->poses[i].position.y);
    }

    geometry_msgs::PoseArray wps;
    wps.poses.clear ();

    cal_orientation (msg, wps);

#else

    int LEN = msg->poses.size();

    ROS_INFO ("LEN = [%d]", LEN);

    for (int i = 0; i < LEN; i++)
    {
        ROS_INFO ("x = [%.2f] -- y = [%.2f]", msg->poses[i].position.x, msg->poses[i].position.y);
    }

    if (sm_robot == e_state::GETPATH)
    {
        waypoints_rsv = *msg;

        sm_robot = e_state::FOLLOW_PATH;

        boost::thread follow_thread(&follow_wp::threadAcMission, this);
        follow_thread.detach();

        ROS_INFO ("(wayPoints_callback) Received path from User App");
    }
    else
    {
        ROS_INFO ("(wayPoints_callback) Reject Path from User App");
    }
#endif
}

void follow_wp::control_callback (const std_msgs::Int32::ConstPtr& msg)
{
    switch (msg->data)
    {
        case 0:     // exit path
            if (sm_robot == e_state::FOLLOW_PATH)
            {
                s_robot = e_state_rb::STOP;
                sm_robot = e_state::GETPATH;

                ac->cancelGoal();

                ROS_INFO ("(control_callback) EXIT PATH FOLLOWING");
            }
            break;

        case 1:     // pending robot
            if(sm_robot == e_state::FOLLOW_PATH && s_robot == e_state_rb::RUN)
            {
                s_robot = e_state_rb::PENDING;
                ROS_INFO ("(control_callback) PENDING ROBOT");
            }
            break;

        case 2:     // run robot pending
            if(sm_robot == e_state::FOLLOW_PATH && s_robot == e_state_rb::PENDING)
            {
                s_robot = e_state_rb::RUN;
                ROS_INFO ("(control_callback) RUN ROBOT");
            }
            break;

        case 3:
            if (sm_robot == e_state::FOLLOW_PATH && s_robot == e_state_rb::RUN)
            {
                ac->cancelGoal();

                ROS_INFO ("(control_callback) SKIP A GOAL FOLLOWING");
            }

        default:

            ROS_INFO ("(control_callback) UNKNOW");
            break;
    }
}

void follow_wp::threadAcMission()
{   
    if (sm_robot == e_state::FOLLOW_PATH)
    {
        //wait for the action server to come up
        while(!ac->waitForServer(ros::Duration(5.0)))
        {
            ROS_INFO("Waiting for the move_base action server to come up");
        }

        move_base_msgs::MoveBaseGoal goal;

        //we'll send a goal to the robot to move 1 meter forward
        goal.target_pose.header.frame_id = "odom";
        goal.target_pose.header.stamp = ros::Time::now();

        unsigned int size_wps = waypoints_rsv.poses.size ();

        ROS_INFO ("size_wps = [%d]", size_wps);

        std_msgs::Int8 msg_mission_status;
        msg_mission_status.data = 1;
        status_pub.publish (msg_mission_status);

        bool reachToMiss = false;
        for (int i = 0; i < size_wps; i++) 
        {
            msg_mission_status.data = 0;
            status_pub.publish (msg_mission_status);

            if (s_robot == e_state_rb::STOP && sm_robot == e_state::GETPATH)
            {
                ROS_INFO ("STOP threadAcMission");
                break;
            }

            goal.target_pose.pose.position.x = waypoints_rsv.poses[i].position.x;
            goal.target_pose.pose.position.y = waypoints_rsv.poses[i].position.y;
            goal.target_pose.pose.orientation = tf::createQuaternionMsgFromYaw(0.0); //waypoints_rsv.poses[i].orientation;

            // odom_.pose.pose.orientation = tf::createQuaternionMsgFromYaw(0.0);

            ROS_INFO("Sending goal");
            s_robot = e_state_rb::RUN;
            ac->sendGoal(goal);

            ac->waitForResult();

            if(ac->getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
            {
                ROS_INFO("Hooray, the base moved 1 meter forward");
                ROS_INFO ("SUCCEEDED");

                if (i == size_wps - 1)
                {
                    reachToMiss = true;
                }
            }
            else
            {
                ROS_INFO("The base failed to move forward 1 meter for some reason");
            }

            if (s_robot == e_state_rb::RUN)
            {
                s_robot = e_state_rb::WAIT_RUN;
            }
        }

        msg_mission_status.data = 2;
        if (reachToMiss == true)
        {
            msg_mission_status.data = 1;
        }
        status_pub.publish (msg_mission_status);

        s_robot = e_state_rb::STOP;
        sm_robot = e_state::PATH_COMPLETE;

        ROS_INFO ("(threadAcMission) PATH_COMPLETE: Ready to receive a new goal path");

        sm_robot = e_state::GETPATH;
        waypoints_rsv.poses.clear ();
    }
    else
    {
        ROS_INFO ("sm_robot is [%d]", sm_robot);
    }
}

int main(int argc, char *argv[])
{
    ros::init(argc, argv, "follow_waypoints_node");
    follow_wp fw;
    fw.run ();
    ros::spin ();

    return 0;
}