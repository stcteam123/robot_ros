#include <ros/ros.h>
#include <ros/package.h>
#include <fstream>
#include <utility>
#include <vector>
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>
#include <robot_localization/navsat_conversions.h>
#include <geometry_msgs/PointStamped.h>
#include <geometry_msgs/PoseArray.h>
#include <std_msgs/Bool.h>
#include <std_msgs/Int32.h>
#include <std_msgs/String.h>
#include <std_msgs/Float64MultiArray.h>
#include <tf/transform_listener.h>
#include <math.h>
#include <list>
#include <thread>
#include <mutex>
#include <unistd.h>
#include <sstream>
#include <iostream>

class gps_waypoints
{
public:
    gps_waypoints();

    // using file
    int countWaypointsInFile(std::string path_local);
    std::vector <std::pair<double, double>> getWaypoints(std::string path_local);

    // using pick points
    geometry_msgs::PointStamped latLongtoUTM(double latti_input, double longti_input);
    geometry_msgs::PointStamped UTMtoMapPoint(geometry_msgs::PointStamped UTM_input);
    bool TryUTMtoMapPoint(geometry_msgs::PointStamped UTM_input, geometry_msgs::PointStamped* map_output);
    void gps_waypointsCallback(const std_msgs::Float64MultiArray::ConstPtr& array);

private:
    ros::NodeHandle nh;
    std::vector<std::pair<double, double>> waypointVect;
    std::vector<std::pair<double,double>>::iterator iter; //init. iterator
    geometry_msgs::PointStamped UTM_point, map_point, UTM_next, map_next;
    int count = 0;
    double numWaypoints = 0;
    double latiGoal, longiGoal, latiNext, longiNext;
    std::string utm_zone;
    std::string path_local, path_abs;
    ros::Subscriber sub_misstion;  
    ros::Publisher Wp_GPS_pub;
};

// Counting the number of points from File
int gps_waypoints::countWaypointsInFile(std::string path_local)
{
    // Đường dẫn tuyệt đối
    path_abs = ros::package::getPath("akog_bringup") + path_local;
    std::ifstream fileCount(path_abs.c_str());

    if(fileCount.is_open())
    {
        double lati = 0;
        while(!fileCount.eof())
        {
            // nếu chưa ở cuối file
            fileCount >> lati;
            ++count;
        }
        count = count - 1;
        numWaypoints = count / 2;
        ROS_INFO("%.0f GPS waypoints were read", numWaypoints);
        fileCount.close();
    }
    else
    {
        std::cout << "Unable to open waypoint file" << std::endl;
        ROS_ERROR("Unable to open waypoint file");
    }
    return numWaypoints;
}

// Get the axist points from File
std::vector <std::pair<double, double>> gps_waypoints::getWaypoints(std::string path_local)
{
    double lati = 0, longi = 0;

    path_abs = ros::package::getPath("akog_bringup") + path_local;
    std::ifstream fileRead(path_abs.c_str());
    for(int i = 0; i < numWaypoints; i++)
    {
        fileRead >> lati;
        fileRead >> longi;
        waypointVect.push_back(std::make_pair(lati, longi));
    }
    fileRead.close();

    //Outputting vector
    ROS_INFO("The following GPS Waypoints have been set:");

    for(std::vector<std::pair<double, double>>::iterator iterDisp = waypointVect.begin(); iterDisp != waypointVect.end(); iterDisp++)
    {
        ROS_INFO("%.9g %.9g", iterDisp->first, iterDisp->second);
    }
    return waypointVect;
}

// Convert latti and longti to UTM axits
geometry_msgs::PointStamped gps_waypoints::latLongtoUTM(double latti_input, double longti_input)
{
    double utm_x = 0, utm_y = 0;
    geometry_msgs::PointStamped UTM_point_output;

    // chuyển đổi từ kinh độ vị độ sang tọa độ UTM
    RobotLocalization::NavsatConversions::LLtoUTM(latti_input, longti_input,utm_y, utm_x, utm_zone);
    UTM_point_output.header.frame_id = "utm";
    UTM_point_output.header.stamp = ros::Time(0);
    UTM_point_output.point.x = utm_x;
    UTM_point_output.point.y = utm_y;
    UTM_point_output.point.z = 0;
    return UTM_point_output;
}

// Chuyển đội tọa độ từ UTM sang tạo độ cục bộ "map" hoặc "odom"
geometry_msgs::PointStamped gps_waypoints::UTMtoMapPoint(geometry_msgs::PointStamped UTM_input)
{
    geometry_msgs::PointStamped map_point_output;
    bool notDone = true;
    tf::TransformListener listener;
    ros::Time time_now = ros::Time::now();
    while(notDone)
    {
        try
        {
            UTM_point.header.stamp = ros::Time::now();
            listener.waitForTransform("odom", "utm", time_now, ros::Duration(3.0));
            listener.transformPoint("odom", UTM_input, map_point_output);
            notDone = false;
        }
        catch (tf::TransformException& ex)
        {
            ROS_WARN("%s", ex.what());
            ros::Duration(0.01).sleep();
            //return;
        }
    }
    return map_point_output;
}

bool gps_waypoints::TryUTMtoMapPoint(geometry_msgs::PointStamped UTM_input, geometry_msgs::PointStamped* map_output)
{
    geometry_msgs::PointStamped* map_point_output;
    map_point_output = map_output;
    bool notDone = true;
    tf::TransformListener listener;
    ros::Time time_now = ros::Time::now();
    while(notDone)
    {
        try
        {
            UTM_point.header.stamp = ros::Time::now();
            listener.waitForTransform("odom", "utm", time_now, ros::Duration(3.0));
            listener.transformPoint("odom", UTM_input, *map_point_output);
            notDone = false;
        }
        catch (tf::TransformException& ex)
        {
            ROS_WARN("%s", ex.what());
            ros::Duration(0.01).sleep();
            return false;
        }
    }
    return true;
}

void gps_waypoints::gps_waypointsCallback(const std_msgs::Float64MultiArray::ConstPtr& array)
{
    std::vector<double> data;
    for(std::vector<double>::const_iterator it = array->data.begin(); it != array->data.end(); ++it)
    {
        data.push_back(*it);
    }

    waypointVect.clear();
    for(int j = 0; j < data.size() / 2; j++)
    {
        waypointVect.push_back (std::make_pair(data[2 * j], data[2 * j + 1]));
        ROS_INFO_STREAM ("Target Points: " << waypointVect[j].first  << " **** " << waypointVect[j].second);
        std::stringstream ss;
        ss << "Received Latitude - Longitude goal: " <<  waypointVect[j].first << " " << waypointVect[j].second;
    }

    // convert Gps points to Map points and add to pArr to Publish
    geometry_msgs::PoseArray pArr;
    for (int i = 0; i < waypointVect.size (); i++)
    {
        latiGoal = waypointVect.at(i).first;
        longiGoal = waypointVect.at(i).second;
        UTM_point = latLongtoUTM(latiGoal, longiGoal);

        if (TryUTMtoMapPoint(UTM_point, &map_point) == 0)
        {
            ROS_INFO ("convert UTM to MapPoint FALSE");
            return;
        }

        geometry_msgs::Pose pose;
        pose.position.x = map_point.point.x;
        pose.position.y = map_point.point.y;
        pArr.poses.push_back (pose);
    }

    Wp_GPS_pub.publish (pArr);
}

gps_waypoints::gps_waypoints()
{
    sub_misstion = nh.subscribe("/user/gps_mission",10,&gps_waypoints::gps_waypointsCallback,this);
    Wp_GPS_pub = nh.advertise <geometry_msgs::PoseArray>("/user/wp_mission", 10);
}

int main(int argc, char** argv)
{
    ros::init(argc, argv, "gps_mission");
    ROS_INFO("Initiated gps_mission node");

    gps_waypoints gpsmisstion;
    ros::spin();
    return 0;
}