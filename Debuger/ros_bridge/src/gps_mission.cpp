#include <ros/ros.h>
#include <ros/package.h>
#include <fstream>
#include <utility>
#include <vector>
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>
#include <robot_localization/navsat_conversions.h>
#include <geometry_msgs/PointStamped.h>
#include <std_msgs/Bool.h>
#include <std_msgs/Int32.h>
#include <std_msgs/String.h>
#include <std_msgs/Float64MultiArray.h>
#include <tf/transform_listener.h>
#include <math.h>
#include <list>
#include <thread>
#include <mutex>
#include <unistd.h>
#include <sstream>
#include <iostream>

typedef actionlib::SimpleActionClient <move_base_msgs::MoveBaseAction> MoveBaseClient; //create a type definition for a client called MoveBaseClient

class gpsMission
{
public:
    gpsMission();
    int countWaypointsInFile(std::string path_local);
    std::vector <std::pair<double, double>> getWaypoints(std::string path_local);
    geometry_msgs::PointStamped latLongtoUTM(double latti_input, double longti_input);
    geometry_msgs::PointStamped UTMtoMapPoint(geometry_msgs::PointStamped UTM_input);
    move_base_msgs::MoveBaseGoal buildGoal(geometry_msgs::PointStamped map_point, geometry_msgs::PointStamped map_next, bool last_point);
    bool TryUTMtoMapPoint(geometry_msgs::PointStamped UTM_input, geometry_msgs::PointStamped* map_output);
    void gpsMissionCallback(const std_msgs::Float64MultiArray::ConstPtr& array);
    void cmdCallback(const std_msgs::Int32::ConstPtr& msg);

    MoveBaseClient *acc; // Action Client for cancel
    bool isAccConnectted;
private:
    
    ros::NodeHandle nh;
    std::vector<std::pair<double, double>> waypointVect;
    std::vector<std::pair<double,double>>::iterator iter; //init. iterator
    geometry_msgs::PointStamped UTM_point, map_point, UTM_next, map_next;
    int count = 0, waypointCount = 0, wait_count = 0;
    double numWaypoints = 0;
    double latiGoal, longiGoal, latiNext, longiNext;
    std::string utm_zone;
    std::string path_local, path_abs;
    
    ros::Subscriber sub_misstion;
    ros::Subscriber sub_cmd;
    // Publisher để phát đi topic thông báo node đã kết thúc hay chưa.
    ros::Publisher pubWaypointNodeEnded;
    ros::Publisher pub_mission_rcv_fb;
    ros::Publisher pub_gps_mission_node_status;
    void threadAcMission();
    bool isThreadAcRunning;
    
    std::mutex missionLock;
};

// đếm số point từ file
int gpsMission::countWaypointsInFile(std::string path_local)
{
    // Đường dẫn tuyệt đối
    path_abs = ros::package::getPath("akog_bringup") + path_local;
    std::ifstream fileCount(path_abs.c_str());

    if(fileCount.is_open())
    {
        double lati = 0;
        while(!fileCount.eof())
        {
            // nếu chưa ở cuối file
            fileCount >> lati;
            ++count;
        }
        count = count - 1;
        numWaypoints = count / 2;
        ROS_INFO("%.0f GPS waypoints were read", numWaypoints);
        fileCount.close();
    }
    else
    {
        std::cout << "Unable to open waypoint file" << std::endl;
        ROS_ERROR("Unable to open waypoint file");
    }
    return numWaypoints;
}

// get point từ file
std::vector <std::pair<double, double>> gpsMission::getWaypoints(std::string path_local)
{
    double lati = 0, longi = 0;

    path_abs = ros::package::getPath("akog_bringup") + path_local;
    std::ifstream fileRead(path_abs.c_str());
    for(int i = 0; i < numWaypoints; i++)
    {
        fileRead >> lati;
        fileRead >> longi;
        waypointVect.push_back(std::make_pair(lati, longi));
    }
    fileRead.close();

    //Outputting vector
    ROS_INFO("The following GPS Waypoints have been set:");

    for(std::vector<std::pair<double, double>>::iterator iterDisp = waypointVect.begin(); iterDisp != waypointVect.end(); iterDisp++)
    {
        ROS_INFO("%.9g %.9g", iterDisp->first, iterDisp->second);
    }
    return waypointVect;
}

// chuyển đổi từ kinh độ vị độ sang tọa độ UTM
geometry_msgs::PointStamped gpsMission::latLongtoUTM(double latti_input, double longti_input)
{
    double utm_x = 0, utm_y = 0;
    geometry_msgs::PointStamped UTM_point_output;

    // chuyển đổi từ kinh độ vị độ sang tọa độ UTM
    RobotLocalization::NavsatConversions::LLtoUTM(latti_input, longti_input,utm_y, utm_x, utm_zone);
    UTM_point_output.header.frame_id = "utm";
    UTM_point_output.header.stamp = ros::Time(0);
    UTM_point_output.point.x = utm_x;
    UTM_point_output.point.y = utm_y;
    UTM_point_output.point.z = 0;
    return UTM_point_output;
}

// Chuyển đội tọa độ từ UTM sang tạo độ cục bộ "map" hoặc "odom"
geometry_msgs::PointStamped gpsMission::UTMtoMapPoint(geometry_msgs::PointStamped UTM_input)
{
    geometry_msgs::PointStamped map_point_output;
    bool notDone = true;
    tf::TransformListener listener;
    ros::Time time_now = ros::Time::now();
    while(notDone)
    {
        try
        {
            UTM_point.header.stamp = ros::Time::now();
            listener.waitForTransform("odom", "utm", time_now, ros::Duration(3.0));
            listener.transformPoint("odom", UTM_input, map_point_output);
            notDone = false;
        }
        catch (tf::TransformException& ex)
        {
            ROS_WARN("%s", ex.what());
            ros::Duration(0.01).sleep();
            //return;
        }
    }
    return map_point_output;
}

bool gpsMission::TryUTMtoMapPoint(geometry_msgs::PointStamped UTM_input, geometry_msgs::PointStamped* map_output)
{
    geometry_msgs::PointStamped* map_point_output;
    map_point_output = map_output;
    bool notDone = true;
    tf::TransformListener listener;
    ros::Time time_now = ros::Time::now();
    while(notDone)
    {
        try
        {
            UTM_point.header.stamp = ros::Time::now();
            listener.waitForTransform("odom", "utm", time_now, ros::Duration(3.0));
            listener.transformPoint("odom", UTM_input, *map_point_output);
            notDone = false;
        }
        catch (tf::TransformException& ex)
        {
            ROS_WARN("%s", ex.what());
            ros::Duration(0.01).sleep();
            return false;
        }
    }
    return true;
}

// Tạo MoveBaseGoal
move_base_msgs::MoveBaseGoal gpsMission::buildGoal(geometry_msgs::PointStamped map_point, geometry_msgs::PointStamped map_next, bool last_point)
{
    move_base_msgs::MoveBaseGoal goal;

    // Lựa chọn frame chứa goal
    goal.target_pose.header.frame_id = "odom";
    goal.target_pose.header.stamp = ros::Time::now();
    // Tạo độ x,y
    goal.target_pose.pose.position.x = map_point.point.x; //specify x goal
    goal.target_pose.pose.position.y = map_point.point.y; //specify y goal

    // Đã là điểm cuối hay chưa
    if(last_point == false)
    {
        tf::Matrix3x3 rot_euler;
        tf::Quaternion rot_quat;

        // Calculate quaternion
        float x_curr = map_point.point.x, y_curr = map_point.point.y;
        float x_next = map_next.point.x, y_next = map_next.point.y; 
        float delta_x = x_next - x_curr, delta_y = y_next - y_curr;  
        float yaw_curr = 0, pitch_curr = 0, roll_curr = 0;
        yaw_curr = atan2(delta_y, delta_x);

        // Quaternions
        rot_euler.setEulerYPR(yaw_curr, pitch_curr, roll_curr);
        rot_euler.getRotation(rot_quat);

        goal.target_pose.pose.orientation.x = rot_quat.getX();
        goal.target_pose.pose.orientation.y = rot_quat.getY();
        goal.target_pose.pose.orientation.z = rot_quat.getZ();
        goal.target_pose.pose.orientation.w = rot_quat.getW();
    }
    else
    {
        goal.target_pose.pose.orientation.w = 1.0;
    }

    return goal;
}

void gpsMission::cmdCallback(const std_msgs::Int32::ConstPtr& msg)
{
    
    int cmd = msg->data;
    switch(cmd)
    {
        case 1:
            if(isAccConnectted)
            {
                ROS_INFO_STREAM("Cancel All Goals");
                acc->cancelAllGoals();
            }
            else
            {
                ROS_INFO_STREAM("Action Client is not connected to move_base");
            }
            break;
    }
}
void gpsMission::gpsMissionCallback(const std_msgs::Float64MultiArray::ConstPtr& array)
{
    if(!isThreadAcRunning)
    {
        int i = 0;
	    //print all the remaining numbers
        std::vector<double> data;
	    for(std::vector<double>::const_iterator it = array->data.begin(); it != array->data.end(); ++it)
	    {
            data.push_back(*it);
	    	//Arr[i] = *it;
	    	i++;
	    }
        waypointVect.clear();
        for(int j=0; j<data.size() / 2; j++)
        {
            waypointVect.push_back(std::make_pair(data[2*j],data[2*j+1]));
            ROS_INFO_STREAM("Target Points: " << waypointVect[j].first  << " **** " << waypointVect[j].second);
            std::stringstream ss;
            ss << "Received Latitude - Longitude goal: " <<  waypointVect[j].first << " " << waypointVect[j].second;
            std_msgs::String msgStatus;
            msgStatus.data = ss.str();
            ss.clear();
            pub_gps_mission_node_status.publish(msgStatus);
        }
        std::thread th(&gpsMission::threadAcMission, this);
        th.detach();
    }
    else
    {
        ROS_INFO_STREAM("Other Mission is running!");
        std_msgs::String msgStatus;
        msgStatus.data = "Other Mission is running!";
        pub_gps_mission_node_status.publish(msgStatus);
    }
	return;
}

void gpsMission::threadAcMission()
{
    this->isThreadAcRunning = true;
    MoveBaseClient ac("/move_base", true);
    std_msgs::String msgStatus;

    bool checkserver = true;
    while(!ac.waitForServer(ros::Duration(5.0)))
    {
        wait_count++;
        if(wait_count > 3)
        {
            msgStatus.data = "move_base action server did not come up.";
            ROS_ERROR("move_base action server did not come up.");
            pub_gps_mission_node_status.publish(msgStatus);
            //std_msgs::Bool node_ended;
            //node_ended.data = true;
            //pubWaypointNodeEnded.publish(node_ended);
            checkserver = false;
            break;
        }
        msgStatus.data = "Waiting for the move_base action server to come up";
        ROS_INFO("Waiting for the move_base action server to come up");
        pub_gps_mission_node_status.publish(msgStatus);
    }
    
    if(checkserver)
    {
        for(iter = waypointVect.begin(); iter < waypointVect.end(); iter++)
        {
            latiGoal = iter->first;
            longiGoal = iter->second;
            bool final_point = false;

            if(iter < (waypointVect.end() - 1))
            {
                iter++;
                latiNext = iter->first;
                longiNext = iter->second;
                iter--;
            }
            else
            {
                latiNext = iter->first;
                longiNext = iter->second;
                final_point = true;
            }

            ROS_INFO("Received Latitude goal:%.8f", latiGoal);
            ROS_INFO("Received Longitude goal:%.8f", longiGoal);
            std::stringstream ss;
            ss << "Received Latitude - Longitude goal: " <<  latiGoal << " " << longiGoal;
            msgStatus.data = ss.str();
            ss.clear();
            pub_gps_mission_node_status.publish(msgStatus);

            UTM_point = latLongtoUTM(latiGoal, longiGoal);
            UTM_next = latLongtoUTM(latiNext, longiNext);
            //Transform UTM to map point in odom frame
            //map_point = UTMtoMapPoint(UTM_point);
            //map_next = UTMtoMapPoint(UTM_next);
            bool check_utmtomap = false;
            check_utmtomap = TryUTMtoMapPoint(UTM_point,&map_point) && TryUTMtoMapPoint(UTM_next,&map_next);
            if(check_utmtomap)
            {
                move_base_msgs::MoveBaseGoal goal = buildGoal(map_point, map_next, final_point); 

                ROS_INFO("Sending goal");
                msgStatus.data = "Sending goal";
                pub_gps_mission_node_status.publish(msgStatus);
                ac.sendGoal(goal); 
                //Wait for result
                ac.waitForResult();
                //ac.cancelAllGoals ();
                if(ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
                {
                    ROS_INFO("RB has reached its goal!");
                    msgStatus.data = "RB has reached its goal!";
                    pub_gps_mission_node_status.publish(msgStatus);
                }
                else
                {
                    ROS_ERROR("RB was unable to reach its goal. GPS Waypoint unreachable. (Next Goal)");
                    msgStatus.data = "RB was unable to reach its goal. GPS Waypoint unreachable. (Next Goal)";
                    pub_gps_mission_node_status.publish(msgStatus);

                    // break;
                    
                    //std_msgs::Bool node_ended;
                    //node_ended.data = true;
                    //pubWaypointNodeEnded.publish(node_ended);
                    //ros::shutdown();
                }
            }
            else
            {
                msgStatus.data = "Tranform failed.";
                pub_gps_mission_node_status.publish(msgStatus);
            }
        }
    }
    msgStatus.data = "End of Mission";
    pub_gps_mission_node_status.publish(msgStatus);
    this->isThreadAcRunning = false;
}


gpsMission::gpsMission()
{
    acc = new MoveBaseClient("/move_base",true);
    sub_misstion = nh.subscribe("/gps_mission",10,&gpsMission::gpsMissionCallback,this);
    sub_cmd = nh.subscribe("/cmd_mission",10,&gpsMission::cmdCallback,this);
    // Publisher để phát đi topic thông báo node đã kết thúc hay chưa.
    pubWaypointNodeEnded = nh.advertise<std_msgs::Bool>("/waypoint_status", 10);
    pub_mission_rcv_fb = nh.advertise<std_msgs::Bool>("/mission_rcv_fb",10);
    pub_gps_mission_node_status = nh.advertise<std_msgs::String>("/gps_mission_node_status",100);
    isThreadAcRunning = false;
    isAccConnectted = false;
}


int main(int argc, char** argv)
{
    ros::init(argc, argv, "gps_mission");
    ros::NodeHandle n;
    ROS_INFO("Initiated gps_mission node");
    
    gpsMission gpsmisstion;
    int wait_count = 0;
    bool check_move_base = true;
    while(!gpsmisstion.acc->waitForServer(ros::Duration(5.0)))
    {
        wait_count++;
        if(wait_count > 3)
        {
            ROS_ERROR("move_base action server did not come up.");
            check_move_base = false;
            break;
        }
        ROS_INFO("Waiting for the move_base action server to come up");
    }
    
    ROS_INFO ("Connected");

    if(check_move_base)
    {
        gpsmisstion.isAccConnectted = true;
    }
    ros::spin();
    return 0;
}