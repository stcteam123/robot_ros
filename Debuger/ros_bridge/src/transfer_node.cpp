#include "ros_bridge/transfer_node.h"

// using namespace bridge;

int main(int argc, char *argv[])
{
    /* code for main function */
    ros::init(argc, argv, "bridge_transfer");
    
    bridge::transfer    tf_;

    tf_.tf_run ();

    ros::spin ();

    return 0;
}