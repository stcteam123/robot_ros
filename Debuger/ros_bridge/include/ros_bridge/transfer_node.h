#ifndef TRANSFER_NODE_H_
#define TRANSFER_NODE_H_

#include <ros/ros.h>
#include <std_msgs/Int8MultiArray.h>
#include <std_msgs/Int8.h>
#include <nav_msgs/OccupancyGrid.h>
#include "compress.h"
#include "cryption.h"

#include <iostream>
#include <vector>
#include <math.h>

using namespace std;

// typedef void (*func)(void);

// using bFunc = vector<func>; 

namespace bridge
{
    #define ROBOT_MAX 10

    /*

    example command: typeCommand(set) + command(robot,mission...) + struct value
                     typeCommand(get) + command

    */

    /* TYPE COMMAND */
    typedef enum cmdType
    {
        GET = 0,
        SET
    } cmdType_e;


    /* COMMAND */
    typedef enum cmd
    {
        ROBOTSTATUS = 0,
        MAPSTATUS,
        MISSTIONSTATUS,
    } cmd_e;

    /* VALUE COMMAND */

    /* robot status about running, speed... */
    typedef struct cmdValueRobotMoveStatus
    {
        bool stopped = true;
        int speed = 0;
    } cmdValueRobotMoveStatus_t;


    /* Map using */
    enum mapType
    {
        FROMFILE = 0,
        AUTO,
        MANUAL
    };
    struct cmdValueMap
    {
        mapType type = FROMFILE;
    };


    /* Mission status */
    typedef enum missionStatusType
    {
        RUNNING = 0,
        DONE,
        FAILED
    } missionStatusType_e;
    typedef struct cmdValueMissionStatus
    {
        missionStatusType type = DONE;
    } cmdValueMissionStatus_t;


    /* on hit robot's handler */
    enum hitType
    {
        SPEEDUP = 0,
        GOCOVER
    };
    struct cmdValueonHit
    {
        hitType type = SPEEDUP;
    };


    class transfer
    {
        public:
            typedef void (transfer::*func)(void);

            transfer();

            ~transfer();

            void tf_run ();
        
        private:
            bool                        status, old_status;
            vector<func>                bExe;
            cmdValueMissionStatus_t     cmd_mission;
            cmdValueRobotMoveStatus_t   cmd_robotStatus;

            ros::NodeHandle             nh;
            ros::Publisher              tf_pub, data_pub;
            ros::Subscriber             user_sub, grip_gmapping_sub, grip_move_base_sub;
            ros::Subscriber             mission_sub;
            ros::Timer                  timer;

            void user_cb                (const std_msgs::Int8MultiArray::ConstPtr& msg);
            void grip_gmapping_cb       (const nav_msgs::OccupancyGrid::ConstPtr& msg);
            void grip_move_base_cb      (const nav_msgs::OccupancyGrid::ConstPtr& msg);
            void mission_cb             (const std_msgs::Int8::ConstPtr& msg);


            void spin_status            (const ros::TimerEvent& e);

            void assign_funcHandler     ();
            void func_1                 ();

    };

    transfer::transfer() : status (false), old_status (false)
    {
        bExe.clear ();
        assign_funcHandler ();
    }

    transfer::~transfer() 
    {
        bExe.clear();
    }

    void transfer::tf_run ()
    {
        double freq_ = 10;

        data_pub            = nh.advertise<std_msgs::Int8MultiArray>("/robot/cmd", 10);

        tf_pub              = nh.advertise<std_msgs::Int8MultiArray>("/robot/map_compress", 10);

        user_sub            = nh.subscribe("/user/cmd", 10, &transfer::user_cb, this);

        grip_gmapping_sub   = nh.subscribe("/map", 10, &transfer::grip_gmapping_cb, this);

        grip_move_base_sub  = nh.subscribe("/move_base/global_costmap/costmap", 10,      \
                                                    &transfer::grip_move_base_cb, this);

        timer               = nh.createTimer               \
                                (ros::Duration (1.0/std::max(freq_, 1.0)), &transfer::spin_status, this);
    }

    void transfer::user_cb (const std_msgs::Int8MultiArray::ConstPtr& msg)
    {
        ROS_INFO ("(transfer_node) user cb");

        cmdType_e   type    = (cmdType_e)msg->data[0];
        cmd_e       cmd     = (cmd_e)msg->data[1];

        if (type == cmdType_e::SET)
        {
            switch (cmd)
            {
                case cmd_e::ROBOTSTATUS:
                    ROS_INFO ("SET: ROBOTSTATUS");

                    break;

                case cmd_e::MAPSTATUS:
                    ROS_INFO ("SET: MAPSTATUS");
                    break;

                case cmd_e::MISSTIONSTATUS:
                    ROS_INFO ("SET: MISSTIONSTATUS");

                    break;

                // default:
                //     ROS_INFO ("DEFAULT SET");
            }
        }
        else if (type == cmdType_e::GET)
        {
            uint16_t lenStruct = 0;
            std_msgs::Int8MultiArray msg;

            switch (cmd)
            {
                case cmd_e::ROBOTSTATUS:
                    ROS_INFO ("GET: ROBOTSTATUS");

                    lenStruct = sizeof (cmd_robotStatus);
                    msg.data.push_back (cmdType_e::SET);
                    msg.data.push_back (cmd_e::ROBOTSTATUS);
                    msg.data.resize (lenStruct + 2);
                    cmd_robotStatus.speed = 150;
                    cmd_robotStatus.stopped = true;
                    std::copy ((uint8_t*)&cmd_robotStatus, (uint8_t*)&cmd_robotStatus + lenStruct, msg.data.begin() + 2);                    

                    data_pub.publish (msg);
                    break;

                case cmd_e::MAPSTATUS:
                    ROS_INFO ("GET: MAPSTATUS");
                    break;

                case cmd_e::MISSTIONSTATUS:
                    ROS_INFO ("GET: MISSTIONSTATUS");
                    lenStruct = sizeof (cmd_mission);
                    msg.data.push_back (cmdType_e::SET);
                    msg.data.push_back (cmd_e::MISSTIONSTATUS);
                    msg.data.resize (lenStruct + 2);
                    cmd_mission.type = missionStatusType_e::DONE;
                    std::copy ((uint8_t*)&cmd_mission, (uint8_t*)&cmd_mission + lenStruct, msg.data.begin() + 2);                    

                    data_pub.publish (msg);
                    break;

                // default:
                //     ROS_INFO ("DEFAULT GET");
            }
        }
        else
        {
            ROS_INFO ("WRONG CMD FROM USER: NOT GET OR SET");
        }
        
    }

    void transfer::grip_gmapping_cb (const nav_msgs::OccupancyGrid::ConstPtr& msg)
    {
        static int8_t skip = 0;
        if (skip >= 2)
        {
            buffer compressed(msg->data.size());

            compress_map_gmapping(msg->data, compressed);

            ROS_INFO ("(transfer_node) gmapping cb");

            skip = 0;
        }

        skip++;
    }

    void transfer::grip_move_base_cb (const nav_msgs::OccupancyGrid::ConstPtr& msg)
    {
        static int8_t skip = 0;
        if (skip >= 2)
        {
            double secs =ros::Time::now().toSec();

            ROS_INFO ("------------------------------------------");

            buffer compressed(msg->data.size ());
            buffer decompressd;

            compress_map_move_base(msg->data, compressed);

#if 1
            std_msgs::Int8MultiArray tf_send;
            tf_send.data.resize (compressed.size());
            std::copy (compressed.begin(), compressed.end(), tf_send.data.begin());

            tf_pub.publish (tf_send);
#endif

#if 0
            decompress_map_move_base (compressed, decompressd);
#endif 

            ROS_INFO ("(transfer_node) move_base cb");

            skip = 0;

            double secs_end =ros::Time::now().toSec();

            ROS_INFO ("diff time cb = [%f]", secs_end - secs);
        }

        skip++;
    }

    void transfer::mission_cb (const std_msgs::Int8::ConstPtr& msg)
    {
        cmd_mission.type = (missionStatusType_e)msg->data;
    }

    void transfer::spin_status (const ros::TimerEvent& e)
    {
        // ROS_INFO ("SPIN TIMER");

        //todo:  check connect status with User App
        if (old_status != status)
        {
            if (status == false && old_status == false)
            {
                ROS_INFO ("[status]: NO CONNECTED");
            }
            else if (status == true)
            {
                ROS_INFO ("[status]: CONNECTED");
            }
            else 
            {
                ROS_INFO ("[status]: NO CONNECTED - NEED RECONNECT");
            }

            old_status = status;
        }
#if 0
        transfer::func f = bExe.at(0);
        (this->*f) ();
#endif
    }

    void transfer::assign_funcHandler ()
    {
        bExe.push_back (&transfer::func_1);
    }

    void transfer::func_1 ()
    {
        // ROS_INFO ("func_1 run");
    }
}

#endif // TRANSFER_NODE_H_