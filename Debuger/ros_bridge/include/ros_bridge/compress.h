#ifndef COMPRESS_H_
#define COMPRESS_H_

#include <iostream>
#include <vector>
#include <lz4.h>

using namespace std;
using buffer = vector<int8_t>;

#define     USE_RLE     (1)
#define     WIDTH       1000
#define     HEIGHT      1000

namespace bridge
{
    /* code */
    typedef enum 
    {
        GMAPPING = 0,
        MOVE_BASE,
        DRIVER_INFO,

    } e_type;

    void lz4_compress(const buffer& in, buffer& out)
    {
        auto rv = LZ4_compress_default((char*)in.data(), (char*)out.data(), in.size(), out.size());
        if(rv < 1) 
            cerr << "Something went wrong!" << endl;
        else 
            out.resize(rv);
    }

    void lz4_decompress(const buffer& in, buffer& out)
    {
        auto rv = LZ4_decompress_safe((char*)in.data(), (char*)out.data(), in.size(), out.size());
        if(rv < 1) 
            cerr << "Something went wrong!" << endl;
        else 
            out.resize(rv);
    }

    void RLE_push_int32 (int32_t data, buffer& out)
    {
        int8_t tmp = (data >> 16) & 0xff;
        out.push_back(tmp);

        tmp = (data >> 8) & 0xff;
        out.push_back(tmp);

        tmp = data & 0xff;
        out.push_back(tmp);
    }

    int32_t RLE_int8_to_int32 (const buffer& in, unsigned int index)
    {
        int32_t out = 0;

        for (unsigned int i = 0; i < 3; i++)
        {
            out += (uint8_t)in[index + i] << (8 * (2 - i));
        }

        return out;
    }
    
    void RLE_compress (const buffer& in, buffer& out)
    {
        out.clear ();
        unsigned int len = in.size ();

        RLE_push_int32 (WIDTH, out);
        RLE_push_int32 (HEIGHT, out);

        vector<int8_t>::const_iterator ptr = in.begin();
        int8_t tmp = 0;
        out.push_back (tmp);
	    ptr++;

        unsigned int count = 1;
        for (register unsigned int i = 1; i < len; i++)
        {
            if (tmp != *ptr)
            {
                tmp = *ptr;

                RLE_push_int32 (count, out);
                out.push_back(tmp);
                count = 1;
            }
            else
            {
                count++;
            }
            ptr++;
        }

        RLE_push_int32 (count, out);

        unsigned int total_size = out.size ();
        RLE_push_int32 (total_size + 3, out);
    }

    void RLE_decompress (const buffer& in, buffer& out)
    {
        out.clear ();

        unsigned int len = in.size ();

        if (len != RLE_int8_to_int32(in, len - 3))
        {
            ROS_INFO ("ERROR len");
            return;
        }

        for (register unsigned int i = 6; i < len - 3; i = i + 4)
        {
            int8_t tmp = in[i];
            unsigned int repeat = RLE_int8_to_int32 (in, i + 1);

            // ROS_INFO ("[%d] --- repeat = [%d]", i, repeat);

            for (register unsigned int j = 0; j < repeat; j++)
            {
                out.push_back(tmp);
            }
        }

        int len_decode = out.size ();
    }

    bool compress_map_gmapping (const buffer& in, buffer& out)
    {
        buffer tmp;
        RLE_compress (in, tmp);

        cout << "RLE compress, bytes in: " << in.size() << ", bytes out: " << tmp.size() << endl;

        lz4_compress (tmp, out);

        cout << "LZ4 compress, bytes in: " << tmp.size() << ", bytes out: " << out.size() << endl;

        out.insert (out.begin(), int8_t(GMAPPING));

        std::cout << "size out gmapping = " << out.size() << std::endl;

        return true;
    }

    bool compress_map_move_base (const buffer& in, buffer& out)
    {
        buffer tmp;
        RLE_compress (in, tmp);

        cout << "RLE compress, bytes in: " << in.size() << ", bytes out: " << tmp.size() << endl;

        lz4_compress (tmp, out);

        cout << "LZ4 compress, bytes in: " << tmp.size() << ", bytes out: " << out.size() << endl;

        out.insert (out.begin(), int8_t(MOVE_BASE));

        std::cout << "size out move_base = " << out.size() << std::endl;

        return true;
    }

    bool decompress_map_move_base (buffer& in, buffer& out)
    {
        buffer decompress (HEIGHT * WIDTH);

        ROS_INFO ("type = [%d]", in[0]);
        in.erase (in.begin());

        lz4_decompress (in, decompress);

        cout << "LZ4 decompress, bytes in: " << in.size() << ", bytes out: " << decompress.size() << endl;

        RLE_decompress (decompress, out);

        cout << "RLE decompress, bytes in: " << decompress.size() << ", bytes out: " << out.size() << endl;

        return true;
    }

    bool compress_driver_info (const buffer& in, buffer& out)
    {
        out.insert (out.begin(), int8_t(DRIVER_INFO));

        return true;
    }
}


#endif // COMPRESS_H_