#include <ros/ros.h>
#include <gazebo_msgs/ModelStates.h>
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/Imu.h>
#include <geometry_msgs/Twist.h>
#include <tf/transform_listener.h>
#include <tf/transform_broadcaster.h>
#include <geometry_msgs/Quaternion.h>
#include <tf/tf.h>
#include <iostream>
#include <sstream>

ros::Publisher                  odom_pub;
ros::Publisher                  imu_pub;
ros::Subscriber                 odom_sub;
nav_msgs::Odometry              odom;
sensor_msgs::Imu                imu;

ros::Time                       start;

std::string                     odom_frame_id_, imu_frame_id_;
std::string                     odom_child_frame_id_;

int cnt = 0;

class odom_gazebo
{
    public:
        odom_gazebo ();

        ~odom_gazebo ();

        void run ();

    private:

        /* variable */

        tf::TransformBroadcaster        odom_broadcaster;

        ros::Timer                      timer;

        ros::Subscriber                 odom_gazebo_sub, odom_husky_sub; 

        ros::Publisher                  odom_pub;

        ros::NodeHandle                 nh;
        ros::NodeHandle                 nh_private;

        nav_msgs::Odometry              odom;

        geometry_msgs::Twist            vel_husky_;

        double                          freq;

        bool                            isActive;

        void gModelStates_callback(const gazebo_msgs::ModelStates::ConstPtr& msg); 

        void odomHusky_Cb (const nav_msgs::Odometry::ConstPtr& msg);

        void cal_tf_odom ();

        void spin_timer (const ros::TimerEvent& e);

        void odom_publish ();

        double quaternion_2_yaw (geometry_msgs::Quaternion orien);
};

odom_gazebo::odom_gazebo () : nh_private ("~")
{
    nh_private.param ("freq",                  freq, 20.0);
    nh_private.param ("odom_frame_id_",        odom_frame_id_, std::string("odom_ekf"));
    nh_private.param ("odom_child_frame_id_",  odom_child_frame_id_, std::string("base_link"));

    odom.pose.pose.position.x = 0;
    odom.pose.pose.position.y = 0;
    odom.pose.pose.position.z = 0;
    odom.pose.pose.orientation = tf::createQuaternionMsgFromYaw(0.0);
}

odom_gazebo::~odom_gazebo ()
{

}

void odom_gazebo::run ()
{
    odom_pub        = nh.advertise <nav_msgs::Odometry> ("odom_ekf", 10);

    odom_gazebo_sub = nh.subscribe
                                ("gazebo/model_states", 1, &odom_gazebo::gModelStates_callback, this);
                                

    odom_husky_sub = nh.subscribe ("/husky_velocity_controller/odom", 1, &odom_gazebo::odomHusky_Cb, this);

    timer           = nh.createTimer               \
                                (ros::Duration (1.0/std::max(freq, 1.0)), &odom_gazebo::spin_timer, this);
}


void odom_gazebo::odomHusky_Cb (const nav_msgs::Odometry::ConstPtr& msg)
{
    vel_husky_ = msg->twist.twist;
}

double odom_gazebo::quaternion_2_yaw (geometry_msgs::Quaternion orien)
{
    double roll, pitch, yaw;
    tf::Quaternion q ( orien.x, orien.y, orien.z, orien.w);
    tf::Matrix3x3 m(q);
    m.getRPY (roll, pitch, yaw);

    return yaw;
}

void odom_gazebo::gModelStates_callback(const gazebo_msgs::ModelStates::ConstPtr& msg)
{
    static int cnt = 0;
        
    cnt++;
    if (cnt < 20)
        return;
    cnt = 0;

    double yaw = quaternion_2_yaw (msg->pose[1].orientation);
    geometry_msgs::Quaternion odom_quat;
    odom_quat = tf::createQuaternionMsgFromYaw(yaw);
    
    /* header odom */
    odom.header.stamp = ros::Time::now ();
    odom.header.frame_id = odom_frame_id_;
    odom.child_frame_id = odom_child_frame_id_;

    odom.pose.pose = msg->pose[1];
    odom.pose.pose.position.x = msg->pose[1].position.x;
    odom.pose.pose.position.y = msg->pose[1].position.y;
    odom.pose.pose.position.z = 0.0;
    odom.pose.pose.orientation = odom_quat;

    // odom.twist.twist = msg->twist[1];
    odom.twist.twist = vel_husky_;
}

void odom_gazebo::cal_tf_odom ()
{
    ros::Time current_time = ros::Time::now ();

    double yaw = quaternion_2_yaw (odom.pose.pose.orientation);
    geometry_msgs::Quaternion odom_quat;
    odom_quat = tf::createQuaternionMsgFromYaw(yaw);

    geometry_msgs::TransformStamped odom_trans;
    odom_trans.header.stamp = current_time;
    odom_trans.header.frame_id = odom_frame_id_;
    odom_trans.child_frame_id = odom_child_frame_id_;

    odom_trans.transform.translation.x = odom.pose.pose.position.x;
    odom_trans.transform.translation.y = odom.pose.pose.position.y;
    odom_trans.transform.translation.z = 0.0;
    odom_trans.transform.rotation = odom_quat;

    //send the transform
    odom_broadcaster.sendTransform(odom_trans);
}

void odom_gazebo::spin_timer (const ros::TimerEvent& e)
{
    // ROS_INFO ("spin timer");

    cal_tf_odom ();

    odom_publish ();
}

void odom_gazebo::odom_publish ()
{
    odom_pub.publish (odom);
}

int main(int argc, char **argv)
{
	ros::init(argc, argv, "odom_gazebo");

    odom_gazebo gOdom;

    gOdom.run ();

    ros::spin();
	return 0;
}


