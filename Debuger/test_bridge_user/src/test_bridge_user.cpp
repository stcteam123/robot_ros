#include "test_bridge_user/def.h"

#include <ros/ros.h>
#include <std_msgs/Int8MultiArray.h>

#include <iostream>
#include <boost/thread.hpp>
#include <thread>
#include <mutex>
#include <vector>
#include <math.h>

using namespace bridge_test;

class testBridge
{
    public:
        testBridge ();

        ~testBridge ();

        ros::NodeHandle                 nh;
            
        ros::Publisher                  test_package_pub;
        ros::Subscriber                 test_package_sub;

        ros::Timer                      timer;

        void Test_Package_Cb (const std_msgs::Int8MultiArray::ConstPtr& msg);

        void Send_data_to_Robot ();

        void spin_timer (const ros::TimerEvent& e);

        void Thread_handle ();
};

testBridge::testBridge ()
{
    test_package_pub        = nh.advertise<std_msgs::Int8MultiArray>("robot/recv_package", 10);
    test_package_sub        = nh.subscribe("robot/send_package", 1, &testBridge::Test_Package_Cb, this);

    // timer                   = nh.createTimer ( ros::Duration (1.0), &testBridge::spin_timer, this);
}

testBridge::~testBridge () {}

void testBridge::Thread_handle ()
{
    char str;
    while (true)
    {
        std::cin >> str;
        // get Robot infor
        std_msgs::Int8MultiArray msg;
        msg.data.clear ();

        if (str == '1') // get robot infor
        {
            msg.data.push_back (command_type_e::GET_CMD);
            msg.data.push_back (object_type_e::ROBOT_INFOR);
            
            test_package_pub.publish (msg);
        }
        else if (str == '2') // get map infor
        {
            // do nothing
            msg.data.push_back (command_type_e::GET_CMD);
            msg.data.push_back (object_type_e::MAP_INFOR);
            
            test_package_pub.publish (msg);
        }
        else if (str == '3') // set PATH_MISSION
        {
            // PATH_MISSION
            Obj_path_mission_t path;  
            Point p1 (-20, -19);    
            Point p2 (15, -20);
            Point p3 (15, 12);

            path.len = 3;
            path.path[0] = p1;
            path.path[1] = p2;
            path.path[2] = p3;

            msg.data.push_back (command_type_e::SET_CMD);
            msg.data.push_back (object_type_e::PATH_MISSION);

            uint32_t len = sizeof (path);
            msg.data.resize (len + PAYLOAD_OFFSET);
            std::copy ( (uint8_t*)&path, 
                        (uint8_t*)&path + len, 
                        msg.data.begin() + PAYLOAD_OFFSET);

            test_package_pub.publish (msg);
        }
        else if (str == '4') // set CONTROL_SIGNAL
        {
            // CONTROL_SIGNAL
            Obj_signal_t signal;
            signal.signal_chosen = signal_e::FLAG_GET_MAP;
            signal.control = 1;
            signal.emergency = 0;
            signal.send_map = 1;
            signal.flag_get_map = 1;

            msg.data.push_back (command_type_e::SET_CMD);
            msg.data.push_back (object_type_e::CONTROL_SIGNAL);

            uint32_t len = sizeof (signal);
            msg.data.resize (len + PAYLOAD_OFFSET);
            std::copy ( (uint8_t*)&signal, 
                        (uint8_t*)&signal + len, 
                        msg.data.begin() + PAYLOAD_OFFSET);

            test_package_pub.publish (msg);
        }
        else if (str == '5') // set CONTROL_SIGNAL
        {
            // CONTROL_SIGNAL
            Obj_signal_t signal;
            signal.signal_chosen = signal_e::FLAG_GET_MAP;
            signal.control = 1;
            signal.emergency = 0;
            signal.send_map = 1;
            signal.flag_get_map = 0;

            msg.data.push_back (command_type_e::SET_CMD);
            msg.data.push_back (object_type_e::CONTROL_SIGNAL);

            uint32_t len = sizeof (signal);
            msg.data.resize (len + PAYLOAD_OFFSET);
            std::copy ( (uint8_t*)&signal, 
                        (uint8_t*)&signal + len, 
                        msg.data.begin() + PAYLOAD_OFFSET);

            test_package_pub.publish (msg);
        }
        else if (str == '6') // reload map
        {
            // CONTROL_SIGNAL
            Obj_signal_t signal;
            signal.signal_chosen = signal_e::SEND_BIG_MAP;
            signal.control = 1;
            signal.emergency = 0;
            signal.send_map = 1;
            signal.flag_get_map = 0;

            msg.data.push_back (command_type_e::SET_CMD);
            msg.data.push_back (object_type_e::CONTROL_SIGNAL);

            uint32_t len = sizeof (signal);
            msg.data.resize (len + PAYLOAD_OFFSET);
            std::copy ( (uint8_t*)&signal, 
                        (uint8_t*)&signal + len, 
                        msg.data.begin() + PAYLOAD_OFFSET);

            test_package_pub.publish (msg);
        }
        else
        {
            printf ("Return\n");
            return;
        }
    }
}

void testBridge::spin_timer (const ros::TimerEvent& e)
{
    
}

void testBridge::Test_Package_Cb (const std_msgs::Int8MultiArray::ConstPtr& msg)
{
    printf ("(test_bridge_user) Test_Package_Cb\n");

    command_type_e   cmd_type   = (command_type_e) msg->data[COMMAND_OFFSET];
    object_type_e    Obj_type   = (object_type_e) msg->data[COMMAND_OFFSET + 1];
    uint8_t          lenData    = msg->data.size () - PAYLOAD_OFFSET;

    printf ("cmd_type : %d \t Obj_type: %d \n", cmd_type, Obj_type);
    printf ("OBJ size: [%d]\n", (uint16_t)msg->data.size ());

    if (Obj_type == object_type_e::ROBOT_INFOR)
    {
        Obj_robot_infor_t rInfor;
        std::copy ( msg->data.begin() + PAYLOAD_OFFSET, 
                    msg->data.begin() + PAYLOAD_OFFSET + sizeof(rInfor), 
                    (uint8_t*)&rInfor);
        printf ("encoder speed = %.3f \n", rInfor.drive_left.encoder_speed);
    }
    
    if (Obj_type == object_type_e::MAP_INFOR)
    {
        Obj_map_infor_t mInfor;
        std::copy ( msg->data.begin() + PAYLOAD_OFFSET, 
                    msg->data.begin() + PAYLOAD_OFFSET + sizeof(mInfor), 
                    (uint8_t*)&mInfor);
        printf ("h = %d\tw = %d\treso = %f \n", mInfor.heigh, mInfor.width, mInfor.resolution);
    }

    if (Obj_type == object_type_e::GET_MAP_FULL)
    {
        ROS_INFO ("MAP CALLBACK");
        Obj_map_infor_t mInfor;
        std::copy ( msg->data.begin() + PAYLOAD_OFFSET, 
                    msg->data.begin() + PAYLOAD_OFFSET + sizeof(mInfor), 
                    (uint8_t*)&mInfor);
        printf ("h = %d\tw = %d\treso = %f \n", mInfor.heigh, mInfor.width, mInfor.resolution);
    }
}

void testBridge::Send_data_to_Robot ()
{
    boost::thread th_handle(&testBridge::Thread_handle, this);
    th_handle.detach();
}

int main(int argc, char *argv[])
{
    ros::init(argc, argv, "test_bridge_transport");
    testBridge tB;

    tB.Send_data_to_Robot ();

    ros::spin ();
    return 0;
}