#pragma once

#include <iostream>
#include <vector>

namespace bridge_test
{
    #define ROBOT_MAX               10
    // #define IS_RUN_MOVE_BASE        1

    // Header
    /*
    |-----------------------------------------|
    |        0       |            1           |
    |----------------|------------------------|
    |  Command_Type  |       Object_Type      |
    |-----------------------------------------|
    */
    #define COMMAND_OFFSET					0
    #define HEADER_LEN						2
    #define PAYLOAD_OFFSET					HEADER_LEN
    #define MAX_PATH_LENGTH                 100

    // #define USE_RLE                         1

    using buffer = std::vector<int8_t>;
    typedef std::pair<double, double> Point;

    /*******************************************************************************
     *       Example command: Command_Type(set) + Object_Type + struct value
     *                        Command_Type(get) + Object_Type
     *******************************************************************************/

    /* TYPE COMMAND */
    typedef enum
    {
        GET_CMD = 0,    // 2 chieu
        SET_CMD,        // 1 chieu

        END_CMD
    } command_type_e;

    /* OBJECT COMMAND */
    // user -> robot 
    //     GET:    ROBOT_INFOR, MAP_INFOR
    //     SET:    PATH_MISSION, CONTROL_SIGNAL, GET_MAP_FULL

    // robot -> user
    //     GET:    NONE
    //     SET:    MISSION_STATUS, ROBOT_INFOR, MAP_INFOR, LOAD_MAP_STATUS

    typedef enum
    {   
        START_OBJECT = 0,

        ROBOT_INFOR,     

        MAP_INFOR,

        GET_MAP_FULL,

        MISSION_STATUS,

        LOAD_MAP_STATUS,

        PATH_MISSION,

        CONTROL_SIGNAL,

        END_OBJECT
    } object_type_e;

    // tin hieu dieu khien tu User 
    typedef enum
    {
        SEND_BIG_MAP = 0,       // - update bigMap file
        EMERGENCY,              // - Tin hieu dung xe khan cap
        CONTROL,                // - Tin hieu dieu khien trong 
                                //   qua trinh follow path
        FLAG_GET_MAP,           // - Tin hieu cho phep gui full map len User
        END_SIGNAL
    } signal_e;

    // follow waypoint in move_base
    typedef enum     
    {
        WP_UNKNOW,
        WP_GETPATH,
        WP_FOLLOW_PATH,
        WP_PATH_COMPLETE,
        WP_ERROR

    } wp_state_e;

    /* VALUE COMMAND: HieuTa definition */ 
    typedef struct 
    {
        /* data */
        bool ErrFree;
        bool OverVoltage;
        bool UnderVoltage;
        bool OverCurrent;
        bool Stall;
        bool OverTemperature;
        bool OverTemperatureMore75;
        bool IsEnable;
        bool IsAlive;

    } driver_error_t;

    typedef struct
    {
        /* data */
        float motor_voltage;
        float battery_voltage;
        float temperature;
        float motor_current;
        int64_t encoder_stick;
        driver_error_t fault;
        float encoder_speed;

    } engine_infor_t;

    typedef struct 
    {
        /* data */
        engine_infor_t drive_left;
        engine_infor_t drive_right;
        engine_infor_t drive_human;
        bool isRobotDied;

    } Obj_robot_infor_t;

    typedef struct
    {
        /* data */
        float x;
        float y;
        float yaw;
    } location_point_t;

    typedef struct 
    {
        /* data */
        float resolution;
        uint32_t width;  // [cells]
        uint32_t heigh;  // [cells]
        location_point_t origin;

    } Obj_map_infor_t;
    
    typedef struct
    {
        /* data */
        wp_state_e ms_status;

    } Obj_mission_status_t;

    typedef struct
    {
        /* data */
        /* status:
         *          0: free
         *          1: loading
         *          2: load done
         */             
        uint8_t status;

    } Obj_load_map_status_t;
    
    /*
     *  User to Robot 
     */
    typedef struct 
    {
        /* data */
        uint8_t len;
        Point path[MAX_PATH_LENGTH];

    } Obj_path_mission_t;

    typedef struct
    {
        /* data */
        signal_e signal_chosen;

        /* send_map
         * 0: NONE; 1: RELOAD_MAP;
         */ 
        bool send_map;

        /* emergency
         * 1: POWER_OFF_NOW; 0: NONE
         */ 
        bool emergency;
 
        /* control robot in move_base
         * 0: EXIT_FULL_PATH; 1: REJECT_GOAL_CURRENT; ELSE: NONE 
         */ 
        uint8_t control;

        /* set flag get full map
         * 0: EXIT_FULL_PATH; 1: REJECT_GOAL_CURRENT; ELSE: NONE 
         */ 
        volatile bool flag_get_map;

    } Obj_signal_t;

    typedef struct
    {
        // ROBOT_INFOR
        Obj_robot_infor_t robot;            

        // MAP_INFOR
        Obj_map_infor_t map_infor;

        // PATH_MISSION
        Obj_path_mission_t path;            

        // CONTROL_SIGNAL
        Obj_signal_t signal; 

        // MISSION_STATUS
        Obj_mission_status_t mission;        

        // LOAD_MAP_STATUS
        Obj_load_map_status_t load_map;      

    } appication_data_t;
}