#include <iostream>
#include <fstream>
#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <std_msgs/Float64.h>

using namespace std;
double      start;
std::ofstream wfile;

#define     PI                          3.14159265
#define     R_WHEEL                     0.1625

double speed_send = 0.0;
// double speed_receive = 0.0;
double freq_ = 30;
double speed_left, speed_right;
double right_current, left_current;

void twist_callback (const geometry_msgs::Twist& msg)
{
    speed_send = msg.linear.x;
}

void speed_left_cb (const std_msgs::Float64& msg)
{
    speed_left = msg.data;  // msg.data * R_WHEEL * 2 * PI / 420
}

void speed_right_cb (const std_msgs::Float64& msg)
{
    speed_right = msg.data;
}

void left_motor_current_cb (const std_msgs::Float64& msg)
{
    left_current = msg.data;
}

void right_motor_current_cb (const std_msgs::Float64& msg)
{
    right_current = msg.data;
}

void spin_timer (const ros::TimerEvent& e)
{
    double current = ros::Time::now().toSec();
    double diffTime = current - start;

    wfile << std::fixed << std::setprecision(5) << diffTime << "\t";
    wfile << std::fixed << std::setprecision(5) << speed_left << "\t";
    wfile << std::fixed << std::setprecision(5) << speed_right << "\t";
    wfile << std::fixed << std::setprecision(5) << left_current << "\t";
    wfile << std::fixed << std::setprecision(5) << right_current << std::endl;
}

int main(int argc, char *argv[])
{
    ros::init(argc, argv, "log_data");
    ros::NodeHandle nh;
    start = ros::Time::now ().toSec();
    std::cout << "----------LOG DATA----------" << std::endl;

    wfile.open ("robot_ws/src/application/write_data/cfg/log.txt");
    // wfile.open ("log.txt");

    if (wfile.is_open())
    {
        std::cout << "Write ready" << std::endl;
        // ros::Subscriber twist_sub = nh.subscribe("cmd_vel", 10, twist_callback);
        ros::Subscriber speedLeft_sub_ = nh.subscribe("left_drive/status/encoder_speed", 10, speed_left_cb);
        ros::Subscriber speedRight_sub_ = nh.subscribe("right_drive/status/encoder_speed", 10, speed_right_cb);

        ros::Subscriber left_motor_current_sub_ = nh.subscribe("left_drive/status/motor_current", 10, left_motor_current_cb);
        ros::Subscriber right_motor_current_sub_ = nh.subscribe("right_drive/status/motor_current", 10, right_motor_current_cb);

        // ros::Subscriber left_motor_current = nh.advertise<std_msgs::Float64>("left_drive/status/motor_current", 50);
        // ros::Subscriber right_motor_current = nh.advertise<std_msgs::Float64>("right_drive/status/motor_current", 50);
        
        {
            wfile << std::fixed << "  time   ";
            wfile << std::fixed << "lSpeed  ";
            wfile << std::fixed << "rSpeed  ";
            wfile << std::fixed << "lCurr   ";
            wfile << std::fixed << "rCurr" << std::endl;
        }
        
        ros::Timer timer = nh.createTimer(ros::Duration (1.0/std::max(freq_, 1.0)), spin_timer);
        ros::spin();
    }
    else
    {
        std::cout << "Unble to read file" << std::endl;
        return 0;
    }

    wfile.close ();
    return 0;
}