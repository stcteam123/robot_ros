#ifndef _H_BRIDGE_USER_FOLLOW_WAYPOINTS_H_
#define _H_BRIDGE_USER_FOLLOW_WAYPOINTS_H_

#include "handler.h"

namespace bridge
{
    follow_wp::follow_wp () : sm_robot (wp_state_e::WP_UNKNOW),
                              close_scan (true),
                              isMoveBaseConnected (false)
    {
        waypoints_rsv.poses.clear ();
    }
    
    follow_wp::~follow_wp() 
    {
        if (sm_robot == wp_state_e::WP_FOLLOW_PATH)
        {
            sm_robot = wp_state_e::WP_GETPATH;
            ac->cancelGoal();

            printf ("EXIT PATH FOLLOWING \n");
        }

        delete ac;
    }

    void follow_wp::Init_Fwp ()
    {
        mission_status_pub = nh.advertise<std_msgs::Int8MultiArray>("pathFollow/mission_status", 10);

        ac = new MoveBaseClient("/move_base", true);
        Connect_Move_Base ();

        close_scan = false;
        boost::thread scan_thread(&follow_wp::Thread_Scan_Move_Base_Ready, this);
        scan_thread.detach();
    }

    void follow_wp::Ac_Mission_Follow_Path (Obj_path_mission_t& path)
    {
        if (sm_robot == wp_state_e::WP_GETPATH)
        {
            // assign data -> waypoints_rsv
            waypoints_rsv.poses.clear ();
            geometry_msgs::Pose point; 
            for (int i = 0; i < path.len; i++)
            {
                point.position.x = path.path[i].x;
                point.position.y = path.path[i].y;

                waypoints_rsv.poses.push_back (point);
            }

            if (path.len)
            {
                // thread handle follow path
                sm_robot = wp_state_e::WP_FOLLOW_PATH;
                boost::thread follow_thread(&follow_wp::Thread_Ac_Mission, this);
                follow_thread.detach();
            }
        }
        else
        {
            printf ("Reject Path was sent from User \n");
        }
    }

    void follow_wp::Receive_Control (Obj_ctrl_path_ms_t& ctrl)
    {
        if (isMoveBaseConnected)
        {
            if (ctrl.control == 0)
            {
                if (sm_robot == wp_state_e::WP_FOLLOW_PATH)
                {
                    sm_robot = wp_state_e::WP_GETPATH;
                    ac->cancelGoal();

                    printf ("EXIT FULL PATH FOLLOWING \n");
                }
            }
            else if (ctrl.control == 1)
            {
                if (sm_robot == wp_state_e::WP_FOLLOW_PATH)
                {
                    ac->cancelGoal();
                    printf ("SKIP A GOAL CURRENT FOLLOWING \n");
                }
            }
            else
            {
                printf ("UNKNOW \n");
            }
        }
    }

    void follow_wp::Thread_Ac_Mission ()
    {
        if (sm_robot == wp_state_e::WP_FOLLOW_PATH)
        {
            //wait for the action server to come up
            while(!ac->waitForServer(ros::Duration(5.0)))
            {
                ROS_INFO("Waiting for the move_base action server to come up");
                isMoveBaseConnected = false;
                sm_robot = wp_state_e::WP_ERROR;
                Public_Robot_Mission_Status (wp_state_e::WP_ERROR);
                waypoints_rsv.poses.clear ();
                return;
            }

            Public_Robot_Mission_Status (sm_robot);
            move_base_msgs::MoveBaseGoal goal;

            //we'll send a goal to the robot to move 1 meter forward
            goal.target_pose.header.frame_id = "odom";
            goal.target_pose.header.stamp = ros::Time::now();

            unsigned int size_wps = waypoints_rsv.poses.size ();

            for (int i = 0; i < size_wps; i++) 
            {
                if (sm_robot == wp_state_e::WP_GETPATH)
                {
                    ROS_INFO ("STOP threadAcMission");
                    waypoints_rsv.poses.clear ();
                    break;
                }

                goal.target_pose.pose.position.x = waypoints_rsv.poses[i].position.x;
                goal.target_pose.pose.position.y = waypoints_rsv.poses[i].position.y;
                goal.target_pose.pose.orientation = tf::createQuaternionMsgFromYaw(0.0); //waypoints_rsv.poses[i].orientation;

                // odom_.pose.pose.orientation = tf::createQuaternionMsgFromYaw(0.0);

                ROS_INFO("Sending goal");
                ac->sendGoal(goal);
                ac->waitForResult();
                if(ac->getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
                {
                    ROS_INFO("(SUCCEEDED) Hooray, the base moved 1 meter forward");
                }
                else
                {
                    ROS_INFO("The base failed to move forward 1 meter for some reason");
                }
            }

            sm_robot = wp_state_e::WP_PATH_COMPLETE;
            Public_Robot_Mission_Status (wp_state_e::WP_PATH_COMPLETE);

            ROS_INFO ("(threadAcMission) PATH_COMPLETE: Ready to receive a new goal path");

            sm_robot = wp_state_e::WP_GETPATH;
            Public_Robot_Mission_Status (wp_state_e::WP_GETPATH);
            waypoints_rsv.poses.clear ();
        }
        else
        {
            ROS_INFO ("sm_robot is [%d]", sm_robot);
        }
    }

    void follow_wp::Thread_Scan_Move_Base_Ready ()
    {
        while (!close_scan)
        {
            if (!isMoveBaseConnected)
            {
                if (!Connect_Move_Base ())
                {
                    boost::this_thread::sleep(boost::posix_time::milliseconds(500));
                    continue;  // Connect failed, sleep, try again.
                }
            }

            boost::this_thread::sleep(boost::posix_time::milliseconds(1000));

            // scan connect status with server move_base
            //wait for the action server to come up
            while(!ac->waitForServer(ros::Duration(5.0)))
            {
                isMoveBaseConnected = false;
                sm_robot = wp_state_e::WP_ERROR;
                Public_Robot_Mission_Status (wp_state_e::WP_ERROR);
                printf ("Timer check status: FAIL WITH MOVE_BASE \n");
                break;
            }
        }
    }

    bool follow_wp::Connect_Move_Base ()
    {
        int wait_count = 0;
        while(!ac->waitForServer(ros::Duration(3.0)))
        {
            wait_count++;
            if(wait_count > 3)
            {
                ROS_ERROR("move_base action server did not come up.");
                ROS_INFO ("Fail Connected");
                isMoveBaseConnected = false;
                sm_robot = wp_state_e::WP_ERROR;
                return false;
            }
            ROS_INFO("Waiting for the move_base action server to come up");
        }
        
        isMoveBaseConnected = true;   
        sm_robot = wp_state_e::WP_GETPATH;
        ROS_INFO ("Connected");
        return true;
    }

    void follow_wp::Public_Robot_Mission_Status (wp_state_e state)
    {
        Obj_mission_status_t s_mission;
        s_mission.ms_status = state;
        uint32_t len = sizeof (s_mission);

        std_msgs::Int8MultiArray msg;
        msg.data.clear ();
        msg.data.push_back (command_type_e::SET_CMD);
        msg.data.push_back (object_type_e::MISSION_STATUS);
        
        msg.data.resize (len + PAYLOAD_OFFSET);
        std::copy ( (uint8_t*)&s_mission, 
                        (uint8_t*)&s_mission + len, 
                        msg.data.begin() + PAYLOAD_OFFSET);
        mission_status_pub.publish (msg);
    }

    bool follow_wp::Get_Wp_Ready ()
    {
        return isMoveBaseConnected;
    }
}

#endif // _H_BRIDGE_USER_FOLLOW_WAYPOINTS_H_