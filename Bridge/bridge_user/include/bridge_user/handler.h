#ifndef _H_BRIDGE_USER_HANDLER_H_
#define _H_BRIDGE_USER_HANDLER_H_

#include "define.h"

#include <ros/ros.h>
#include <tf/tf.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>
#include <geometry_msgs/PoseArray.h>    // std_msgs/Header header
                                        // geometry_msgs/Pose[] poses

#include <robot_localization/navsat_conversions.h>
#include <std_msgs/Float64MultiArray.h>
#include <geometry_msgs/PointStamped.h>
#include <geometry_msgs/PoseArray.h>
#include <tf/transform_listener.h>

#include <std_msgs/Int8MultiArray.h>
#include <std_msgs/String.h>
#include <std_msgs/Int32.h>
#include <std_msgs/Int8.h>

#include <iostream>
#include <boost/thread.hpp>
#include <thread>
#include <mutex>

namespace bridge
{
    class gps_waypoints
    {
        public:
            gps_waypoints() {}

            // using pick points
            geometry_msgs::PointStamped latLongtoUTM(double latti_input, double longti_input);
            geometry_msgs::PointStamped UTMtoMapPoint(geometry_msgs::PointStamped UTM_input);
            bool TryUTMtoMapPoint(geometry_msgs::PointStamped UTM_input, geometry_msgs::PointStamped* map_output);
            void gps_convert (Obj_path_mission_gps_t& gps_path, Obj_path_mission_t& path);
            void gps_waypointsCallback(const std_msgs::Float64MultiArray::ConstPtr& array);

        private:
            geometry_msgs::PointStamped UTM_point, map_point, UTM_next, map_next;
            std::string utm_zone;
    };

    class destroy
    {
        public:
            void destroy_handler (void);
    };

    class reload_map
    {
        public:
            void reload_map_handler();
        
        private:
            void loadMap_thread ();
    };

    class follow_wp
    {
        public:
            typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;

            follow_wp();
            ~follow_wp();

            void Init_Fwp ();

            void Ac_Mission_Follow_Path (Obj_path_mission_t& path);

            void Receive_Control (Obj_ctrl_path_ms_t& ctrl);

            void Thread_Ac_Mission ();

            void Thread_Scan_Move_Base_Ready ();

            bool Connect_Move_Base ();

            void Public_Robot_Mission_Status (wp_state_e state);

            bool Get_Wp_Ready ();
        
        private:

            ros::Publisher              mission_status_pub;

            // variables
            wp_state_e                  sm_robot;
            bool                        isMoveBaseConnected;
            bool                        close_scan;

            geometry_msgs::PoseArray    waypoints_rsv;
            MoveBaseClient*             ac;
            ros::NodeHandle             nh;

            // functions
            void Cal_Orientation ( const geometry_msgs::PoseArray::ConstPtr& src,
                                   geometry_msgs::PoseArray& dst);
    };   
}

#endif // _H_BRIDGE_USER_HANDLER_H_