#ifndef _BRIDGE_BRIDGE_USER_DEFINITION_H_
#define _BRIDGE_BRIDGE_USER_DEFINITION_H_

#include <iostream>
#include <vector>

namespace bridge
{
    #define ROBOT_MAX               10

    // Header
    /*
    |-----------------------------------------|
    |        0       |            1           |
    |----------------|------------------------|
    |  Command_Type  |       Object_Type      |
    |-----------------------------------------|
    */
    #define COMMAND_OFFSET					0
    #define HEADER_LEN						2
    #define PAYLOAD_OFFSET					HEADER_LEN
    #define MAX_PATH_LENGTH                 100

    /*******************************************************************************
     *       Example command: Command_Type(set) + Object_Type + struct value
     *                        Command_Type(get) + Object_Type
     *******************************************************************************/

    /* TYPE COMMAND */
    typedef enum
    {
        GET_CMD = 0,    
        SET_CMD,        

        END_CMD
    } command_type_e;

    typedef enum
    {   
        ROBOT_LOCATION,             // GET
        ROBOT_INFO,                 // GET
        PATH_MISSION,                   // SET
        PATH_MISSION_GPS,               // SET
        CONTROL_PATH_MISSION,           // SET
        MISSION_STATUS,             // GET
        RELOAD_MAP_MOVE_BASE,           // SET
        CONTROLLER_ROBOT,               // SET
        DESTROY,                        // SET

        END_OBJECT
    } object_type_e;

    // follow waypoint in move_base
    typedef enum     
    {
        WP_UNKNOW,
        WP_GETPATH,
        WP_FOLLOW_PATH,
        WP_PATH_COMPLETE,
        WP_ERROR

    } wp_state_e;

    // define struct data
    typedef struct driver_error
    {
        /* data */
        bool ErrFree;
        bool OverVoltage;
        bool UnderVoltage;
        bool OverCurrent;
        bool Stall;
        bool OverTemperature;
        bool OverTemperatureMore75;
        bool IsEnable;
        bool IsAlive;

    } driver_error_t;

    typedef struct engine_info
    {
        /* data */
        float motor_voltage;
        float battery_voltage;
        float temperature;
        float motor_current;
        int64_t encoder_stick;
        driver_error_t fault;
        float encoder_speed;

    } engine_info_t;

    typedef struct location_point
    {
        /* data */
        float x;
        float y;
        float yaw;
    } location_point_t;

    typedef struct location_gps
    {
        /* data */
        float longitude;
        float latitude;
    } location_gps_t;

    // ROBOT_LOCATION
    typedef struct Obj_robot_location
    {
         /* data */
        location_gps_t robot_gps;
        location_point_t robot_pose;

    } Obj_robot_location_t;

    // ROBOT_INFO
    typedef struct Obj_robot_info
    {
        /* data */
        engine_info_t drive_left;
        engine_info_t drive_right;
        engine_info_t drive_mannequin;
        bool isRobotDied;

    } Obj_robot_info_t;

    // PATH_MISSION
    typedef struct Obj_path_mission
    {
        /* data */
        uint8_t len;
        location_point_t path[MAX_PATH_LENGTH];

    } Obj_path_mission_t;

    // PATH_MISSION_GPS
    typedef struct Obj_path_mission_gps
    {
        /* data */
        uint8_t len;
        location_gps path[MAX_PATH_LENGTH];
    } Obj_path_mission_gps_t;
    
    // CONTROL_PATH_MISSION
    typedef struct Obj_ctrl_path_ms
    {
        /* control robot in move_base
         * 0: EXIT_FULL_PATH; 1: REJECT_GOAL_CURRENT; ELSE: NONE 
         */
        uint8_t control;
    } Obj_ctrl_path_ms_t;

    // MISSION_STATUS
    typedef struct Obj_mission_status
    {
        /* data */
        wp_state_e ms_status;

    } Obj_mission_status_t;

    // RELOAD_MAP_MOVE_BASE
    typedef struct Obj_reload_map
    {
        /* reload: 0 - None, 1 - Reload */
        uint8_t reload;
    } Obj_reload_map_t;

    // CONTROLLER_ROBOT
    typedef struct Obj_controller
    {
        /* reload: 0 - None, 1 - Reload */
        float linear;
        float angular;
    } Obj_controller_t;

    // DESTROY
    typedef struct Obj_destroy
    {
        uint8_t key[8];
        uint8_t destroy;
    } Obj_destroy_t;

    typedef struct
    {
        Obj_robot_location_t    robot_loc;          // ROBOT_LOCATION 
        Obj_robot_info_t        robot;              // ROBOT_INFO
        Obj_path_mission_t      path;               // PATH_MISSION
        Obj_path_mission_gps_t  path_gps;           // PATH_MISSION_GPS
        Obj_ctrl_path_ms_t      ctrl_path;          // CONTROL_PATH_MISSION
        Obj_mission_status_t    mission_s;          // MISSION_STATUS
        Obj_reload_map_t        reload;             // RELOAD_MAP_MOVE_BASE
        Obj_controller_t        ctrl_Robot;         // CONTROLLER_ROBOT
        Obj_destroy_t           destroy;            // DESTROY   

    } application_data_t;
}

#endif // _BRIDGE_BRIDGE_USER_DEFINITION_H_