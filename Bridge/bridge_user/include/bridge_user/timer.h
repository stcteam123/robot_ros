#ifndef _H_BRIDGE_USER_TIMER_H_
#define _H_BRIDGE_USER_TIMER_H_

#include <iostream>
#include <chrono> // for std::chrono functions

namespace bridge 
{
	class Timer
	{
		private:
			// Type aliases to make accessing nested type easier
			using clock_t = std::chrono::high_resolution_clock;
			using second_t = std::chrono::duration<double, std::ratio<1> >;
		
			std::chrono::time_point<clock_t> m_beg;
		
		public:
			Timer() : m_beg(clock_t::now())
			{
			}
		
			void reset()
			{
				m_beg = clock_t::now();
			}
		
			double elapsed() const
			{
				return std::chrono::duration_cast<second_t>(clock_t::now() - m_beg).count();
			}
	};
}

// use: example
/*
	Timer t;
 
	//  TODO (func)
 
	std::cout << "Time taken: " << t.elapsed() << " seconds\n";
*/

#endif // _H_BRIDGE_USER_TIMER_H_