#ifndef _H_BRIDGE_USER_GPS_WAYPOINTS_H_
#define _H_BRIDGE_USER_GPS_WAYPOINTS_H_

#include "handler.h"

namespace bridge
{
    // Convert latti and longti to UTM axits
    geometry_msgs::PointStamped gps_waypoints::latLongtoUTM(double latti_input, double longti_input)
    {
        double utm_x = 0, utm_y = 0;
        geometry_msgs::PointStamped UTM_point_output;

        // chuyển đổi từ kinh độ vị độ sang tọa độ UTM
        RobotLocalization::NavsatConversions::LLtoUTM(latti_input, longti_input,utm_y, utm_x, utm_zone);
        UTM_point_output.header.frame_id = "utm";
        UTM_point_output.header.stamp = ros::Time(0);
        UTM_point_output.point.x = utm_x;
        UTM_point_output.point.y = utm_y;
        UTM_point_output.point.z = 0;
        return UTM_point_output;
    }

    // Chuyển đội tọa độ từ UTM sang tạo độ cục bộ "map" hoặc "odom"
    geometry_msgs::PointStamped gps_waypoints::UTMtoMapPoint(geometry_msgs::PointStamped UTM_input)
    {
        geometry_msgs::PointStamped map_point_output;
        bool notDone = true;
        tf::TransformListener listener;
        ros::Time time_now = ros::Time::now();
        while(notDone)
        {
            try
            {
                UTM_point.header.stamp = ros::Time::now();
                listener.waitForTransform("odom", "utm", time_now, ros::Duration(3.0));
                listener.transformPoint("odom", UTM_input, map_point_output);
                notDone = false;
            }
            catch (tf::TransformException& ex)
            {
                ROS_WARN("%s", ex.what());
                ros::Duration(0.01).sleep();
                //return;
            }
        }
        return map_point_output;
    }

    bool gps_waypoints::TryUTMtoMapPoint(geometry_msgs::PointStamped UTM_input, geometry_msgs::PointStamped* map_output)
    {
        geometry_msgs::PointStamped* map_point_output;
        map_point_output = map_output;
        bool notDone = true;
        tf::TransformListener listener;
        ros::Time time_now = ros::Time::now();
        while(notDone)
        {
            try
            {
                UTM_point.header.stamp = ros::Time::now();
                listener.waitForTransform("odom", "utm", time_now, ros::Duration(3.0));
                listener.transformPoint("odom", UTM_input, *map_point_output);
                notDone = false;
            }
            catch (tf::TransformException& ex)
            {
                ROS_WARN("%s", ex.what());
                ros::Duration(0.01).sleep();
                return false;
            }
        }
        return true;
    }

    void gps_waypoints::gps_convert (Obj_path_mission_gps_t& gps_path, Obj_path_mission_t& path)
    {
        path.len = gps_path.len;

        for (int i = 0; i < gps_path.len ; i++)
        {
            UTM_point = latLongtoUTM((double)gps_path.path[i].latitude, (double)gps_path.path[i].longitude);
            if (TryUTMtoMapPoint(UTM_point, &map_point) == 0)
            {
                ROS_INFO ("convert UTM to MapPoint FALSE");
                return;
            }
            path.path[i].x = map_point.point.x;
            path.path[i].y = map_point.point.y;
        }
    }
}

#endif // _H_BRIDGE_USER_GPS_WAYPOINTS_H_