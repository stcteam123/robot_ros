#pragma once

/* Ros Lib */
#include <ros/ros.h>
#include <std_msgs/Int8MultiArray.h>
#include <std_msgs/Int8.h>
#include <nav_msgs/OccupancyGrid.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Int64.h>
#include <tf/tf.h>
#include <tf2/utils.h>
#include "bridge_user/drive_error.h"
#include "nav_msgs/Odometry.h"
#include "geometry_msgs/Twist.h"
#include "sensor_msgs/NavSatFix.h"

/* Transport Lib */
#include "gps_waypoints.h"
#include "destroy.h"
#include "reload_map.h"
#include "follow_waypoints.h"

/* C++ Lib */
#include <iostream>
#include <boost/thread.hpp>
#include <thread>
#include <mutex>
#include <vector>
#include <sys/time.h>
#include <math.h>

namespace bridge
{
    class transport
    {
        public:
            transport();
            ~transport();

            void transport_run ();
        
        private:
            application_data_t              app_data;
            follow_wp                       fWayPoints;

            bool                            isMoveBaseRun;
            double                          time_get_map;

            ros::NodeHandle                 nh, nh_private;
            
            ros::Publisher                  user_package_pub;
            ros::Subscriber                 user_package_sub;
            ros::Subscriber                 robot_mission_status_sub;
            
            ros::Publisher                  ctrl_robot_pub;

            ros::Subscriber                 odomRobot_sub;
            ros::Subscriber                 gpsRobot_sub;

            /*
             * data robot info
             * */
            ros::Subscriber                 left_motor_voltage,             \
                                            left_battery_voltage,           \
                                            left_drive_temp,                \
                                            left_motor_current,             \
                                            left_encoder_stick,             \
                                            left_drive_fault,               \
                                            left_encoder_speed,             \
                                                                            \
                                            right_motor_voltage,            \
                                            right_battery_voltage,          \
                                            right_drive_temp,               \
                                            right_motor_current,            \
                                            right_encoder_stick,            \
                                            right_drive_fault,              \
                                            right_encoder_speed;

            // Receive from User
            void User_Package_Cb            (const std_msgs::Int8MultiArray::ConstPtr& msg);

            void Robot_mission_status       (const std_msgs::Int8MultiArray& msg);

            void Odom_Callback              (const nav_msgs::Odometry& msg);
            void Gps_Callback               (const sensor_msgs::NavSatFix& msg);

            void Left_Motor_Voltage_Cb      (const std_msgs::Float64::ConstPtr& msg);
            void Left_Battery_Voltage_Cb    (const std_msgs::Float64::ConstPtr& msg);
            void Left_Drive_Temp_Cb         (const std_msgs::Float64::ConstPtr& msg);
            void Left_Motor_Current_Cb      (const std_msgs::Float64::ConstPtr& msg);
            void Left_Encoder_Stick_Cb      (const std_msgs::Int64::ConstPtr& msg);
            void Left_Drive_Fault_Cb        (const bridge_user::drive_error::ConstPtr& msg);
            void Left_Encoder_Speed_Cb      (const std_msgs::Float64::ConstPtr& msg);

            void Right_Motor_Voltage_Cb     (const std_msgs::Float64::ConstPtr& msg);
            void Right_Battery_Voltage_Cb   (const std_msgs::Float64::ConstPtr& msg);
            void Right_Drive_Temp_Cb        (const std_msgs::Float64::ConstPtr& msg);
            void Right_Motor_Current_Cb     (const std_msgs::Float64::ConstPtr& msg);
            void Right_Encoder_Stick_Cb     (const std_msgs::Int64::ConstPtr& msg);
            void Right_Drive_Fault_Cb       (const bridge_user::drive_error::ConstPtr& msg);
            void Right_Encoder_Speed_Cb     (const std_msgs::Float64::ConstPtr& msg);

            void App_Send_Robot_Info        ();
            void App_Send_Robot_location    ();

            void App_Send_Data_To_Protocol  (const std_msgs::Int8MultiArray& msg);
    };

    transport::transport() : isMoveBaseRun (true),
                             nh_private ("~")
    {
        memset (&app_data, 0, sizeof (app_data));
        if(!nh_private.getParam("run_move_base", isMoveBaseRun))
        {
            isMoveBaseRun = true;
        }
    }

    transport::~transport() {}

    void transport::transport_run ()
    {
        // send and receive from protocol
        user_package_pub        = nh.advertise<std_msgs::Int8MultiArray>("robot/send_package", 10, true);
        user_package_sub        = nh.subscribe("robot/recv_package", 10, &transport::User_Package_Cb, this);

        // receive path mission to follow waypoint func
        robot_mission_status_sub = nh.subscribe("pathFollow/mission_status", 10, &transport::Robot_mission_status, this);

        // controller robot from User
        ctrl_robot_pub          = nh.advertise<geometry_msgs::Twist>("/user_app/cmd_vel", 10);

        // location Robot
        odomRobot_sub           = nh.subscribe("/odom", 10, &transport::Odom_Callback, this);
        gpsRobot_sub            = nh.subscribe("/gps/fix", 10, &transport::Gps_Callback, this);

        // driver info: left and right
        left_motor_voltage      = nh.subscribe("left_drive/status/motor_voltage", 10, &transport::Left_Motor_Voltage_Cb, this);
        left_battery_voltage    = nh.subscribe("left_drive/status/battery_voltage", 10, &transport::Left_Battery_Voltage_Cb, this);
        left_drive_temp         = nh.subscribe("left_drive/status/drive_temperature", 10, &transport::Left_Drive_Temp_Cb, this);
        left_motor_current      = nh.subscribe("left_drive/status/motor_current", 10, &transport::Left_Motor_Current_Cb, this);
        left_encoder_stick      = nh.subscribe("left_drive/status/encoder_stick", 10, &transport::Left_Encoder_Stick_Cb, this);
        left_drive_fault        = nh.subscribe("left_drive/status/fault", 10, &transport::Left_Drive_Fault_Cb, this);
        left_encoder_speed      = nh.subscribe("left_drive/status/encoder_speed", 10, &transport::Left_Encoder_Speed_Cb, this);
        
        right_motor_voltage     = nh.subscribe("right_drive/status/motor_voltage", 10, &transport::Right_Motor_Voltage_Cb, this);
        right_battery_voltage   = nh.subscribe("right_drive/status/battery_voltage", 10, &transport::Right_Battery_Voltage_Cb, this);
        right_drive_temp        = nh.subscribe("right_drive/status/drive_temperature", 10, &transport::Right_Drive_Temp_Cb, this);
        right_motor_current     = nh.subscribe("right_drive/status/motor_current", 10, &transport::Right_Motor_Current_Cb, this);
        right_encoder_stick     = nh.subscribe("right_drive/status/encoder_stick", 10, &transport::Right_Encoder_Stick_Cb, this);
        right_drive_fault       = nh.subscribe("right_drive/status/fault", 10, &transport::Right_Drive_Fault_Cb, this);
        right_encoder_speed     = nh.subscribe("right_drive/status/encoder_speed", 10, &transport::Right_Encoder_Speed_Cb, this);

        if (isMoveBaseRun)
        {
            fWayPoints.Init_Fwp ();
        }
    }

    // functions handler: receive data from protocol
    void transport::User_Package_Cb (const std_msgs::Int8MultiArray::ConstPtr& msg)
    {
        //todo: need to check lenData with length of struct before using function std::copy
        printf ("(transport_node) User_Package_Cb\n");

        command_type_e   cmd_type   = (command_type_e) msg->data[COMMAND_OFFSET];
        object_type_e    Obj_type   = (object_type_e) msg->data[COMMAND_OFFSET + 1];
        uint16_t         lenData    = msg->data.size () - PAYLOAD_OFFSET;

        printf ("cmd_type : %d \t Obj_type: %d \t lenData: %d\n", cmd_type, Obj_type, lenData);

        if (cmd_type >= END_CMD || Obj_type >= END_OBJECT)
        {
            return;
        }

        if (cmd_type == command_type_e::SET_CMD)
        {
            switch (Obj_type)
            {
                case object_type_e::PATH_MISSION:
                case object_type_e::PATH_MISSION_GPS:
                {
                    if (fWayPoints.Get_Wp_Ready () == false)
                    {
                        break;
                    }

                    if (Obj_type == object_type_e::PATH_MISSION_GPS)
                    {
                        std::copy ( msg->data.begin() + PAYLOAD_OFFSET, 
                                    msg->data.begin() + PAYLOAD_OFFSET + sizeof(app_data.path_gps), 
                                    (uint8_t*)&app_data.path_gps);

                        // convert from path_gps to path
                        bridge::gps_waypoints gps;
                        gps.gps_convert (app_data.path_gps, app_data.path);
                    }
                    else
                    {
                        std::copy ( msg->data.begin() + PAYLOAD_OFFSET, 
                                    msg->data.begin() + PAYLOAD_OFFSET + sizeof(app_data.path), 
                                    (uint8_t*)&app_data.path);
                    }

                    fWayPoints.Ac_Mission_Follow_Path (app_data.path);
                    break;
                }

                case object_type_e::CONTROL_PATH_MISSION:
                {
                    std::copy ( msg->data.begin() + PAYLOAD_OFFSET, 
                                msg->data.begin() + PAYLOAD_OFFSET + sizeof(app_data.ctrl_path), 
                                (uint8_t*)&app_data.ctrl_path);
                    fWayPoints.Receive_Control (app_data.ctrl_path);
                    break;
                }

                case object_type_e::RELOAD_MAP_MOVE_BASE:
                {
                    std::copy ( msg->data.begin() + PAYLOAD_OFFSET, 
                                msg->data.begin() + PAYLOAD_OFFSET + sizeof(app_data.reload), 
                                (uint8_t*)&app_data.reload);

                    if (app_data.reload.reload == 1)
                    {
                        bridge::reload_map loadMap;
                        loadMap.reload_map_handler ();
                    }
                    break;
                }

                case object_type_e::CONTROLLER_ROBOT:
                {
                    std::copy ( msg->data.begin() + PAYLOAD_OFFSET, 
                                msg->data.begin() + PAYLOAD_OFFSET + sizeof(app_data.ctrl_Robot), 
                                (uint8_t*)&app_data.ctrl_Robot);
                    // controller robot from user
                    geometry_msgs::Twist cmd_vel;
                    cmd_vel.linear.x = app_data.ctrl_Robot.linear;
                    cmd_vel.angular.z = app_data.ctrl_Robot.angular;

                    ctrl_robot_pub.publish (cmd_vel);
                    break;
                }

                case object_type_e::DESTROY:
                {
                    std::copy ( msg->data.begin() + PAYLOAD_OFFSET, 
                                msg->data.begin() + PAYLOAD_OFFSET + sizeof(app_data.destroy), 
                                (uint8_t*)&app_data.destroy);

                    //todo: check key is true: app_data.destroy.key

                    if (app_data.destroy.destroy == 1)
                    {
                        // 1 2 3: BUM
                        bridge::destroy destroy;
                        destroy.destroy_handler();
                    }

                    break;
                }

                default:
                    printf ("DEFAULT SET_CMD\n");
            }
        }
        
        if (cmd_type == command_type_e::GET_CMD)
        {
            switch (Obj_type)
            {
                case object_type_e::ROBOT_LOCATION:
                {
                    App_Send_Robot_location ();
                    break;
                }
                
                case object_type_e::ROBOT_INFO:
                {   
                    App_Send_Robot_Info ();
                    break;
                }

                default:
                    printf ("DEFAULT GET\n");
            }
        }
    }

    void transport::Robot_mission_status (const std_msgs::Int8MultiArray& msg)
    {
        //todo: can don't need publish to User
        // App_Send_Data_To_Protocol (msg);
    }

    void transport::Odom_Callback (const nav_msgs::Odometry& msg)
    {
        double roll, pitch, yaw;
        tf::Quaternion q ( msg.pose.pose.orientation.x, msg.pose.pose.orientation.y, 
                                msg.pose.pose.orientation.z, msg.pose.pose.orientation.w);
        tf::Matrix3x3 m(q);
        m.getRPY (roll, pitch, yaw);

        app_data.robot_loc.robot_pose.x = msg.pose.pose.position.x;
        app_data.robot_loc.robot_pose.y = msg.pose.pose.position.y;
        app_data.robot_loc.robot_pose.yaw = yaw;
    }

    void transport::Gps_Callback (const sensor_msgs::NavSatFix& msg)
    {
        app_data.robot_loc.robot_gps.latitude = msg.latitude;
        app_data.robot_loc.robot_gps.longitude = msg.longitude;
    }

    void transport::Left_Motor_Voltage_Cb (const std_msgs::Float64::ConstPtr& msg)
    {
        app_data.robot.drive_left.motor_voltage = msg->data;
    }
    void transport::Left_Battery_Voltage_Cb (const std_msgs::Float64::ConstPtr& msg)
    {
        app_data.robot.drive_left.battery_voltage = msg->data;
    }
    void transport::Left_Drive_Temp_Cb (const std_msgs::Float64::ConstPtr& msg)
    {
        app_data.robot.drive_left.temperature = msg->data;
    }
    void transport::Left_Motor_Current_Cb (const std_msgs::Float64::ConstPtr& msg)
    {
        app_data.robot.drive_left.motor_current = msg->data;
    }
    void transport::Left_Encoder_Stick_Cb (const std_msgs::Int64::ConstPtr& msg)
    {
        app_data.robot.drive_left.encoder_stick = msg->data;
    }
    void transport::Left_Drive_Fault_Cb (const bridge_user::drive_error::ConstPtr& msg)
    {
        app_data.robot.drive_left.fault.ErrFree      = msg->ErrFree;
        app_data.robot.drive_left.fault.OverVoltage  = msg->OverVoltage;
        app_data.robot.drive_left.fault.UnderVoltage = msg->UnderVoltage;
        app_data.robot.drive_left.fault.OverCurrent  = msg->OverCurrent;
        app_data.robot.drive_left.fault.Stall        = msg->Stall;
        app_data.robot.drive_left.fault.OverTemperature = msg->OverTemperature;
        app_data.robot.drive_left.fault.OverTemperatureMore75 = msg->OverTemperatureMore75;
        app_data.robot.drive_left.fault.IsEnable     = msg->IsEnable;
        app_data.robot.drive_left.fault.IsAlive      = msg->IsAlive;
    }
    void transport::Left_Encoder_Speed_Cb (const std_msgs::Float64::ConstPtr& msg)
    {
        app_data.robot.drive_left.encoder_speed = msg->data;
    }

    void transport::Right_Motor_Voltage_Cb (const std_msgs::Float64::ConstPtr& msg)
    {
        app_data.robot.drive_right.motor_voltage = msg->data;
    }
    void transport::Right_Battery_Voltage_Cb (const std_msgs::Float64::ConstPtr& msg)
    {
        app_data.robot.drive_right.battery_voltage = msg->data;
    }
    void transport::Right_Drive_Temp_Cb (const std_msgs::Float64::ConstPtr& msg)
    {
        app_data.robot.drive_right.temperature = msg->data;
    }
    void transport::Right_Motor_Current_Cb (const std_msgs::Float64::ConstPtr& msg)
    {
        app_data.robot.drive_right.motor_current = msg->data;
    }
    void transport::Right_Encoder_Stick_Cb (const std_msgs::Int64::ConstPtr& msg)
    {
        app_data.robot.drive_right.encoder_stick = msg->data;
    }
    void transport::Right_Drive_Fault_Cb (const bridge_user::drive_error::ConstPtr& msg)
    {
        app_data.robot.drive_right.fault.ErrFree      = msg->ErrFree;
        app_data.robot.drive_right.fault.OverVoltage  = msg->OverVoltage;
        app_data.robot.drive_right.fault.UnderVoltage = msg->UnderVoltage;
        app_data.robot.drive_right.fault.OverCurrent  = msg->OverCurrent;
        app_data.robot.drive_right.fault.Stall        = msg->Stall;
        app_data.robot.drive_right.fault.OverTemperature = msg->OverTemperature;
        app_data.robot.drive_right.fault.OverTemperatureMore75 = msg->OverTemperatureMore75;
        app_data.robot.drive_right.fault.IsEnable     = msg->IsEnable;
        app_data.robot.drive_right.fault.IsAlive      = msg->IsAlive;
    }
    void transport::Right_Encoder_Speed_Cb (const std_msgs::Float64::ConstPtr& msg)
    {
        app_data.robot.drive_right.encoder_speed = msg->data;
    }

    void transport::App_Send_Robot_Info ()
    {
        uint32_t len = sizeof (app_data.robot);
        std_msgs::Int8MultiArray msg;
        msg.data.clear();
        msg.data.push_back (command_type_e::SET_CMD);
        msg.data.push_back (object_type_e::ROBOT_INFO);

        msg.data.resize (len + PAYLOAD_OFFSET);
        std::copy ( (uint8_t*)&app_data.robot, 
                    (uint8_t*)&app_data.robot + len, 
                    msg.data.begin() + PAYLOAD_OFFSET);

        App_Send_Data_To_Protocol (msg);
    }

    void transport::App_Send_Robot_location ()
    {
        uint32_t len = sizeof (app_data.robot_loc);
        std_msgs::Int8MultiArray msg;
        msg.data.clear();
        msg.data.push_back (command_type_e::SET_CMD);
        msg.data.push_back (object_type_e::ROBOT_LOCATION);

        msg.data.resize (len + PAYLOAD_OFFSET);
        std::copy ( (uint8_t*)&app_data.robot_loc, 
                    (uint8_t*)&app_data.robot_loc + len, 
                    msg.data.begin() + PAYLOAD_OFFSET);

        App_Send_Data_To_Protocol (msg);
    }

    void transport::App_Send_Data_To_Protocol (const std_msgs::Int8MultiArray& msg)
    {
        user_package_pub.publish (msg);
    }
}