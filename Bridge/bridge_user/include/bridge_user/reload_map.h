#ifndef _H_BRIDGE_USER_RELOAD_MAP_H_
#define _H_BRIDGE_USER_RELOAD_MAP_H_
#include "handler.h"

namespace bridge
{
    void reload_map::reload_map_handler()
    {
        std::cout << "run code cmd" << std::endl;
        system ("rosnode kill /map_server");
        system ("sleep 1.0");

        boost::thread load_map = boost::thread(boost::bind(&reload_map::loadMap_thread, this));
        load_map.detach();
    }

    void reload_map::loadMap_thread ()
    {
        printf ("reLoad Map Server\n");
        system ("roslaunch execute load_map.launch");
    }
}

#endif // _H_BRIDGE_USER_RELOAD_MAP_H_