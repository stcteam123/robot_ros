#include "bridge_user/transport_node.h"

int main(int argc, char *argv[])
{
    /* code for main function */
    ros::init(argc, argv, "bridge_transport");
    bridge::transport ts;
    ts.transport_run ();

    printf ("transport_node ready \n");
    ros::spin ();

    return 0;
}