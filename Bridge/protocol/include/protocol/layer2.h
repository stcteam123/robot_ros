#pragma once
#include "LObject.h"

eStatus_t L2Init (sL2Object_t* pL2State);

eStatus_t L2InitRecv(sL2Object_t* pL2State);

eStatus_t L2SendData(sL2Object_t* pL2State, U8_TYPE* u8Data, uint16_t u16Len, eMinorType_t eMinorCMD);

eStatus_t L2SendCMD(sL2Object_t* pL2State);

eStatus_t L2PackData(sL2Object_t* pL2State, U8_TYPE* u8Buff);

eStatus_t L2AppendCRC(U8_TYPE* u8Buff, uint16_t u16Len);

eStatus_t L2SendDataHandler(U8_TYPE* u8Buff, uint16_t u16Len);

eStatus_t L2RecvHandler(sL2Object_t* pL2State);

eStatus_t L2CallbackHandler(U8_TYPE* u8Buff, uint16_t u16Len);

eStatus_t DefaultRecvHandler(U8_TYPE* u8Buff, uint16_t u16Len);

/* unPackage */
eStatus_t L2u8BuffToPkg(sL2Object_t* pL2State);

BOOL      L2CheckCRC(U8_TYPE* u8Buff, uint16_t u16Len);