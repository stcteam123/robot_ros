#pragma once
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

/* define */
#ifndef BOOL
#define BOOL		uint8_t
#define TRUE		(1)
#define FALSE		(0)
#endif 

#ifndef NULL
#define NULL		(0)
#endif

#define DEBUG_PRINT		(1)

// printf("\r\n[%s-%s:%d]\r\n\t\t\t\t\r\n" fmt, (char*)__func__, (char*)__FILE__, __LINE__, ##__VA_ARGS__); \

#ifndef LREP
#define LREP(fmt, ...) do { 	\
			if (DEBUG_PRINT){	\
				printf("\r\n[%s - %d] : " fmt, (char*)__func__, __LINE__, ##__VA_ARGS__); \	
				printf("\n");   \
			}					\
} while (0)
#endif // !LREP

#define ASSERT(x)	if (!(x)) {				\
						LREP ("ASSERT");	\
						exit(0);				\
					}						

#define U8_TYPE		uint8_t

/* enum */

typedef enum eStatus
{
	RET_OK,
	RET_FAIL,
	RET_ERROR
} eStatus_t;


/*  package  */
/*
	layer3 : encryption
	layer2 : encoder and decoder data 
	layer1 : init socket, send and receive data from socket
*/