#include "layer1.h"
#include "layer2.h"
#include "layer3.h"
#include "crc8.h"

extern eStatus_t RecvHandlerDataMinor(U8_TYPE* u8Buff, uint16_t u16Len);
extern eStatus_t RecvHandlerSyncKeyMinor(U8_TYPE* u8Buff, uint16_t u16Len);

eStatus_t L2Init(sL2Object_t* pL2State)
{
	ASSERT(pL2State);

	pL2State->u8Data			= NULL;
	pL2State->u16DataLen		= 0;
	pL2State->bIsEnableEncrypt	= DEFAULT_ENCRYPT;
	pL2State->bIsNeedAck		= DEFAULT_ACK;
	pL2State->eACK				= NONE;
	pL2State->u8MajorCmd		= ROBOT1_CMD;
	pL2State->u8MinorCmd		= NONE_PKG;
	pL2State->u8NumberResend	= 3;
	pL2State->u8Seq				= 0;
	pL2State->u8LastSeq			= pL2State->u8Seq;
	pL2State->fnSendData		= L2SendDataHandler;
	pL2State->fnCallback		= NULL;
	pL2State->fnDriverInitL1	= L1InitDrive;

	return L1Init(pL2State);
}

eStatus_t L2InitRecv(sL2Object_t* pL2State)
{
	ASSERT(pL2State);
	
	pL2State->u8Data			= NULL;
	pL2State->u16DataLen		= 0;
	pL2State->bIsEnableEncrypt	= DEFAULT_ENCRYPT;
	pL2State->bIsNeedAck		= DEFAULT_ACK;
	pL2State->eACK				= NONE;
	pL2State->u8MajorCmd		= NONE_CMD;
	pL2State->u8MinorCmd		= NONE_PKG;
	pL2State->u8NumberResend	= 3;
	pL2State->u8Seq				= 0;
	pL2State->u8LastSeq			= pL2State->u8Seq;
	pL2State->fnSendData		= NULL;
	pL2State->fnCallback		= DefaultRecvHandler;
	pL2State->fnDriverInitL1	= NULL;

	return RET_OK;
}

eStatus_t L2SendData(	sL2Object_t* pL2State, 
						U8_TYPE* u8Data, 
						uint16_t u16Len, 
						eMinorType_t eMinorCMD)
{
	ASSERT(pL2State);
	ASSERT(u8Data);
	ASSERT(u16Len <= MAX_PAYLOAD_LEN);

	pL2State->u8MinorCmd = eMinorCMD;
	pL2State->u8Data = u8Data;
	pL2State->u16DataLen = u16Len;

	if (L2SendCMD (pL2State) == RET_OK)
	{
		if (pL2State->bIsNeedAck)
		{
			// waiting ACK
			// semaphore_pend () handler
			
		} /* End if (bIsNeedAck) */
	}
	else
	{
		LREP("Can't send pkg");
		return RET_FAIL;
	}

	return RET_OK;
}

eStatus_t L2SendCMD(sL2Object_t* pL2State)
{
	ASSERT(pL2State);

	eStatus_t ret = RET_OK;

	U8_TYPE* u8Buff = (U8_TYPE*)malloc(pL2State->u16DataLen + HEADER_LENGTH + 1);

	ret = L2PackData(pL2State, u8Buff);
	if (ret != RET_OK)
	{
		return ret;
	}

	if (pL2State->fnSendData != NULL)
	{
		ret = pL2State->fnSendData(u8Buff, (uint16_t) (pL2State->u16DataLen + HEADER_LENGTH + 1));
		if (ret != RET_OK)
		{
			return ret;
		}
	}

	free(u8Buff);

	return RET_OK;
}

eStatus_t L2PackData(sL2Object_t* pL2State, U8_TYPE* u8Buff)
{
	ASSERT(pL2State);
	ASSERT(u8Buff);

	u8Buff[PREAMBLE_OFFSET]			= PREAMBLE;
	u8Buff[MAJOR_OFFSET]			= pL2State->u8MajorCmd;
	u8Buff[MINOR_OFFSET]			= pL2State->u8MinorCmd;
	u8Buff[SEQ_OFFSET]				= pL2State->u8Seq;
	u8Buff[RESERVER_OFFSET]			= (pL2State->bIsEnableEncrypt << IS_ENABLE_ENCRYPT_POS) | (pL2State->bIsNeedAck << IS_NEED_FEEDBACK_ACK);
	u8Buff[LEN_PAY_LOAD_OFFSET]		= (uint8_t) ((pL2State->u16DataLen >> 8) & 0xff);
	u8Buff[LEN_PAY_LOAD_OFFSET + 1] = (uint8_t)(pL2State->u16DataLen & 0xff);
	
	L2AppendCRC(u8Buff, HEADER_LENGTH);

	memcpy(u8Buff + PAY_LOAD_OFFSET, pL2State->u8Data, pL2State->u16DataLen);

	L2AppendCRC(u8Buff + PAY_LOAD_OFFSET, pL2State->u16DataLen + 1);

	return RET_OK;
}

eStatus_t L2AppendCRC(U8_TYPE* u8Buff, uint16_t u16Len)
{
	ASSERT(u8Buff);
	ASSERT(u16Len > 0);

	u8Buff[u16Len - 1] = crc_8(u8Buff, u16Len - 1);

	return RET_OK;
}

eStatus_t L2SendDataHandler(U8_TYPE* u8Buff, uint16_t u16Len)
{
	ASSERT (u8Buff);

	sL1Object_t L1Obj;
	L1Obj.u8DataSending = u8Buff;
	L1Obj.u16DataLen = u16Len;
	return L1SendData(&L1Obj);
}

eStatus_t L2RecvHandler(sL2Object_t* pL2State)
{
	ASSERT(pL2State);

	uint16_t u16LenPkg = pL2State->u16DataLen;
	if (u16LenPkg < HEADER_LENGTH)
	{
		LREP ("u16LenPkg is lower HEADER_LENGTH %d", u16LenPkg);
		return RET_FAIL;
	}
	else
	{
		LREP ("[%d]", u16LenPkg);
	}

	uint16_t u16CntPkg = 0;
	uint8_t u8Ret = 0;

	/* Unpack data from Layer1 */
	/* Split data to package */
	U8_TYPE* u8Ptr = pL2State->u8Data;
	do 
	{
		// find PREAMBLE in package received
		if (*u8Ptr != PREAMBLE)
		{
			/* delete data until meet PREAMBLE */
			uint16_t cntIdx = 0;
			U8_TYPE u8Tmp = *u8Ptr;
			do
			{
				cntIdx++;
				u8Tmp = *(u8Ptr + cntIdx);
			} while (cntIdx < u16LenPkg && u8Tmp != PREAMBLE);

			if (cntIdx >= u16LenPkg)
			{
				LREP ("don't find PREAMBLE");
				return RET_FAIL;
			}

			u8Ptr += cntIdx;
			u16LenPkg -= cntIdx;
		}

		if (u16LenPkg >= HEADER_LENGTH)
		{
			if (!L2CheckCRC (u8Ptr, HEADER_LENGTH))
			{
				/* Wrong CRC Header */
				LREP ("Wrong CRC Header, check next pkg");
				u8Ptr += HEADER_LENGTH;
				u16LenPkg -= HEADER_LENGTH;
				u8Ret += RET_FAIL;
				continue;
			}

			uint16_t PayLoadLength = (uint16_t)(((*(u8Ptr + LEN_PAY_LOAD_OFFSET)) << 8) + (*(u8Ptr + LEN_PAY_LOAD_OFFSET + 1)));
			if (PayLoadLength > MAX_PAYLOAD_LEN)
			{
				/* something wrong Header */
				LREP ("PayLoadLength is higher MAX_PAYLOAD_LEN");
				u8Ptr += HEADER_LENGTH;
				u16LenPkg -= HEADER_LENGTH;
				u8Ret += RET_FAIL;
				continue;
			}

			if (u16LenPkg < PayLoadLength + HEADER_LENGTH + 1)
			{
				/* Do not enough data */
				LREP ("Do not enough data for one pkg: %d", u16LenPkg);
				return RET_FAIL;
			}

			pL2State->u16DataLen = PayLoadLength + HEADER_LENGTH + 1;
			pL2State->u8Data = u8Ptr;

			if (L2u8BuffToPkg (pL2State) == RET_OK)
			{
				if (pL2State->u8Seq == pL2State->u8LastSeq)
				{
					if (pL2State->u8MajorCmd == USER_CMD && pL2State->u8MinorCmd < ERROR_PKG)
					{
						/* check Minor CMD */
						if (pL2State->u8MinorCmd == DATA_PKG)
						{
							sL3Object_t pL3State;
							pL3State.eMajor				= (eMajorType_t) (pL2State->u8MajorCmd);
							pL3State.u8RawData			= pL2State->u8Data + HEADER_LENGTH;
							pL3State.u16DataLen			= PayLoadLength;
							pL3State.bIsEnableEncrypt	= pL2State->bIsEnableEncrypt;

							pL2State->fnCallback 		= RecvHandlerDataMinor;
							u8Ret += L3RecvHandler(&pL3State, pL2State->fnCallback);
						}
						else if (pL2State->u8MinorCmd == SYNC_PKG)
						{
							// implement after
							// check key data to sync with user
							sL3Object_t pL3State;
							pL3State.eMajor				= (eMajorType_t) (pL2State->u8MajorCmd);
							pL3State.u8RawData			= pL2State->u8Data + HEADER_LENGTH;
							pL3State.u16DataLen			= PayLoadLength;
							pL3State.bIsEnableEncrypt	= pL2State->bIsEnableEncrypt;

							pL2State->fnCallback 		= RecvHandlerSyncKeyMinor;
							u8Ret += L3RecvHandler(&pL3State, pL2State->fnCallback);
						}
						else
						{
							// implement after
							// do nothing
							u8Ret += RET_OK;
						}
					}
					else
					{
						u8Ret += RET_FAIL;
						LREP ("u8MajorCmd or u8MinorCmd have something wrong");
					}
				}
				else
				{
					/* Pkg was split */
					LREP("Pkg was split");
					u8Ret += RET_FAIL;
				}
			}
			else
			{
				u8Ret += RET_FAIL;
			}
		}
		else
		{
			LREP ("lenPkg enough Header langth");
			return RET_FAIL;
		}

		u16CntPkg ++;
		u16LenPkg -= pL2State->u16DataLen;
		u8Ptr += pL2State->u16DataLen;
	} while (u16LenPkg >= HEADER_LENGTH);

	u8Ret = (u8Ret == RET_OK) ? RET_OK : RET_FAIL;
	LREP ("u16CntPkg = %d", u16CntPkg);

	return (eStatus_t) u8Ret;
}

eStatus_t L2CallbackHandler(U8_TYPE* u8Buff, uint16_t u16Len)
{
	uint8_t print_data = 0;
	if (print_data)
	{
		/* print data receive from user : origin data */
		for (int i = 0; i < u16Len; i++)
		{
			printf("%d  ", *(u8Buff + i));
		}
		printf("\n");
	}

	return RET_OK;
}

eStatus_t DefaultRecvHandler(U8_TYPE* u8Buff, uint16_t u16Len)
{
	// do nothing
	LREP ("default Recv Handler");

	return RET_OK;
}

BOOL L2CheckCRC(U8_TYPE* u8Buff, uint16_t u16Len)
{
	ASSERT(u8Buff);
	return (u8Buff[u16Len - 1] == crc_8(u8Buff, u16Len - 1));
}

eStatus_t L2u8BuffToPkg(sL2Object_t* pL2State)
{
	ASSERT(pL2State);

	pL2State->u8MajorCmd		= pL2State->u8Data[MAJOR_OFFSET];
	pL2State->u8MinorCmd		= pL2State->u8Data[MINOR_OFFSET];
	pL2State->u8Seq				= pL2State->u8Data[SEQ_OFFSET];
	pL2State->bIsEnableEncrypt	= (pL2State->u8Data[RESERVER_OFFSET] & (1 << IS_ENABLE_ENCRYPT_POS)) == 0 ? FALSE : TRUE;
	pL2State->bIsNeedAck		= (pL2State->u8Data[RESERVER_OFFSET] & (1 << IS_NEED_FEEDBACK_ACK)) == 0 ? FALSE : TRUE;

	uint16_t u16LenPayLoad = (uint16_t)((pL2State->u8Data[LEN_PAY_LOAD_OFFSET] << 8) + pL2State->u8Data[LEN_PAY_LOAD_OFFSET + 1]);;
	if (!L2CheckCRC(pL2State->u8Data + PAY_LOAD_OFFSET, u16LenPayLoad + 1))
	{
		LREP ("Wrong CRC Payload");
		return RET_FAIL;
	}

	return RET_OK;
}