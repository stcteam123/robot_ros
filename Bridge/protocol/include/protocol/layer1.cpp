#include "layer1.h"
#include "layer2.h"

static TcpConnection client;
static TcpConnection server;

static boost::mutex recv_mux;

eStatus_t L1Init(sL2Object_t* pL2State)
{
	ASSERT(pL2State);
	if (pL2State->fnDriverInitL1 != NULL)
	{
		return pL2State->fnDriverInitL1(pL2State->port);
	}

	return RET_OK;
}

eStatus_t L1SendData(sL1Object_t* L1State)
{
	ASSERT (L1State);

	if (client.stt_conn == 0)
	{
		LREP ("socket is not connected");
		return RET_FAIL;
	}
	
	if (!client.send (L1State->u8DataSending, L1State->u16DataLen))
	{
		LREP ("send false");
		client.close();
		return RET_FAIL;
	}
	else
	{
		LREP ("send oki");
	}

	return RET_OK;
}

eStatus_t L1RecvData(U8_TYPE* u8Buff, uint16_t u16Len)
{
	ASSERT (u8Buff);

	recv_mux.lock ();
	// sL2Object_t pL2State;
	sL1Object_t L1State;
	L1State.u16DataLen = u16Len;
	L1State.u8DataSending = u8Buff;
	eStatus_t ret = L1RecvHandler(&L1State);
	recv_mux.unlock ();
	return ret;
}

eStatus_t L1RecvHandler(sL1Object_t* L1State)
{
	ASSERT (L1State);

	sL2Object_t pL2State;
	L2InitRecv(&pL2State);

	pL2State.u8Data = L1State->u8DataSending;
	pL2State.u16DataLen = L1State->u16DataLen;

	return L2RecvHandler (&pL2State);
}

eStatus_t L1InitDrive (uint16_t port)
{
	signal(SIGPIPE, SIG_IGN);
	Socket newSocket;
	server = newSocket.init();
	server.init (port);

	// create thread receive handler
	boost::thread re_socket_thread = boost::thread(boost::bind(L1ReconnectSocketThreadHandler));
	re_socket_thread.detach();

	return RET_OK;
}

void L1RecvThreadHandler (void)
{
	while (1)
	{
		U8_TYPE u8Buff[50000];
		int16_t i16Len = client.receive (u8Buff, 50000);
		if (i16Len > 0) /* Receive data from client */
		{
			if (L1RecvData (u8Buff, i16Len) == RET_OK)
			{
				LREP ("Receive OK");
			}
			else
			{
				LREP ("Receive FALSE");
				// exit (0);
			}
		}
		else if (i16Len == 0) /* Plz waiting client to reconnect */
		{
			client.close();
			LREP ("Disconnect with client");
			break;
		}

		usleep (20);  // sleep 20 us
	}
}


// checking connection status between server and client
void L1ReconnectSocketThreadHandler (void)
{
	while (1)
	{
		if (client.stt_conn == 0)
		{
			// client.close();
			LREP ("waiting to client reconnect..\n");
			client = server.accept ();
			// create thread receive handler
			boost::thread recv_thread = boost::thread(boost::bind(L1RecvThreadHandler));
			recv_thread.detach();
		}

		usleep (50000); // sleep 50 ms
	}
}