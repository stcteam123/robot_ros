#pragma once
#include "LObject.h"
#include "socket_server.h"

eStatus_t L1Init(sL2Object_t* pL2State);

eStatus_t L1SendData(sL1Object_t* L1State);

eStatus_t L1RecvData(sL2Object_t* pL2State, U8_TYPE* u8Buff, uint16_t u16Len);

eStatus_t L1RecvHandler(sL1Object_t* L1State);

eStatus_t L1InitDrive (uint16_t);

void L1RecvThreadHandler (void);

void L1ReconnectSocketThreadHandler (void);