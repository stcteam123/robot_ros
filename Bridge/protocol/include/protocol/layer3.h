#pragma once
#include "layer2.h"

/* API */

eStatus_t L3Init				(sL2Object_t* pL2State);

eStatus_t L3SendData			(sL2Object_t* pL2State, U8_TYPE* u8Data, uint16_t u16Len, eMinorType_t eMinorCMD);

eStatus_t L3RecvHandler			(sL3Object_t* pL3State, RecvCallback fnCallBack);

eStatus_t L3Encryption			(sL3Object_t* pL3State);

eStatus_t L3Decryption			(sL3Object_t* pL3State);