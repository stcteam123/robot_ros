#include "layer3.h"
#include "layer2.h"

eStatus_t L3Init(sL2Object_t* pL2State)
{
	ASSERT(pL2State);

	return L2Init(pL2State);
}

eStatus_t L3SendData(	sL2Object_t* pL2State, 
						U8_TYPE* u8Data,			/* Raw data */
						uint16_t u16Len,			/* Len raw data */
						eMinorType_t eMinorCMD)
{
	ASSERT(pL2State);
	ASSERT(u8Data);

	/* assign u8Data to L3State */
	sL3Object_t L3State;
	L3State.u8RawData = u8Data;
	L3State.u16DataLen = u16Len;

	if (pL2State->bIsEnableEncrypt)
	{
		/* Encrypt first */
		if (RET_OK != L3Encryption(&L3State))
		{
			LREP ("Encryption false");
			return RET_FAIL;
		}
	}

	return L2SendData(pL2State, L3State.u8RawData, L3State.u16DataLen, eMinorCMD);
}

eStatus_t L3RecvHandler(sL3Object_t* pL3State, RecvCallback fnCallBack)
{
	ASSERT(pL3State);
	if (pL3State->bIsEnableEncrypt)
	{
		if (L3Decryption(pL3State) != RET_OK)
		{
			LREP ("Decryption false");
			return RET_FAIL;
		}
	}

	if (fnCallBack != NULL)
	{
		(*fnCallBack)(pL3State->u8RawData, pL3State->u16DataLen);
	}

	return RET_OK;
}

eStatus_t L3Encryption(sL3Object_t* pL3State)
{
	ASSERT(pL3State);

	return RET_OK;
}

eStatus_t L3Decryption(sL3Object_t* pL3State)
{
	ASSERT(pL3State);

	return RET_OK;
}