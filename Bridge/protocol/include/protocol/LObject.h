#pragma once
#include "define.h"
// #include "socket_server.h"

#include <boost/chrono.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/thread.hpp>

/* Function pointer */
typedef eStatus_t(*RecvCallback) (U8_TYPE* u8Buff, uint16_t u16Len);
typedef eStatus_t(*SendData) (U8_TYPE* u8Buff, uint16_t u16Len);
typedef eStatus_t(*DriverInit)(uint16_t);

/* Define */
#define MAX_PAYLOAD_LEN			(1000)
#define MAX_KEY_SIZE			(16)
#define PREAMBLE				(0x23)

#define PREAMBLE_OFFSET			(0)
#define MAJOR_OFFSET			(PREAMBLE_OFFSET	 + 1)
#define MINOR_OFFSET			(MAJOR_OFFSET		 + 1)
#define SEQ_OFFSET				(MINOR_OFFSET		 + 1)
#define RESERVER_OFFSET			(SEQ_OFFSET			 + 1)
#define LEN_PAY_LOAD_OFFSET		(RESERVER_OFFSET	 + 1)
#define HEADER_CRC_OFFSET		(LEN_PAY_LOAD_OFFSET + 2)
#define PAY_LOAD_OFFSET			(HEADER_CRC_OFFSET	 + 1)
#define HEADER_LENGTH			(PAY_LOAD_OFFSET)

#define DEFAULT_MAJOR_CMD		ROBOT_CMD
#define DEFAULT_ACK				(FALSE)
#define DEFAULT_ENCRYPT			(FALSE)
#define IS_ENABLE_ENCRYPT_POS	(0)
#define IS_NEED_FEEDBACK_ACK	(1)

/* enum */
typedef enum eMajorType
{
	ROBOT1_CMD,
	ROBOT2_CMD,
	USER_CMD,

	NONE_CMD,
	ERROR_CMD
} eMajorType_t;

typedef enum
{
	HEADER_PKG,
	DATA_PKG,
	ACK_PKG,
	NAK_PKG,
	PING_PKG,
	SYNC_PKG,
	DEBUG_PKG,
	
	NONE_PKG,
	ERROR_PKG
} eMinorType_t;

typedef enum
{
	NONE,
	ACK,
	NAK 

} eACKType_t;


/* Struct */

// L3_Object
typedef struct
{
	U8_TYPE*		u8RawData;
	uint16_t		u16DataLen;
	eMajorType_t    eMajor;
	BOOL			bIsEnableEncrypt;
} sL3Object_t;

// L2_Object
typedef struct sL2Object
{
	U8_TYPE*		u8Data;
	uint16_t		u16DataLen;

	uint8_t			u8MajorCmd;
	uint8_t			u8MinorCmd;
	uint8_t			u8Seq;
	uint8_t			u8LastSeq;
	BOOL			bIsNeedAck;
	BOOL			bIsEnableEncrypt;
	eACKType_t		eACK;
	uint8_t			u8NumberResend;

	uint16_t        port;

	/* Callback handler to App */
	RecvCallback	fnCallback;
	SendData		fnSendData;
	DriverInit		fnDriverInitL1;

} sL2Object_t;

// L1_Object
typedef struct
{
	U8_TYPE*		u8DataSending;
	uint16_t		u16DataLen;
} sL1Object_t;


