#include "protocol/layer3.h"

#include <ros/ros.h>
#include <std_msgs/Int8MultiArray.h>

#include <string.h>
#include <stdio.h>

static sL2Object_t g_L2State;
static ros::Subscriber send_pkg_sub;
static ros::Publisher recv_pkg_pub;

static BOOL g_bNodeReady = false;
static BOOL g_bPairedKey = false;

eStatus_t checkKey (U8_TYPE* u8Buff, uint16_t u16Len)
{
	ASSERT (u8Buff);
	//todo: write function check key to sync

	return RET_OK;
}

void SendPkgHandler_callback (const std_msgs::Int8MultiArray::ConstPtr& msg)
{
	if (g_bNodeReady == true && g_bPairedKey == true)
	{
		uint16_t u16Len = msg->data.size ();

		U8_TYPE* u8Buff = new U8_TYPE[u16Len];
		for (int i = 0; i < u16Len; i++)
		{
			*(u8Buff + i) = msg->data[i];
		}

		if (L3SendData(&g_L2State, u8Buff, u16Len, DATA_PKG) != RET_OK)
		{
			LREP("Send fail pkg");
		}

		delete u8Buff;
	}
	else
	{
		LREP ("Node is not readly or not paired yet");
	}
}

eStatus_t RecvHandlerDataMinor(U8_TYPE* u8Buff, uint16_t u16Len)
{
	if (g_bNodeReady == true && g_bPairedKey == true)
	{
		// /* print data receive from user : raw data */
		// for (int i = 0; i < u16Len; i++)
		// {
		// 	printf("%d  ", *(u8Buff + i));
		// }
		// printf("\n");

		std_msgs::Int8MultiArray msg;
		msg.data.clear ();

		for (int i = 0; i < u16Len; i++)
		{
			msg.data.push_back (u8Buff[i]);
		}

		recv_pkg_pub.publish (msg);
		return RET_OK;
	}

	LREP ("RecvHandlerDataMinor is not Ready");
	return RET_FAIL;
}

eStatus_t RecvHandlerSyncKeyMinor(U8_TYPE* u8Buff, uint16_t u16Len)
{
	if (g_bNodeReady == true)
	{
		//todo: check key and send return to user sync
		if (checkKey (u8Buff, u16Len) == RET_OK)
		{
			U8_TYPE u8Data[] = {};
			if (L3SendData(&g_L2State, u8Data, 0, SYNC_PKG) != RET_OK)
			{
				LREP("Send fail pkg");
			}
			else
			{
				if (!g_bPairedKey)
				{
					g_bPairedKey = true;
					LREP ("sync key was sucessful");
				}
			}

			return RET_OK;
		}
		else
		{
			g_bPairedKey == false;
		}
	}

	g_bPairedKey == false;
	LREP ("g_bNodeReady is not Ready");
	return RET_FAIL;
}

int main(int argc, char** argv)
{
	// ros init
	ros::init(argc, argv, "protocol");
	ros::NodeHandle n;
	send_pkg_sub = n.subscribe ("robot/send_package", 10, SendPkgHandler_callback);
	recv_pkg_pub = n.advertise<std_msgs::Int8MultiArray>("robot/recv_package", 10);
	
	// protocol Init
	g_L2State.port = 7001;
	L3Init(&g_L2State);

	g_bNodeReady = true;

	ros::spin(); 
	return 0;
}